package com.mgoulene.msha.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A SpentSharingConfig.
 */
@Entity
@Table(name = "spent_sharing_config")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SpentSharingConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "share", nullable = false)
    private Float share;

    @ManyToOne
    @JsonIgnoreProperties("spentSharingConfigs")
    private Profil sharingUser;

    @ManyToOne
    @JsonIgnoreProperties("defaultSpentSharingConfigs")
    private SpentConfig defaultSpentConfig;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getShare() {
        return share;
    }

    public SpentSharingConfig share(Float share) {
        this.share = share;
        return this;
    }

    public void setShare(Float share) {
        this.share = share;
    }

    public Profil getSharingUser() {
        return sharingUser;
    }

    public SpentSharingConfig sharingUser(Profil profil) {
        this.sharingUser = profil;
        return this;
    }

    public void setSharingUser(Profil profil) {
        this.sharingUser = profil;
    }

    public SpentConfig getDefaultSpentConfig() {
        return defaultSpentConfig;
    }

    public SpentSharingConfig defaultSpentConfig(SpentConfig spentConfig) {
        this.defaultSpentConfig = spentConfig;
        return this;
    }

    public void setDefaultSpentConfig(SpentConfig spentConfig) {
        this.defaultSpentConfig = spentConfig;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SpentSharingConfig)) {
            return false;
        }
        return id != null && id.equals(((SpentSharingConfig) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "SpentSharingConfig{" +
            "id=" + getId() +
            ", share=" + getShare() +
            "}";
    }
}
