package com.mgoulene.msha.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A SpentSharing.
 */
@Entity
@Table(name = "spent_sharing")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SpentSharing implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "amount_share", nullable = false)
    private Float amountShare;

    @NotNull
    @Column(name = "share", nullable = false)
    private Float share;

    @ManyToOne
    @JsonIgnoreProperties("spentSharings")
    private Profil sharingUser;

    @ManyToOne
    @JsonIgnoreProperties("sharings")
    private Spent spent;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getAmountShare() {
        return amountShare;
    }

    public SpentSharing amountShare(Float amountShare) {
        this.amountShare = amountShare;
        return this;
    }

    public void setAmountShare(Float amountShare) {
        this.amountShare = amountShare;
    }

    public Float getShare() {
        return share;
    }

    public SpentSharing share(Float share) {
        this.share = share;
        return this;
    }

    public void setShare(Float share) {
        this.share = share;
    }

    public Profil getSharingUser() {
        return sharingUser;
    }

    public SpentSharing sharingUser(Profil profil) {
        this.sharingUser = profil;
        return this;
    }

    public void setSharingUser(Profil profil) {
        this.sharingUser = profil;
    }

    public Spent getSpent() {
        return spent;
    }

    public SpentSharing spent(Spent spent) {
        this.spent = spent;
        return this;
    }

    public void setSpent(Spent spent) {
        this.spent = spent;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SpentSharing)) {
            return false;
        }
        return id != null && id.equals(((SpentSharing) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "SpentSharing{" +
            "id=" + getId() +
            ", amountShare=" + getAmountShare() +
            ", share=" + getShare() +
            "}";
    }
}
