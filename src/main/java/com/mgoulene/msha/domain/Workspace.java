package com.mgoulene.msha.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Workspace.
 */
@Entity
@Table(name = "workspace")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Workspace implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 5, max = 40)
    @Column(name = "label", length = 40, nullable = false)
    private String label;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "workspace_owner",
               joinColumns = @JoinColumn(name = "workspace_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "owner_id", referencedColumnName = "id"))
    private Set<Profil> owners = new HashSet<>();

    @OneToMany(mappedBy = "workspace")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<TaskProject> taskProjects = new HashSet<>();

    @OneToMany(mappedBy = "workspace")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Wallet> wallets = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public Workspace label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Set<Profil> getOwners() {
        return owners;
    }

    public Workspace owners(Set<Profil> profils) {
        this.owners = profils;
        return this;
    }

    public Workspace addOwner(Profil profil) {
        this.owners.add(profil);
        profil.getWorkspaces().add(this);
        return this;
    }

    public Workspace removeOwner(Profil profil) {
        this.owners.remove(profil);
        profil.getWorkspaces().remove(this);
        return this;
    }

    public void setOwners(Set<Profil> profils) {
        this.owners = profils;
    }

    public Set<TaskProject> getTaskProjects() {
        return taskProjects;
    }

    public Workspace taskProjects(Set<TaskProject> taskProjects) {
        this.taskProjects = taskProjects;
        return this;
    }

    public Workspace addTaskProject(TaskProject taskProject) {
        this.taskProjects.add(taskProject);
        taskProject.setWorkspace(this);
        return this;
    }

    public Workspace removeTaskProject(TaskProject taskProject) {
        this.taskProjects.remove(taskProject);
        taskProject.setWorkspace(null);
        return this;
    }

    public void setTaskProjects(Set<TaskProject> taskProjects) {
        this.taskProjects = taskProjects;
    }

    public Set<Wallet> getWallets() {
        return wallets;
    }

    public Workspace wallets(Set<Wallet> wallets) {
        this.wallets = wallets;
        return this;
    }

    public Workspace addWallet(Wallet wallet) {
        this.wallets.add(wallet);
        wallet.setWorkspace(this);
        return this;
    }

    public Workspace removeWallet(Wallet wallet) {
        this.wallets.remove(wallet);
        wallet.setWorkspace(null);
        return this;
    }

    public void setWallets(Set<Wallet> wallets) {
        this.wallets = wallets;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Workspace)) {
            return false;
        }
        return id != null && id.equals(((Workspace) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Workspace{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            "}";
    }
}
