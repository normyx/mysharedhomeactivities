package com.mgoulene.msha.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Spent.
 */
@Entity
@Table(name = "spent")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Spent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 2, max = 100)
    @Column(name = "label", length = 100, nullable = false)
    private String label;

    @NotNull
    @Column(name = "amount", nullable = false)
    private Float amount;

    @NotNull
    @Column(name = "spent_date", nullable = false)
    private LocalDate spentDate;

    @NotNull
    @Column(name = "validated", nullable = false)
    private Boolean validated;

    @ManyToOne
    @JsonIgnoreProperties("spents")
    private Profil spender;

    @ManyToOne
    @JsonIgnoreProperties("spents")
    private Wallet wallet;

    @OneToOne(mappedBy = "spent")
    @JsonIgnore
    private SpentRecurrence recurrence;

    @OneToMany(mappedBy = "spent")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<SpentSharing> sharings = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public Spent label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Float getAmount() {
        return amount;
    }

    public Spent amount(Float amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public LocalDate getSpentDate() {
        return spentDate;
    }

    public Spent spentDate(LocalDate spentDate) {
        this.spentDate = spentDate;
        return this;
    }

    public void setSpentDate(LocalDate spentDate) {
        this.spentDate = spentDate;
    }

    public Boolean isValidated() {
        return validated;
    }

    public Spent validated(Boolean validated) {
        this.validated = validated;
        return this;
    }

    public void setValidated(Boolean validated) {
        this.validated = validated;
    }

    public Profil getSpender() {
        return spender;
    }

    public Spent spender(Profil profil) {
        this.spender = profil;
        return this;
    }

    public void setSpender(Profil profil) {
        this.spender = profil;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public Spent wallet(Wallet wallet) {
        this.wallet = wallet;
        return this;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }

    public SpentRecurrence getRecurrence() {
        return recurrence;
    }

    public Spent recurrence(SpentRecurrence spentRecurrence) {
        this.recurrence = spentRecurrence;
        return this;
    }

    public void setRecurrence(SpentRecurrence spentRecurrence) {
        this.recurrence = spentRecurrence;
    }

    public Set<SpentSharing> getSharings() {
        return sharings;
    }

    public Spent sharings(Set<SpentSharing> spentSharings) {
        this.sharings = spentSharings;
        return this;
    }

    public Spent addSharing(SpentSharing spentSharing) {
        this.sharings.add(spentSharing);
        spentSharing.setSpent(this);
        return this;
    }

    public Spent removeSharing(SpentSharing spentSharing) {
        this.sharings.remove(spentSharing);
        spentSharing.setSpent(null);
        return this;
    }

    public void setSharings(Set<SpentSharing> spentSharings) {
        this.sharings = spentSharings;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Spent)) {
            return false;
        }
        return id != null && id.equals(((Spent) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Spent{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", amount=" + getAmount() +
            ", spentDate='" + getSpentDate() + "'" +
            ", validated='" + isValidated() + "'" +
            "}";
    }
}
