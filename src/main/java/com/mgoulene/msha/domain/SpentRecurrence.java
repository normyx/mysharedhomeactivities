package com.mgoulene.msha.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A SpentRecurrence.
 */
@Entity
@Table(name = "spent_recurrence")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SpentRecurrence implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "amount", nullable = false)
    private Float amount;

    @NotNull
    @Column(name = "recurrence_from", nullable = false)
    private LocalDate recurrenceFrom;

    @Column(name = "recurrence_to")
    private LocalDate recurrenceTo;

    @NotNull
    @Min(value = 1)
    @Max(value = 31)
    @Column(name = "day_in_month", nullable = false)
    private Integer dayInMonth;

    @OneToOne(optional = false)    @NotNull

    @JoinColumn(unique = true)
    private Spent spent;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getAmount() {
        return amount;
    }

    public SpentRecurrence amount(Float amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public LocalDate getRecurrenceFrom() {
        return recurrenceFrom;
    }

    public SpentRecurrence recurrenceFrom(LocalDate recurrenceFrom) {
        this.recurrenceFrom = recurrenceFrom;
        return this;
    }

    public void setRecurrenceFrom(LocalDate recurrenceFrom) {
        this.recurrenceFrom = recurrenceFrom;
    }

    public LocalDate getRecurrenceTo() {
        return recurrenceTo;
    }

    public SpentRecurrence recurrenceTo(LocalDate recurrenceTo) {
        this.recurrenceTo = recurrenceTo;
        return this;
    }

    public void setRecurrenceTo(LocalDate recurrenceTo) {
        this.recurrenceTo = recurrenceTo;
    }

    public Integer getDayInMonth() {
        return dayInMonth;
    }

    public SpentRecurrence dayInMonth(Integer dayInMonth) {
        this.dayInMonth = dayInMonth;
        return this;
    }

    public void setDayInMonth(Integer dayInMonth) {
        this.dayInMonth = dayInMonth;
    }

    public Spent getSpent() {
        return spent;
    }

    public SpentRecurrence spent(Spent spent) {
        this.spent = spent;
        return this;
    }

    public void setSpent(Spent spent) {
        this.spent = spent;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SpentRecurrence)) {
            return false;
        }
        return id != null && id.equals(((SpentRecurrence) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "SpentRecurrence{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", recurrenceFrom='" + getRecurrenceFrom() + "'" +
            ", recurrenceTo='" + getRecurrenceTo() + "'" +
            ", dayInMonth=" + getDayInMonth() +
            "}";
    }
}
