package com.mgoulene.msha.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A ShoppingList.
 */
@Entity
@Table(name = "shopping_list")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ShoppingList implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 5, max = 100)
    @Column(name = "label", length = 100, nullable = false)
    private String label;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "shopping_list_owner",
               joinColumns = @JoinColumn(name = "shopping_list_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "owner_id", referencedColumnName = "id"))
    private Set<Profil> owners = new HashSet<>();

    @OneToMany(mappedBy = "list")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ShoppingItem> items = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public ShoppingList label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Set<Profil> getOwners() {
        return owners;
    }

    public ShoppingList owners(Set<Profil> profils) {
        this.owners = profils;
        return this;
    }

    public ShoppingList addOwner(Profil profil) {
        this.owners.add(profil);
        profil.getShoppingLists().add(this);
        return this;
    }

    public ShoppingList removeOwner(Profil profil) {
        this.owners.remove(profil);
        profil.getShoppingLists().remove(this);
        return this;
    }

    public void setOwners(Set<Profil> profils) {
        this.owners = profils;
    }

    public Set<ShoppingItem> getItems() {
        return items;
    }

    public ShoppingList items(Set<ShoppingItem> shoppingItems) {
        this.items = shoppingItems;
        return this;
    }

    public ShoppingList addItem(ShoppingItem shoppingItem) {
        this.items.add(shoppingItem);
        shoppingItem.setList(this);
        return this;
    }

    public ShoppingList removeItem(ShoppingItem shoppingItem) {
        this.items.remove(shoppingItem);
        shoppingItem.setList(null);
        return this;
    }

    public void setItems(Set<ShoppingItem> shoppingItems) {
        this.items = shoppingItems;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ShoppingList)) {
            return false;
        }
        return id != null && id.equals(((ShoppingList) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ShoppingList{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            "}";
    }
}
