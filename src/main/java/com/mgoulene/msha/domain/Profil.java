package com.mgoulene.msha.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Profil.
 */
@Entity
@Table(name = "profil")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Profil implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "display_name", nullable = false)
    private String displayName;

    @Lob
    @Column(name = "photo")
    private byte[] photo;

    @Column(name = "photo_content_type")
    private String photoContentType;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @ManyToMany(mappedBy = "owners")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<Wallet> wallets = new HashSet<>();

    @ManyToMany(mappedBy = "owners")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<TaskProject> taskProjects = new HashSet<>();

    @ManyToMany(mappedBy = "owners")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<Workspace> workspaces = new HashSet<>();

    @ManyToMany(mappedBy = "owners")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<Task> tasks = new HashSet<>();

    @ManyToMany(mappedBy = "owners")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<ShoppingList> shoppingLists = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Profil displayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public Profil photo(byte[] photo) {
        this.photo = photo;
        return this;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPhotoContentType() {
        return photoContentType;
    }

    public Profil photoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
        return this;
    }

    public void setPhotoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
    }

    public User getUser() {
        return user;
    }

    public Profil user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Wallet> getWallets() {
        return wallets;
    }

    public Profil wallets(Set<Wallet> wallets) {
        this.wallets = wallets;
        return this;
    }

    public Profil addWallet(Wallet wallet) {
        this.wallets.add(wallet);
        wallet.getOwners().add(this);
        return this;
    }

    public Profil removeWallet(Wallet wallet) {
        this.wallets.remove(wallet);
        wallet.getOwners().remove(this);
        return this;
    }

    public void setWallets(Set<Wallet> wallets) {
        this.wallets = wallets;
    }

    public Set<TaskProject> getTaskProjects() {
        return taskProjects;
    }

    public Profil taskProjects(Set<TaskProject> taskProjects) {
        this.taskProjects = taskProjects;
        return this;
    }

    public Profil addTaskProject(TaskProject taskProject) {
        this.taskProjects.add(taskProject);
        taskProject.getOwners().add(this);
        return this;
    }

    public Profil removeTaskProject(TaskProject taskProject) {
        this.taskProjects.remove(taskProject);
        taskProject.getOwners().remove(this);
        return this;
    }

    public void setTaskProjects(Set<TaskProject> taskProjects) {
        this.taskProjects = taskProjects;
    }

    public Set<Workspace> getWorkspaces() {
        return workspaces;
    }

    public Profil workspaces(Set<Workspace> workspaces) {
        this.workspaces = workspaces;
        return this;
    }

    public Profil addWorkspace(Workspace workspace) {
        this.workspaces.add(workspace);
        workspace.getOwners().add(this);
        return this;
    }

    public Profil removeWorkspace(Workspace workspace) {
        this.workspaces.remove(workspace);
        workspace.getOwners().remove(this);
        return this;
    }

    public void setWorkspaces(Set<Workspace> workspaces) {
        this.workspaces = workspaces;
    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public Profil tasks(Set<Task> tasks) {
        this.tasks = tasks;
        return this;
    }

    public Profil addTask(Task task) {
        this.tasks.add(task);
        task.getOwners().add(this);
        return this;
    }

    public Profil removeTask(Task task) {
        this.tasks.remove(task);
        task.getOwners().remove(this);
        return this;
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }

    public Set<ShoppingList> getShoppingLists() {
        return shoppingLists;
    }

    public Profil shoppingLists(Set<ShoppingList> shoppingLists) {
        this.shoppingLists = shoppingLists;
        return this;
    }

    public Profil addShoppingList(ShoppingList shoppingList) {
        this.shoppingLists.add(shoppingList);
        shoppingList.getOwners().add(this);
        return this;
    }

    public Profil removeShoppingList(ShoppingList shoppingList) {
        this.shoppingLists.remove(shoppingList);
        shoppingList.getOwners().remove(this);
        return this;
    }

    public void setShoppingLists(Set<ShoppingList> shoppingLists) {
        this.shoppingLists = shoppingLists;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Profil)) {
            return false;
        }
        return id != null && id.equals(((Profil) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Profil{" +
            "id=" + getId() +
            ", displayName='" + getDisplayName() + "'" +
            ", photo='" + getPhoto() + "'" +
            ", photoContentType='" + getPhotoContentType() + "'" +
            "}";
    }
}
