package com.mgoulene.msha.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A SpentConfig.
 */
@Entity
@Table(name = "spent_config")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SpentConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 2, max = 40)
    @Column(name = "label", length = 40, nullable = false)
    private String label;

    @ManyToOne
    @JsonIgnoreProperties("defaultSpentConfigs")
    private Wallet wallet;

    @OneToMany(mappedBy = "defaultSpentConfig")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<SpentSharingConfig> defaultSpentSharingConfigs = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public SpentConfig label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public SpentConfig wallet(Wallet wallet) {
        this.wallet = wallet;
        return this;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }

    public Set<SpentSharingConfig> getDefaultSpentSharingConfigs() {
        return defaultSpentSharingConfigs;
    }

    public SpentConfig defaultSpentSharingConfigs(Set<SpentSharingConfig> spentSharingConfigs) {
        this.defaultSpentSharingConfigs = spentSharingConfigs;
        return this;
    }

    public SpentConfig addDefaultSpentSharingConfig(SpentSharingConfig spentSharingConfig) {
        this.defaultSpentSharingConfigs.add(spentSharingConfig);
        spentSharingConfig.setDefaultSpentConfig(this);
        return this;
    }

    public SpentConfig removeDefaultSpentSharingConfig(SpentSharingConfig spentSharingConfig) {
        this.defaultSpentSharingConfigs.remove(spentSharingConfig);
        spentSharingConfig.setDefaultSpentConfig(null);
        return this;
    }

    public void setDefaultSpentSharingConfigs(Set<SpentSharingConfig> spentSharingConfigs) {
        this.defaultSpentSharingConfigs = spentSharingConfigs;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SpentConfig)) {
            return false;
        }
        return id != null && id.equals(((SpentConfig) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "SpentConfig{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            "}";
    }
}
