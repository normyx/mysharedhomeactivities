package com.mgoulene.msha.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Wallet.
 */
@Entity
@Table(name = "wallet")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Wallet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 5, max = 400)
    @Column(name = "label", length = 400, nullable = false)
    private String label;

    @ManyToOne
    @JsonIgnoreProperties("wallets")
    private Workspace workspace;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "wallet_owner",
               joinColumns = @JoinColumn(name = "wallet_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "owner_id", referencedColumnName = "id"))
    private Set<Profil> owners = new HashSet<>();

    @OneToMany(mappedBy = "wallet")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Spent> spents = new HashSet<>();

    @OneToMany(mappedBy = "wallet")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<SpentConfig> defaultSpentConfigs = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public Wallet label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Workspace getWorkspace() {
        return workspace;
    }

    public Wallet workspace(Workspace workspace) {
        this.workspace = workspace;
        return this;
    }

    public void setWorkspace(Workspace workspace) {
        this.workspace = workspace;
    }

    public Set<Profil> getOwners() {
        return owners;
    }

    public Wallet owners(Set<Profil> profils) {
        this.owners = profils;
        return this;
    }

    public Wallet addOwner(Profil profil) {
        this.owners.add(profil);
        profil.getWallets().add(this);
        return this;
    }

    public Wallet removeOwner(Profil profil) {
        this.owners.remove(profil);
        profil.getWallets().remove(this);
        return this;
    }

    public void setOwners(Set<Profil> profils) {
        this.owners = profils;
    }

    public Set<Spent> getSpents() {
        return spents;
    }

    public Wallet spents(Set<Spent> spents) {
        this.spents = spents;
        return this;
    }

    public Wallet addSpent(Spent spent) {
        this.spents.add(spent);
        spent.setWallet(this);
        return this;
    }

    public Wallet removeSpent(Spent spent) {
        this.spents.remove(spent);
        spent.setWallet(null);
        return this;
    }

    public void setSpents(Set<Spent> spents) {
        this.spents = spents;
    }

    public Set<SpentConfig> getDefaultSpentConfigs() {
        return defaultSpentConfigs;
    }

    public Wallet defaultSpentConfigs(Set<SpentConfig> spentConfigs) {
        this.defaultSpentConfigs = spentConfigs;
        return this;
    }

    public Wallet addDefaultSpentConfig(SpentConfig spentConfig) {
        this.defaultSpentConfigs.add(spentConfig);
        spentConfig.setWallet(this);
        return this;
    }

    public Wallet removeDefaultSpentConfig(SpentConfig spentConfig) {
        this.defaultSpentConfigs.remove(spentConfig);
        spentConfig.setWallet(null);
        return this;
    }

    public void setDefaultSpentConfigs(Set<SpentConfig> spentConfigs) {
        this.defaultSpentConfigs = spentConfigs;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Wallet)) {
            return false;
        }
        return id != null && id.equals(((Wallet) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Wallet{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            "}";
    }
}
