package com.mgoulene.msha.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import org.hibernate.cache.jcache.ConfigSettings;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager) {
        return hibernateProperties -> hibernateProperties.put(ConfigSettings.CACHE_MANAGER, cacheManager);
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, com.mgoulene.msha.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, com.mgoulene.msha.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, com.mgoulene.msha.domain.User.class.getName());
            createCache(cm, com.mgoulene.msha.domain.Authority.class.getName());
            createCache(cm, com.mgoulene.msha.domain.User.class.getName() + ".authorities");
            createCache(cm, com.mgoulene.msha.domain.Workspace.class.getName());
            createCache(cm, com.mgoulene.msha.domain.Workspace.class.getName() + ".owners");
            createCache(cm, com.mgoulene.msha.domain.Workspace.class.getName() + ".taskProjects");
            createCache(cm, com.mgoulene.msha.domain.Workspace.class.getName() + ".wallets");
            createCache(cm, com.mgoulene.msha.domain.Wallet.class.getName());
            createCache(cm, com.mgoulene.msha.domain.Wallet.class.getName() + ".owners");
            createCache(cm, com.mgoulene.msha.domain.Wallet.class.getName() + ".spents");
            createCache(cm, com.mgoulene.msha.domain.Wallet.class.getName() + ".defaultSpentConfigs");
            createCache(cm, com.mgoulene.msha.domain.SpentConfig.class.getName());
            createCache(cm, com.mgoulene.msha.domain.SpentConfig.class.getName() + ".defaultSpentSharingConfigs");
            createCache(cm, com.mgoulene.msha.domain.SpentSharingConfig.class.getName());
            createCache(cm, com.mgoulene.msha.domain.Spent.class.getName());
            createCache(cm, com.mgoulene.msha.domain.Spent.class.getName() + ".sharings");
            createCache(cm, com.mgoulene.msha.domain.SpentSharing.class.getName());
            createCache(cm, com.mgoulene.msha.domain.SpentRecurrence.class.getName());
            createCache(cm, com.mgoulene.msha.domain.TaskProject.class.getName());
            createCache(cm, com.mgoulene.msha.domain.TaskProject.class.getName() + ".owners");
            createCache(cm, com.mgoulene.msha.domain.TaskProject.class.getName() + ".tasks");
            createCache(cm, com.mgoulene.msha.domain.Task.class.getName());
            createCache(cm, com.mgoulene.msha.domain.Task.class.getName() + ".owners");
            createCache(cm, com.mgoulene.msha.domain.ShoppingList.class.getName());
            createCache(cm, com.mgoulene.msha.domain.ShoppingList.class.getName() + ".owners");
            createCache(cm, com.mgoulene.msha.domain.ShoppingList.class.getName() + ".items");
            createCache(cm, com.mgoulene.msha.domain.ShoppingItem.class.getName());
            createCache(cm, com.mgoulene.msha.domain.Profil.class.getName());
            createCache(cm, com.mgoulene.msha.domain.Profil.class.getName() + ".wallets");
            createCache(cm, com.mgoulene.msha.domain.Profil.class.getName() + ".taskProjects");
            createCache(cm, com.mgoulene.msha.domain.Profil.class.getName() + ".workspaces");
            createCache(cm, com.mgoulene.msha.domain.Profil.class.getName() + ".tasks");
            createCache(cm, com.mgoulene.msha.domain.Profil.class.getName() + ".shoppingLists");
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache != null) {
            cm.destroyCache(cacheName);
        }
        cm.createCache(cacheName, jcacheConfiguration);
    }
}
