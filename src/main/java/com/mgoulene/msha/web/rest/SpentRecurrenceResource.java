package com.mgoulene.msha.web.rest;

import com.mgoulene.msha.service.SpentRecurrenceService;
import com.mgoulene.msha.web.rest.errors.BadRequestAlertException;
import com.mgoulene.msha.service.dto.SpentRecurrenceDTO;
import com.mgoulene.msha.service.dto.SpentRecurrenceCriteria;
import com.mgoulene.msha.service.SpentRecurrenceQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mgoulene.msha.domain.SpentRecurrence}.
 */
@RestController
@RequestMapping("/api")
public class SpentRecurrenceResource {

    private final Logger log = LoggerFactory.getLogger(SpentRecurrenceResource.class);

    private static final String ENTITY_NAME = "spentRecurrence";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SpentRecurrenceService spentRecurrenceService;

    private final SpentRecurrenceQueryService spentRecurrenceQueryService;

    public SpentRecurrenceResource(SpentRecurrenceService spentRecurrenceService, SpentRecurrenceQueryService spentRecurrenceQueryService) {
        this.spentRecurrenceService = spentRecurrenceService;
        this.spentRecurrenceQueryService = spentRecurrenceQueryService;
    }

    /**
     * {@code POST  /spent-recurrences} : Create a new spentRecurrence.
     *
     * @param spentRecurrenceDTO the spentRecurrenceDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new spentRecurrenceDTO, or with status {@code 400 (Bad Request)} if the spentRecurrence has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/spent-recurrences")
    public ResponseEntity<SpentRecurrenceDTO> createSpentRecurrence(@Valid @RequestBody SpentRecurrenceDTO spentRecurrenceDTO) throws URISyntaxException {
        log.debug("REST request to save SpentRecurrence : {}", spentRecurrenceDTO);
        if (spentRecurrenceDTO.getId() != null) {
            throw new BadRequestAlertException("A new spentRecurrence cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SpentRecurrenceDTO result = spentRecurrenceService.save(spentRecurrenceDTO);
        return ResponseEntity.created(new URI("/api/spent-recurrences/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /spent-recurrences} : Updates an existing spentRecurrence.
     *
     * @param spentRecurrenceDTO the spentRecurrenceDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated spentRecurrenceDTO,
     * or with status {@code 400 (Bad Request)} if the spentRecurrenceDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the spentRecurrenceDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/spent-recurrences")
    public ResponseEntity<SpentRecurrenceDTO> updateSpentRecurrence(@Valid @RequestBody SpentRecurrenceDTO spentRecurrenceDTO) throws URISyntaxException {
        log.debug("REST request to update SpentRecurrence : {}", spentRecurrenceDTO);
        if (spentRecurrenceDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SpentRecurrenceDTO result = spentRecurrenceService.save(spentRecurrenceDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, spentRecurrenceDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /spent-recurrences} : get all the spentRecurrences.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of spentRecurrences in body.
     */
    @GetMapping("/spent-recurrences")
    public ResponseEntity<List<SpentRecurrenceDTO>> getAllSpentRecurrences(SpentRecurrenceCriteria criteria) {
        log.debug("REST request to get SpentRecurrences by criteria: {}", criteria);
        List<SpentRecurrenceDTO> entityList = spentRecurrenceQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /spent-recurrences/count} : count all the spentRecurrences.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/spent-recurrences/count")
    public ResponseEntity<Long> countSpentRecurrences(SpentRecurrenceCriteria criteria) {
        log.debug("REST request to count SpentRecurrences by criteria: {}", criteria);
        return ResponseEntity.ok().body(spentRecurrenceQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /spent-recurrences/:id} : get the "id" spentRecurrence.
     *
     * @param id the id of the spentRecurrenceDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the spentRecurrenceDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/spent-recurrences/{id}")
    public ResponseEntity<SpentRecurrenceDTO> getSpentRecurrence(@PathVariable Long id) {
        log.debug("REST request to get SpentRecurrence : {}", id);
        Optional<SpentRecurrenceDTO> spentRecurrenceDTO = spentRecurrenceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(spentRecurrenceDTO);
    }

    /**
     * {@code DELETE  /spent-recurrences/:id} : delete the "id" spentRecurrence.
     *
     * @param id the id of the spentRecurrenceDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/spent-recurrences/{id}")
    public ResponseEntity<Void> deleteSpentRecurrence(@PathVariable Long id) {
        log.debug("REST request to delete SpentRecurrence : {}", id);
        spentRecurrenceService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
