package com.mgoulene.msha.web.rest;

import com.mgoulene.msha.service.ShoppingItemService;
import com.mgoulene.msha.web.rest.errors.BadRequestAlertException;
import com.mgoulene.msha.service.dto.ShoppingItemDTO;
import com.mgoulene.msha.service.dto.ShoppingItemCriteria;
import com.mgoulene.msha.service.ShoppingItemQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mgoulene.msha.domain.ShoppingItem}.
 */
@RestController
@RequestMapping("/api")
public class ShoppingItemResource {

    private final Logger log = LoggerFactory.getLogger(ShoppingItemResource.class);

    private static final String ENTITY_NAME = "shoppingItem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShoppingItemService shoppingItemService;

    private final ShoppingItemQueryService shoppingItemQueryService;

    public ShoppingItemResource(ShoppingItemService shoppingItemService, ShoppingItemQueryService shoppingItemQueryService) {
        this.shoppingItemService = shoppingItemService;
        this.shoppingItemQueryService = shoppingItemQueryService;
    }

    /**
     * {@code POST  /shopping-items} : Create a new shoppingItem.
     *
     * @param shoppingItemDTO the shoppingItemDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shoppingItemDTO, or with status {@code 400 (Bad Request)} if the shoppingItem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shopping-items")
    public ResponseEntity<ShoppingItemDTO> createShoppingItem(@Valid @RequestBody ShoppingItemDTO shoppingItemDTO) throws URISyntaxException {
        log.debug("REST request to save ShoppingItem : {}", shoppingItemDTO);
        if (shoppingItemDTO.getId() != null) {
            throw new BadRequestAlertException("A new shoppingItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShoppingItemDTO result = shoppingItemService.save(shoppingItemDTO);
        return ResponseEntity.created(new URI("/api/shopping-items/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shopping-items} : Updates an existing shoppingItem.
     *
     * @param shoppingItemDTO the shoppingItemDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shoppingItemDTO,
     * or with status {@code 400 (Bad Request)} if the shoppingItemDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the shoppingItemDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shopping-items")
    public ResponseEntity<ShoppingItemDTO> updateShoppingItem(@Valid @RequestBody ShoppingItemDTO shoppingItemDTO) throws URISyntaxException {
        log.debug("REST request to update ShoppingItem : {}", shoppingItemDTO);
        if (shoppingItemDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ShoppingItemDTO result = shoppingItemService.save(shoppingItemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shoppingItemDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /shopping-items} : get all the shoppingItems.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shoppingItems in body.
     */
    @GetMapping("/shopping-items")
    public ResponseEntity<List<ShoppingItemDTO>> getAllShoppingItems(ShoppingItemCriteria criteria) {
        log.debug("REST request to get ShoppingItems by criteria: {}", criteria);
        List<ShoppingItemDTO> entityList = shoppingItemQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /shopping-items/count} : count all the shoppingItems.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/shopping-items/count")
    public ResponseEntity<Long> countShoppingItems(ShoppingItemCriteria criteria) {
        log.debug("REST request to count ShoppingItems by criteria: {}", criteria);
        return ResponseEntity.ok().body(shoppingItemQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /shopping-items/:id} : get the "id" shoppingItem.
     *
     * @param id the id of the shoppingItemDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shoppingItemDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shopping-items/{id}")
    public ResponseEntity<ShoppingItemDTO> getShoppingItem(@PathVariable Long id) {
        log.debug("REST request to get ShoppingItem : {}", id);
        Optional<ShoppingItemDTO> shoppingItemDTO = shoppingItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(shoppingItemDTO);
    }

    /**
     * {@code DELETE  /shopping-items/:id} : delete the "id" shoppingItem.
     *
     * @param id the id of the shoppingItemDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shopping-items/{id}")
    public ResponseEntity<Void> deleteShoppingItem(@PathVariable Long id) {
        log.debug("REST request to delete ShoppingItem : {}", id);
        shoppingItemService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
