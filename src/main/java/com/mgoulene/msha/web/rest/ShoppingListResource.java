package com.mgoulene.msha.web.rest;

import com.mgoulene.msha.service.ShoppingListService;
import com.mgoulene.msha.web.rest.errors.BadRequestAlertException;
import com.mgoulene.msha.service.dto.ShoppingListDTO;
import com.mgoulene.msha.service.dto.ShoppingListCriteria;
import com.mgoulene.msha.service.ShoppingListQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mgoulene.msha.domain.ShoppingList}.
 */
@RestController
@RequestMapping("/api")
public class ShoppingListResource {

    private final Logger log = LoggerFactory.getLogger(ShoppingListResource.class);

    private static final String ENTITY_NAME = "shoppingList";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShoppingListService shoppingListService;

    private final ShoppingListQueryService shoppingListQueryService;

    public ShoppingListResource(ShoppingListService shoppingListService, ShoppingListQueryService shoppingListQueryService) {
        this.shoppingListService = shoppingListService;
        this.shoppingListQueryService = shoppingListQueryService;
    }

    /**
     * {@code POST  /shopping-lists} : Create a new shoppingList.
     *
     * @param shoppingListDTO the shoppingListDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shoppingListDTO, or with status {@code 400 (Bad Request)} if the shoppingList has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shopping-lists")
    public ResponseEntity<ShoppingListDTO> createShoppingList(@Valid @RequestBody ShoppingListDTO shoppingListDTO) throws URISyntaxException {
        log.debug("REST request to save ShoppingList : {}", shoppingListDTO);
        if (shoppingListDTO.getId() != null) {
            throw new BadRequestAlertException("A new shoppingList cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShoppingListDTO result = shoppingListService.save(shoppingListDTO);
        return ResponseEntity.created(new URI("/api/shopping-lists/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shopping-lists} : Updates an existing shoppingList.
     *
     * @param shoppingListDTO the shoppingListDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shoppingListDTO,
     * or with status {@code 400 (Bad Request)} if the shoppingListDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the shoppingListDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shopping-lists")
    public ResponseEntity<ShoppingListDTO> updateShoppingList(@Valid @RequestBody ShoppingListDTO shoppingListDTO) throws URISyntaxException {
        log.debug("REST request to update ShoppingList : {}", shoppingListDTO);
        if (shoppingListDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ShoppingListDTO result = shoppingListService.save(shoppingListDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shoppingListDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /shopping-lists} : get all the shoppingLists.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shoppingLists in body.
     */
    @GetMapping("/shopping-lists")
    public ResponseEntity<List<ShoppingListDTO>> getAllShoppingLists(ShoppingListCriteria criteria) {
        log.debug("REST request to get ShoppingLists by criteria: {}", criteria);
        List<ShoppingListDTO> entityList = shoppingListQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /shopping-lists/count} : count all the shoppingLists.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/shopping-lists/count")
    public ResponseEntity<Long> countShoppingLists(ShoppingListCriteria criteria) {
        log.debug("REST request to count ShoppingLists by criteria: {}", criteria);
        return ResponseEntity.ok().body(shoppingListQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /shopping-lists/:id} : get the "id" shoppingList.
     *
     * @param id the id of the shoppingListDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shoppingListDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shopping-lists/{id}")
    public ResponseEntity<ShoppingListDTO> getShoppingList(@PathVariable Long id) {
        log.debug("REST request to get ShoppingList : {}", id);
        Optional<ShoppingListDTO> shoppingListDTO = shoppingListService.findOne(id);
        return ResponseUtil.wrapOrNotFound(shoppingListDTO);
    }

    /**
     * {@code DELETE  /shopping-lists/:id} : delete the "id" shoppingList.
     *
     * @param id the id of the shoppingListDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shopping-lists/{id}")
    public ResponseEntity<Void> deleteShoppingList(@PathVariable Long id) {
        log.debug("REST request to delete ShoppingList : {}", id);
        shoppingListService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
