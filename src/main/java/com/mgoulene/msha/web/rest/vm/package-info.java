/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mgoulene.msha.web.rest.vm;
