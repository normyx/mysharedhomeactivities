package com.mgoulene.msha.web.rest;

import com.mgoulene.msha.service.SpentConfigService;
import com.mgoulene.msha.web.rest.errors.BadRequestAlertException;
import com.mgoulene.msha.service.dto.SpentConfigDTO;
import com.mgoulene.msha.service.dto.SpentConfigCriteria;
import com.mgoulene.msha.service.SpentConfigQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mgoulene.msha.domain.SpentConfig}.
 */
@RestController
@RequestMapping("/api")
public class SpentConfigResource {

    private final Logger log = LoggerFactory.getLogger(SpentConfigResource.class);

    private static final String ENTITY_NAME = "spentConfig";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SpentConfigService spentConfigService;

    private final SpentConfigQueryService spentConfigQueryService;

    public SpentConfigResource(SpentConfigService spentConfigService, SpentConfigQueryService spentConfigQueryService) {
        this.spentConfigService = spentConfigService;
        this.spentConfigQueryService = spentConfigQueryService;
    }

    /**
     * {@code POST  /spent-configs} : Create a new spentConfig.
     *
     * @param spentConfigDTO the spentConfigDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new spentConfigDTO, or with status {@code 400 (Bad Request)} if the spentConfig has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/spent-configs")
    public ResponseEntity<SpentConfigDTO> createSpentConfig(@Valid @RequestBody SpentConfigDTO spentConfigDTO) throws URISyntaxException {
        log.debug("REST request to save SpentConfig : {}", spentConfigDTO);
        if (spentConfigDTO.getId() != null) {
            throw new BadRequestAlertException("A new spentConfig cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SpentConfigDTO result = spentConfigService.save(spentConfigDTO);
        return ResponseEntity.created(new URI("/api/spent-configs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /spent-configs} : Updates an existing spentConfig.
     *
     * @param spentConfigDTO the spentConfigDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated spentConfigDTO,
     * or with status {@code 400 (Bad Request)} if the spentConfigDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the spentConfigDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/spent-configs")
    public ResponseEntity<SpentConfigDTO> updateSpentConfig(@Valid @RequestBody SpentConfigDTO spentConfigDTO) throws URISyntaxException {
        log.debug("REST request to update SpentConfig : {}", spentConfigDTO);
        if (spentConfigDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SpentConfigDTO result = spentConfigService.save(spentConfigDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, spentConfigDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /spent-configs} : get all the spentConfigs.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of spentConfigs in body.
     */
    @GetMapping("/spent-configs")
    public ResponseEntity<List<SpentConfigDTO>> getAllSpentConfigs(SpentConfigCriteria criteria) {
        log.debug("REST request to get SpentConfigs by criteria: {}", criteria);
        List<SpentConfigDTO> entityList = spentConfigQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /spent-configs/count} : count all the spentConfigs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/spent-configs/count")
    public ResponseEntity<Long> countSpentConfigs(SpentConfigCriteria criteria) {
        log.debug("REST request to count SpentConfigs by criteria: {}", criteria);
        return ResponseEntity.ok().body(spentConfigQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /spent-configs/:id} : get the "id" spentConfig.
     *
     * @param id the id of the spentConfigDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the spentConfigDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/spent-configs/{id}")
    public ResponseEntity<SpentConfigDTO> getSpentConfig(@PathVariable Long id) {
        log.debug("REST request to get SpentConfig : {}", id);
        Optional<SpentConfigDTO> spentConfigDTO = spentConfigService.findOne(id);
        return ResponseUtil.wrapOrNotFound(spentConfigDTO);
    }

    /**
     * {@code DELETE  /spent-configs/:id} : delete the "id" spentConfig.
     *
     * @param id the id of the spentConfigDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/spent-configs/{id}")
    public ResponseEntity<Void> deleteSpentConfig(@PathVariable Long id) {
        log.debug("REST request to delete SpentConfig : {}", id);
        spentConfigService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
