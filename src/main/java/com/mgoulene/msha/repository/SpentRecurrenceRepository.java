package com.mgoulene.msha.repository;

import com.mgoulene.msha.domain.SpentRecurrence;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SpentRecurrence entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SpentRecurrenceRepository extends JpaRepository<SpentRecurrence, Long>, JpaSpecificationExecutor<SpentRecurrence> {

}
