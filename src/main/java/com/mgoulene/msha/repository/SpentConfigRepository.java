package com.mgoulene.msha.repository;

import com.mgoulene.msha.domain.SpentConfig;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SpentConfig entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SpentConfigRepository extends JpaRepository<SpentConfig, Long>, JpaSpecificationExecutor<SpentConfig> {

}
