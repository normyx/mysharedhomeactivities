package com.mgoulene.msha.repository;

import com.mgoulene.msha.domain.SpentSharingConfig;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SpentSharingConfig entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SpentSharingConfigRepository extends JpaRepository<SpentSharingConfig, Long>, JpaSpecificationExecutor<SpentSharingConfig> {

}
