package com.mgoulene.msha.repository;

import com.mgoulene.msha.domain.ShoppingList;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the ShoppingList entity.
 */
@Repository
public interface ShoppingListRepository extends JpaRepository<ShoppingList, Long>, JpaSpecificationExecutor<ShoppingList> {

    @Query(value = "select distinct shoppingList from ShoppingList shoppingList left join fetch shoppingList.owners",
        countQuery = "select count(distinct shoppingList) from ShoppingList shoppingList")
    Page<ShoppingList> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct shoppingList from ShoppingList shoppingList left join fetch shoppingList.owners")
    List<ShoppingList> findAllWithEagerRelationships();

    @Query("select shoppingList from ShoppingList shoppingList left join fetch shoppingList.owners where shoppingList.id =:id")
    Optional<ShoppingList> findOneWithEagerRelationships(@Param("id") Long id);

}
