package com.mgoulene.msha.repository;

import com.mgoulene.msha.domain.SpentSharing;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SpentSharing entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SpentSharingRepository extends JpaRepository<SpentSharing, Long>, JpaSpecificationExecutor<SpentSharing> {

}
