package com.mgoulene.msha.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.msha.domain.ShoppingList;
import com.mgoulene.msha.domain.*; // for static metamodels
import com.mgoulene.msha.repository.ShoppingListRepository;
import com.mgoulene.msha.service.dto.ShoppingListCriteria;
import com.mgoulene.msha.service.dto.ShoppingListDTO;
import com.mgoulene.msha.service.mapper.ShoppingListMapper;

/**
 * Service for executing complex queries for {@link ShoppingList} entities in the database.
 * The main input is a {@link ShoppingListCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ShoppingListDTO} or a {@link Page} of {@link ShoppingListDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ShoppingListQueryService extends QueryService<ShoppingList> {

    private final Logger log = LoggerFactory.getLogger(ShoppingListQueryService.class);

    private final ShoppingListRepository shoppingListRepository;

    private final ShoppingListMapper shoppingListMapper;

    public ShoppingListQueryService(ShoppingListRepository shoppingListRepository, ShoppingListMapper shoppingListMapper) {
        this.shoppingListRepository = shoppingListRepository;
        this.shoppingListMapper = shoppingListMapper;
    }

    /**
     * Return a {@link List} of {@link ShoppingListDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ShoppingListDTO> findByCriteria(ShoppingListCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ShoppingList> specification = createSpecification(criteria);
        return shoppingListMapper.toDto(shoppingListRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ShoppingListDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ShoppingListDTO> findByCriteria(ShoppingListCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ShoppingList> specification = createSpecification(criteria);
        return shoppingListRepository.findAll(specification, page)
            .map(shoppingListMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ShoppingListCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ShoppingList> specification = createSpecification(criteria);
        return shoppingListRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    private Specification<ShoppingList> createSpecification(ShoppingListCriteria criteria) {
        Specification<ShoppingList> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ShoppingList_.id));
            }
            if (criteria.getLabel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLabel(), ShoppingList_.label));
            }
            if (criteria.getOwnerId() != null) {
                specification = specification.and(buildSpecification(criteria.getOwnerId(),
                    root -> root.join(ShoppingList_.owners, JoinType.LEFT).get(Profil_.id)));
            }
            if (criteria.getItemId() != null) {
                specification = specification.and(buildSpecification(criteria.getItemId(),
                    root -> root.join(ShoppingList_.items, JoinType.LEFT).get(ShoppingItem_.id)));
            }
        }
        return specification;
    }
}
