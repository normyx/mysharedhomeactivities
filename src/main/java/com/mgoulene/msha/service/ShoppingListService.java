package com.mgoulene.msha.service;

import com.mgoulene.msha.domain.ShoppingList;
import com.mgoulene.msha.repository.ShoppingListRepository;
import com.mgoulene.msha.service.dto.ShoppingListDTO;
import com.mgoulene.msha.service.mapper.ShoppingListMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link ShoppingList}.
 */
@Service
@Transactional
public class ShoppingListService {

    private final Logger log = LoggerFactory.getLogger(ShoppingListService.class);

    private final ShoppingListRepository shoppingListRepository;

    private final ShoppingListMapper shoppingListMapper;

    public ShoppingListService(ShoppingListRepository shoppingListRepository, ShoppingListMapper shoppingListMapper) {
        this.shoppingListRepository = shoppingListRepository;
        this.shoppingListMapper = shoppingListMapper;
    }

    /**
     * Save a shoppingList.
     *
     * @param shoppingListDTO the entity to save.
     * @return the persisted entity.
     */
    public ShoppingListDTO save(ShoppingListDTO shoppingListDTO) {
        log.debug("Request to save ShoppingList : {}", shoppingListDTO);
        ShoppingList shoppingList = shoppingListMapper.toEntity(shoppingListDTO);
        shoppingList = shoppingListRepository.save(shoppingList);
        return shoppingListMapper.toDto(shoppingList);
    }

    /**
     * Get all the shoppingLists.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<ShoppingListDTO> findAll() {
        log.debug("Request to get all ShoppingLists");
        return shoppingListRepository.findAllWithEagerRelationships().stream()
            .map(shoppingListMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get all the shoppingLists with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<ShoppingListDTO> findAllWithEagerRelationships(Pageable pageable) {
        return shoppingListRepository.findAllWithEagerRelationships(pageable).map(shoppingListMapper::toDto);
    }
    

    /**
     * Get one shoppingList by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ShoppingListDTO> findOne(Long id) {
        log.debug("Request to get ShoppingList : {}", id);
        return shoppingListRepository.findOneWithEagerRelationships(id)
            .map(shoppingListMapper::toDto);
    }

    /**
     * Delete the shoppingList by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ShoppingList : {}", id);
        shoppingListRepository.deleteById(id);
    }
}
