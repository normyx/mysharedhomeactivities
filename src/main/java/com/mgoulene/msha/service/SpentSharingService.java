package com.mgoulene.msha.service;

import com.mgoulene.msha.domain.SpentSharing;
import com.mgoulene.msha.repository.SpentSharingRepository;
import com.mgoulene.msha.service.dto.SpentSharingDTO;
import com.mgoulene.msha.service.mapper.SpentSharingMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link SpentSharing}.
 */
@Service
@Transactional
public class SpentSharingService {

    private final Logger log = LoggerFactory.getLogger(SpentSharingService.class);

    private final SpentSharingRepository spentSharingRepository;

    private final SpentSharingMapper spentSharingMapper;

    public SpentSharingService(SpentSharingRepository spentSharingRepository, SpentSharingMapper spentSharingMapper) {
        this.spentSharingRepository = spentSharingRepository;
        this.spentSharingMapper = spentSharingMapper;
    }

    /**
     * Save a spentSharing.
     *
     * @param spentSharingDTO the entity to save.
     * @return the persisted entity.
     */
    public SpentSharingDTO save(SpentSharingDTO spentSharingDTO) {
        log.debug("Request to save SpentSharing : {}", spentSharingDTO);
        SpentSharing spentSharing = spentSharingMapper.toEntity(spentSharingDTO);
        spentSharing = spentSharingRepository.save(spentSharing);
        return spentSharingMapper.toDto(spentSharing);
    }

    /**
     * Get all the spentSharings.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<SpentSharingDTO> findAll() {
        log.debug("Request to get all SpentSharings");
        return spentSharingRepository.findAll().stream()
            .map(spentSharingMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one spentSharing by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SpentSharingDTO> findOne(Long id) {
        log.debug("Request to get SpentSharing : {}", id);
        return spentSharingRepository.findById(id)
            .map(spentSharingMapper::toDto);
    }

    /**
     * Delete the spentSharing by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete SpentSharing : {}", id);
        spentSharingRepository.deleteById(id);
    }
}
