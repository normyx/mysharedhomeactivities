package com.mgoulene.msha.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.msha.domain.SpentSharing;
import com.mgoulene.msha.domain.*; // for static metamodels
import com.mgoulene.msha.repository.SpentSharingRepository;
import com.mgoulene.msha.service.dto.SpentSharingCriteria;
import com.mgoulene.msha.service.dto.SpentSharingDTO;
import com.mgoulene.msha.service.mapper.SpentSharingMapper;

/**
 * Service for executing complex queries for {@link SpentSharing} entities in the database.
 * The main input is a {@link SpentSharingCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SpentSharingDTO} or a {@link Page} of {@link SpentSharingDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SpentSharingQueryService extends QueryService<SpentSharing> {

    private final Logger log = LoggerFactory.getLogger(SpentSharingQueryService.class);

    private final SpentSharingRepository spentSharingRepository;

    private final SpentSharingMapper spentSharingMapper;

    public SpentSharingQueryService(SpentSharingRepository spentSharingRepository, SpentSharingMapper spentSharingMapper) {
        this.spentSharingRepository = spentSharingRepository;
        this.spentSharingMapper = spentSharingMapper;
    }

    /**
     * Return a {@link List} of {@link SpentSharingDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SpentSharingDTO> findByCriteria(SpentSharingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<SpentSharing> specification = createSpecification(criteria);
        return spentSharingMapper.toDto(spentSharingRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SpentSharingDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SpentSharingDTO> findByCriteria(SpentSharingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<SpentSharing> specification = createSpecification(criteria);
        return spentSharingRepository.findAll(specification, page)
            .map(spentSharingMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SpentSharingCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<SpentSharing> specification = createSpecification(criteria);
        return spentSharingRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    private Specification<SpentSharing> createSpecification(SpentSharingCriteria criteria) {
        Specification<SpentSharing> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SpentSharing_.id));
            }
            if (criteria.getAmountShare() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAmountShare(), SpentSharing_.amountShare));
            }
            if (criteria.getShare() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getShare(), SpentSharing_.share));
            }
            if (criteria.getSharingUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getSharingUserId(),
                    root -> root.join(SpentSharing_.sharingUser, JoinType.LEFT).get(Profil_.id)));
            }
            if (criteria.getSpentId() != null) {
                specification = specification.and(buildSpecification(criteria.getSpentId(),
                    root -> root.join(SpentSharing_.spent, JoinType.LEFT).get(Spent_.id)));
            }
        }
        return specification;
    }
}
