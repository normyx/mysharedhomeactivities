package com.mgoulene.msha.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.mgoulene.msha.domain.SpentSharing} entity. This class is used
 * in {@link com.mgoulene.msha.web.rest.SpentSharingResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /spent-sharings?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SpentSharingCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private FloatFilter amountShare;

    private FloatFilter share;

    private LongFilter sharingUserId;

    private LongFilter spentId;

    public SpentSharingCriteria(){
    }

    public SpentSharingCriteria(SpentSharingCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.amountShare = other.amountShare == null ? null : other.amountShare.copy();
        this.share = other.share == null ? null : other.share.copy();
        this.sharingUserId = other.sharingUserId == null ? null : other.sharingUserId.copy();
        this.spentId = other.spentId == null ? null : other.spentId.copy();
    }

    @Override
    public SpentSharingCriteria copy() {
        return new SpentSharingCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public FloatFilter getAmountShare() {
        return amountShare;
    }

    public void setAmountShare(FloatFilter amountShare) {
        this.amountShare = amountShare;
    }

    public FloatFilter getShare() {
        return share;
    }

    public void setShare(FloatFilter share) {
        this.share = share;
    }

    public LongFilter getSharingUserId() {
        return sharingUserId;
    }

    public void setSharingUserId(LongFilter sharingUserId) {
        this.sharingUserId = sharingUserId;
    }

    public LongFilter getSpentId() {
        return spentId;
    }

    public void setSpentId(LongFilter spentId) {
        this.spentId = spentId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SpentSharingCriteria that = (SpentSharingCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(amountShare, that.amountShare) &&
            Objects.equals(share, that.share) &&
            Objects.equals(sharingUserId, that.sharingUserId) &&
            Objects.equals(spentId, that.spentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        amountShare,
        share,
        sharingUserId,
        spentId
        );
    }

    @Override
    public String toString() {
        return "SpentSharingCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (amountShare != null ? "amountShare=" + amountShare + ", " : "") +
                (share != null ? "share=" + share + ", " : "") +
                (sharingUserId != null ? "sharingUserId=" + sharingUserId + ", " : "") +
                (spentId != null ? "spentId=" + spentId + ", " : "") +
            "}";
    }

}
