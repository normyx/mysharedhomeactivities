package com.mgoulene.msha.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.mgoulene.msha.domain.Profil} entity. This class is used
 * in {@link com.mgoulene.msha.web.rest.ProfilResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /profils?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProfilCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter displayName;

    private LongFilter userId;

    private LongFilter walletId;

    private LongFilter taskProjectId;

    private LongFilter workspaceId;

    private LongFilter taskId;

    private LongFilter shoppingListId;

    public ProfilCriteria(){
    }

    public ProfilCriteria(ProfilCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.displayName = other.displayName == null ? null : other.displayName.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.walletId = other.walletId == null ? null : other.walletId.copy();
        this.taskProjectId = other.taskProjectId == null ? null : other.taskProjectId.copy();
        this.workspaceId = other.workspaceId == null ? null : other.workspaceId.copy();
        this.taskId = other.taskId == null ? null : other.taskId.copy();
        this.shoppingListId = other.shoppingListId == null ? null : other.shoppingListId.copy();
    }

    @Override
    public ProfilCriteria copy() {
        return new ProfilCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDisplayName() {
        return displayName;
    }

    public void setDisplayName(StringFilter displayName) {
        this.displayName = displayName;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getWalletId() {
        return walletId;
    }

    public void setWalletId(LongFilter walletId) {
        this.walletId = walletId;
    }

    public LongFilter getTaskProjectId() {
        return taskProjectId;
    }

    public void setTaskProjectId(LongFilter taskProjectId) {
        this.taskProjectId = taskProjectId;
    }

    public LongFilter getWorkspaceId() {
        return workspaceId;
    }

    public void setWorkspaceId(LongFilter workspaceId) {
        this.workspaceId = workspaceId;
    }

    public LongFilter getTaskId() {
        return taskId;
    }

    public void setTaskId(LongFilter taskId) {
        this.taskId = taskId;
    }

    public LongFilter getShoppingListId() {
        return shoppingListId;
    }

    public void setShoppingListId(LongFilter shoppingListId) {
        this.shoppingListId = shoppingListId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProfilCriteria that = (ProfilCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(displayName, that.displayName) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(walletId, that.walletId) &&
            Objects.equals(taskProjectId, that.taskProjectId) &&
            Objects.equals(workspaceId, that.workspaceId) &&
            Objects.equals(taskId, that.taskId) &&
            Objects.equals(shoppingListId, that.shoppingListId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        displayName,
        userId,
        walletId,
        taskProjectId,
        workspaceId,
        taskId,
        shoppingListId
        );
    }

    @Override
    public String toString() {
        return "ProfilCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (displayName != null ? "displayName=" + displayName + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (walletId != null ? "walletId=" + walletId + ", " : "") +
                (taskProjectId != null ? "taskProjectId=" + taskProjectId + ", " : "") +
                (workspaceId != null ? "workspaceId=" + workspaceId + ", " : "") +
                (taskId != null ? "taskId=" + taskId + ", " : "") +
                (shoppingListId != null ? "shoppingListId=" + shoppingListId + ", " : "") +
            "}";
    }

}
