package com.mgoulene.msha.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.mgoulene.msha.domain.ShoppingItem} entity. This class is used
 * in {@link com.mgoulene.msha.web.rest.ShoppingItemResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /shopping-items?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ShoppingItemCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter label;

    private BooleanFilter done;

    private LongFilter listId;

    public ShoppingItemCriteria(){
    }

    public ShoppingItemCriteria(ShoppingItemCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.label = other.label == null ? null : other.label.copy();
        this.done = other.done == null ? null : other.done.copy();
        this.listId = other.listId == null ? null : other.listId.copy();
    }

    @Override
    public ShoppingItemCriteria copy() {
        return new ShoppingItemCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getLabel() {
        return label;
    }

    public void setLabel(StringFilter label) {
        this.label = label;
    }

    public BooleanFilter getDone() {
        return done;
    }

    public void setDone(BooleanFilter done) {
        this.done = done;
    }

    public LongFilter getListId() {
        return listId;
    }

    public void setListId(LongFilter listId) {
        this.listId = listId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ShoppingItemCriteria that = (ShoppingItemCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(label, that.label) &&
            Objects.equals(done, that.done) &&
            Objects.equals(listId, that.listId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        label,
        done,
        listId
        );
    }

    @Override
    public String toString() {
        return "ShoppingItemCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (label != null ? "label=" + label + ", " : "") +
                (done != null ? "done=" + done + ", " : "") +
                (listId != null ? "listId=" + listId + ", " : "") +
            "}";
    }

}
