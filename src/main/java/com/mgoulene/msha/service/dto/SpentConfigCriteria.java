package com.mgoulene.msha.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.mgoulene.msha.domain.SpentConfig} entity. This class is used
 * in {@link com.mgoulene.msha.web.rest.SpentConfigResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /spent-configs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SpentConfigCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter label;

    private LongFilter walletId;

    private LongFilter defaultSpentSharingConfigId;

    public SpentConfigCriteria(){
    }

    public SpentConfigCriteria(SpentConfigCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.label = other.label == null ? null : other.label.copy();
        this.walletId = other.walletId == null ? null : other.walletId.copy();
        this.defaultSpentSharingConfigId = other.defaultSpentSharingConfigId == null ? null : other.defaultSpentSharingConfigId.copy();
    }

    @Override
    public SpentConfigCriteria copy() {
        return new SpentConfigCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getLabel() {
        return label;
    }

    public void setLabel(StringFilter label) {
        this.label = label;
    }

    public LongFilter getWalletId() {
        return walletId;
    }

    public void setWalletId(LongFilter walletId) {
        this.walletId = walletId;
    }

    public LongFilter getDefaultSpentSharingConfigId() {
        return defaultSpentSharingConfigId;
    }

    public void setDefaultSpentSharingConfigId(LongFilter defaultSpentSharingConfigId) {
        this.defaultSpentSharingConfigId = defaultSpentSharingConfigId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SpentConfigCriteria that = (SpentConfigCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(label, that.label) &&
            Objects.equals(walletId, that.walletId) &&
            Objects.equals(defaultSpentSharingConfigId, that.defaultSpentSharingConfigId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        label,
        walletId,
        defaultSpentSharingConfigId
        );
    }

    @Override
    public String toString() {
        return "SpentConfigCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (label != null ? "label=" + label + ", " : "") +
                (walletId != null ? "walletId=" + walletId + ", " : "") +
                (defaultSpentSharingConfigId != null ? "defaultSpentSharingConfigId=" + defaultSpentSharingConfigId + ", " : "") +
            "}";
    }

}
