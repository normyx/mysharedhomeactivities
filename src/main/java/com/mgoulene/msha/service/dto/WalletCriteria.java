package com.mgoulene.msha.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.mgoulene.msha.domain.Wallet} entity. This class is used
 * in {@link com.mgoulene.msha.web.rest.WalletResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /wallets?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class WalletCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter label;

    private LongFilter workspaceId;

    private LongFilter ownerId;

    private LongFilter spentId;

    private LongFilter defaultSpentConfigId;

    public WalletCriteria(){
    }

    public WalletCriteria(WalletCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.label = other.label == null ? null : other.label.copy();
        this.workspaceId = other.workspaceId == null ? null : other.workspaceId.copy();
        this.ownerId = other.ownerId == null ? null : other.ownerId.copy();
        this.spentId = other.spentId == null ? null : other.spentId.copy();
        this.defaultSpentConfigId = other.defaultSpentConfigId == null ? null : other.defaultSpentConfigId.copy();
    }

    @Override
    public WalletCriteria copy() {
        return new WalletCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getLabel() {
        return label;
    }

    public void setLabel(StringFilter label) {
        this.label = label;
    }

    public LongFilter getWorkspaceId() {
        return workspaceId;
    }

    public void setWorkspaceId(LongFilter workspaceId) {
        this.workspaceId = workspaceId;
    }

    public LongFilter getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(LongFilter ownerId) {
        this.ownerId = ownerId;
    }

    public LongFilter getSpentId() {
        return spentId;
    }

    public void setSpentId(LongFilter spentId) {
        this.spentId = spentId;
    }

    public LongFilter getDefaultSpentConfigId() {
        return defaultSpentConfigId;
    }

    public void setDefaultSpentConfigId(LongFilter defaultSpentConfigId) {
        this.defaultSpentConfigId = defaultSpentConfigId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final WalletCriteria that = (WalletCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(label, that.label) &&
            Objects.equals(workspaceId, that.workspaceId) &&
            Objects.equals(ownerId, that.ownerId) &&
            Objects.equals(spentId, that.spentId) &&
            Objects.equals(defaultSpentConfigId, that.defaultSpentConfigId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        label,
        workspaceId,
        ownerId,
        spentId,
        defaultSpentConfigId
        );
    }

    @Override
    public String toString() {
        return "WalletCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (label != null ? "label=" + label + ", " : "") +
                (workspaceId != null ? "workspaceId=" + workspaceId + ", " : "") +
                (ownerId != null ? "ownerId=" + ownerId + ", " : "") +
                (spentId != null ? "spentId=" + spentId + ", " : "") +
                (defaultSpentConfigId != null ? "defaultSpentConfigId=" + defaultSpentConfigId + ", " : "") +
            "}";
    }

}
