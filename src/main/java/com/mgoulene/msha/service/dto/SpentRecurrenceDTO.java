package com.mgoulene.msha.service.dto;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.mgoulene.msha.domain.SpentRecurrence} entity.
 */
public class SpentRecurrenceDTO implements Serializable {

    private Long id;

    @NotNull
    private Float amount;

    @NotNull
    private LocalDate recurrenceFrom;

    private LocalDate recurrenceTo;

    @NotNull
    @Min(value = 1)
    @Max(value = 31)
    private Integer dayInMonth;


    private Long spentId;

    private String spentLabel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public LocalDate getRecurrenceFrom() {
        return recurrenceFrom;
    }

    public void setRecurrenceFrom(LocalDate recurrenceFrom) {
        this.recurrenceFrom = recurrenceFrom;
    }

    public LocalDate getRecurrenceTo() {
        return recurrenceTo;
    }

    public void setRecurrenceTo(LocalDate recurrenceTo) {
        this.recurrenceTo = recurrenceTo;
    }

    public Integer getDayInMonth() {
        return dayInMonth;
    }

    public void setDayInMonth(Integer dayInMonth) {
        this.dayInMonth = dayInMonth;
    }

    public Long getSpentId() {
        return spentId;
    }

    public void setSpentId(Long spentId) {
        this.spentId = spentId;
    }

    public String getSpentLabel() {
        return spentLabel;
    }

    public void setSpentLabel(String spentLabel) {
        this.spentLabel = spentLabel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SpentRecurrenceDTO spentRecurrenceDTO = (SpentRecurrenceDTO) o;
        if (spentRecurrenceDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), spentRecurrenceDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SpentRecurrenceDTO{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", recurrenceFrom='" + getRecurrenceFrom() + "'" +
            ", recurrenceTo='" + getRecurrenceTo() + "'" +
            ", dayInMonth=" + getDayInMonth() +
            ", spent=" + getSpentId() +
            ", spent='" + getSpentLabel() + "'" +
            "}";
    }
}
