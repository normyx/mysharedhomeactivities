package com.mgoulene.msha.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.mgoulene.msha.domain.SpentSharing} entity.
 */
public class SpentSharingDTO implements Serializable {

    private Long id;

    @NotNull
    private Float amountShare;

    @NotNull
    private Float share;


    private Long sharingUserId;

    private String sharingUserDisplayName;

    private Long spentId;

    private String spentLabel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getAmountShare() {
        return amountShare;
    }

    public void setAmountShare(Float amountShare) {
        this.amountShare = amountShare;
    }

    public Float getShare() {
        return share;
    }

    public void setShare(Float share) {
        this.share = share;
    }

    public Long getSharingUserId() {
        return sharingUserId;
    }

    public void setSharingUserId(Long profilId) {
        this.sharingUserId = profilId;
    }

    public String getSharingUserDisplayName() {
        return sharingUserDisplayName;
    }

    public void setSharingUserDisplayName(String profilDisplayName) {
        this.sharingUserDisplayName = profilDisplayName;
    }

    public Long getSpentId() {
        return spentId;
    }

    public void setSpentId(Long spentId) {
        this.spentId = spentId;
    }

    public String getSpentLabel() {
        return spentLabel;
    }

    public void setSpentLabel(String spentLabel) {
        this.spentLabel = spentLabel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SpentSharingDTO spentSharingDTO = (SpentSharingDTO) o;
        if (spentSharingDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), spentSharingDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SpentSharingDTO{" +
            "id=" + getId() +
            ", amountShare=" + getAmountShare() +
            ", share=" + getShare() +
            ", sharingUser=" + getSharingUserId() +
            ", sharingUser='" + getSharingUserDisplayName() + "'" +
            ", spent=" + getSpentId() +
            ", spent='" + getSpentLabel() + "'" +
            "}";
    }
}
