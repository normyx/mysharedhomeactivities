package com.mgoulene.msha.service.dto;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.mgoulene.msha.domain.Spent} entity.
 */
public class SpentDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 2, max = 100)
    private String label;

    @NotNull
    private Float amount;

    @NotNull
    private LocalDate spentDate;

    @NotNull
    private Boolean validated;


    private Long spenderId;

    private String spenderDisplayName;

    private Long walletId;

    private String walletLabel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public LocalDate getSpentDate() {
        return spentDate;
    }

    public void setSpentDate(LocalDate spentDate) {
        this.spentDate = spentDate;
    }

    public Boolean isValidated() {
        return validated;
    }

    public void setValidated(Boolean validated) {
        this.validated = validated;
    }

    public Long getSpenderId() {
        return spenderId;
    }

    public void setSpenderId(Long profilId) {
        this.spenderId = profilId;
    }

    public String getSpenderDisplayName() {
        return spenderDisplayName;
    }

    public void setSpenderDisplayName(String profilDisplayName) {
        this.spenderDisplayName = profilDisplayName;
    }

    public Long getWalletId() {
        return walletId;
    }

    public void setWalletId(Long walletId) {
        this.walletId = walletId;
    }

    public String getWalletLabel() {
        return walletLabel;
    }

    public void setWalletLabel(String walletLabel) {
        this.walletLabel = walletLabel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SpentDTO spentDTO = (SpentDTO) o;
        if (spentDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), spentDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SpentDTO{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", amount=" + getAmount() +
            ", spentDate='" + getSpentDate() + "'" +
            ", validated='" + isValidated() + "'" +
            ", spender=" + getSpenderId() +
            ", spender='" + getSpenderDisplayName() + "'" +
            ", wallet=" + getWalletId() +
            ", wallet='" + getWalletLabel() + "'" +
            "}";
    }
}
