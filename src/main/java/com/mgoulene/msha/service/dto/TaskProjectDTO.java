package com.mgoulene.msha.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the {@link com.mgoulene.msha.domain.TaskProject} entity.
 */
public class TaskProjectDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 5, max = 40)
    private String label;


    private Long workspaceId;

    private String workspaceLabel;

    private Set<ProfilDTO> owners = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Long getWorkspaceId() {
        return workspaceId;
    }

    public void setWorkspaceId(Long workspaceId) {
        this.workspaceId = workspaceId;
    }

    public String getWorkspaceLabel() {
        return workspaceLabel;
    }

    public void setWorkspaceLabel(String workspaceLabel) {
        this.workspaceLabel = workspaceLabel;
    }

    public Set<ProfilDTO> getOwners() {
        return owners;
    }

    public void setOwners(Set<ProfilDTO> profils) {
        this.owners = profils;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TaskProjectDTO taskProjectDTO = (TaskProjectDTO) o;
        if (taskProjectDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), taskProjectDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TaskProjectDTO{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", workspace=" + getWorkspaceId() +
            ", workspace='" + getWorkspaceLabel() + "'" +
            "}";
    }
}
