package com.mgoulene.msha.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.mgoulene.msha.domain.SpentRecurrence} entity. This class is used
 * in {@link com.mgoulene.msha.web.rest.SpentRecurrenceResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /spent-recurrences?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SpentRecurrenceCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private FloatFilter amount;

    private LocalDateFilter recurrenceFrom;

    private LocalDateFilter recurrenceTo;

    private IntegerFilter dayInMonth;

    private LongFilter spentId;

    public SpentRecurrenceCriteria(){
    }

    public SpentRecurrenceCriteria(SpentRecurrenceCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.amount = other.amount == null ? null : other.amount.copy();
        this.recurrenceFrom = other.recurrenceFrom == null ? null : other.recurrenceFrom.copy();
        this.recurrenceTo = other.recurrenceTo == null ? null : other.recurrenceTo.copy();
        this.dayInMonth = other.dayInMonth == null ? null : other.dayInMonth.copy();
        this.spentId = other.spentId == null ? null : other.spentId.copy();
    }

    @Override
    public SpentRecurrenceCriteria copy() {
        return new SpentRecurrenceCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public FloatFilter getAmount() {
        return amount;
    }

    public void setAmount(FloatFilter amount) {
        this.amount = amount;
    }

    public LocalDateFilter getRecurrenceFrom() {
        return recurrenceFrom;
    }

    public void setRecurrenceFrom(LocalDateFilter recurrenceFrom) {
        this.recurrenceFrom = recurrenceFrom;
    }

    public LocalDateFilter getRecurrenceTo() {
        return recurrenceTo;
    }

    public void setRecurrenceTo(LocalDateFilter recurrenceTo) {
        this.recurrenceTo = recurrenceTo;
    }

    public IntegerFilter getDayInMonth() {
        return dayInMonth;
    }

    public void setDayInMonth(IntegerFilter dayInMonth) {
        this.dayInMonth = dayInMonth;
    }

    public LongFilter getSpentId() {
        return spentId;
    }

    public void setSpentId(LongFilter spentId) {
        this.spentId = spentId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SpentRecurrenceCriteria that = (SpentRecurrenceCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(amount, that.amount) &&
            Objects.equals(recurrenceFrom, that.recurrenceFrom) &&
            Objects.equals(recurrenceTo, that.recurrenceTo) &&
            Objects.equals(dayInMonth, that.dayInMonth) &&
            Objects.equals(spentId, that.spentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        amount,
        recurrenceFrom,
        recurrenceTo,
        dayInMonth,
        spentId
        );
    }

    @Override
    public String toString() {
        return "SpentRecurrenceCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (amount != null ? "amount=" + amount + ", " : "") +
                (recurrenceFrom != null ? "recurrenceFrom=" + recurrenceFrom + ", " : "") +
                (recurrenceTo != null ? "recurrenceTo=" + recurrenceTo + ", " : "") +
                (dayInMonth != null ? "dayInMonth=" + dayInMonth + ", " : "") +
                (spentId != null ? "spentId=" + spentId + ", " : "") +
            "}";
    }

}
