package com.mgoulene.msha.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.mgoulene.msha.domain.ShoppingItem} entity.
 */
public class ShoppingItemDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 2, max = 100)
    private String label;

    @NotNull
    private Boolean done;


    private Long listId;

    private String listLabel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Boolean isDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public Long getListId() {
        return listId;
    }

    public void setListId(Long shoppingListId) {
        this.listId = shoppingListId;
    }

    public String getListLabel() {
        return listLabel;
    }

    public void setListLabel(String shoppingListLabel) {
        this.listLabel = shoppingListLabel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ShoppingItemDTO shoppingItemDTO = (ShoppingItemDTO) o;
        if (shoppingItemDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), shoppingItemDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ShoppingItemDTO{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", done='" + isDone() + "'" +
            ", list=" + getListId() +
            ", list='" + getListLabel() + "'" +
            "}";
    }
}
