package com.mgoulene.msha.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.mgoulene.msha.domain.SpentSharingConfig} entity. This class is used
 * in {@link com.mgoulene.msha.web.rest.SpentSharingConfigResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /spent-sharing-configs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SpentSharingConfigCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private FloatFilter share;

    private LongFilter sharingUserId;

    private LongFilter defaultSpentConfigId;

    public SpentSharingConfigCriteria(){
    }

    public SpentSharingConfigCriteria(SpentSharingConfigCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.share = other.share == null ? null : other.share.copy();
        this.sharingUserId = other.sharingUserId == null ? null : other.sharingUserId.copy();
        this.defaultSpentConfigId = other.defaultSpentConfigId == null ? null : other.defaultSpentConfigId.copy();
    }

    @Override
    public SpentSharingConfigCriteria copy() {
        return new SpentSharingConfigCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public FloatFilter getShare() {
        return share;
    }

    public void setShare(FloatFilter share) {
        this.share = share;
    }

    public LongFilter getSharingUserId() {
        return sharingUserId;
    }

    public void setSharingUserId(LongFilter sharingUserId) {
        this.sharingUserId = sharingUserId;
    }

    public LongFilter getDefaultSpentConfigId() {
        return defaultSpentConfigId;
    }

    public void setDefaultSpentConfigId(LongFilter defaultSpentConfigId) {
        this.defaultSpentConfigId = defaultSpentConfigId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SpentSharingConfigCriteria that = (SpentSharingConfigCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(share, that.share) &&
            Objects.equals(sharingUserId, that.sharingUserId) &&
            Objects.equals(defaultSpentConfigId, that.defaultSpentConfigId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        share,
        sharingUserId,
        defaultSpentConfigId
        );
    }

    @Override
    public String toString() {
        return "SpentSharingConfigCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (share != null ? "share=" + share + ", " : "") +
                (sharingUserId != null ? "sharingUserId=" + sharingUserId + ", " : "") +
                (defaultSpentConfigId != null ? "defaultSpentConfigId=" + defaultSpentConfigId + ", " : "") +
            "}";
    }

}
