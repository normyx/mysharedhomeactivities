package com.mgoulene.msha.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.mgoulene.msha.domain.Spent} entity. This class is used
 * in {@link com.mgoulene.msha.web.rest.SpentResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /spents?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SpentCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter label;

    private FloatFilter amount;

    private LocalDateFilter spentDate;

    private BooleanFilter validated;

    private LongFilter spenderId;

    private LongFilter walletId;

    private LongFilter recurrenceId;

    private LongFilter sharingId;

    public SpentCriteria(){
    }

    public SpentCriteria(SpentCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.label = other.label == null ? null : other.label.copy();
        this.amount = other.amount == null ? null : other.amount.copy();
        this.spentDate = other.spentDate == null ? null : other.spentDate.copy();
        this.validated = other.validated == null ? null : other.validated.copy();
        this.spenderId = other.spenderId == null ? null : other.spenderId.copy();
        this.walletId = other.walletId == null ? null : other.walletId.copy();
        this.recurrenceId = other.recurrenceId == null ? null : other.recurrenceId.copy();
        this.sharingId = other.sharingId == null ? null : other.sharingId.copy();
    }

    @Override
    public SpentCriteria copy() {
        return new SpentCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getLabel() {
        return label;
    }

    public void setLabel(StringFilter label) {
        this.label = label;
    }

    public FloatFilter getAmount() {
        return amount;
    }

    public void setAmount(FloatFilter amount) {
        this.amount = amount;
    }

    public LocalDateFilter getSpentDate() {
        return spentDate;
    }

    public void setSpentDate(LocalDateFilter spentDate) {
        this.spentDate = spentDate;
    }

    public BooleanFilter getValidated() {
        return validated;
    }

    public void setValidated(BooleanFilter validated) {
        this.validated = validated;
    }

    public LongFilter getSpenderId() {
        return spenderId;
    }

    public void setSpenderId(LongFilter spenderId) {
        this.spenderId = spenderId;
    }

    public LongFilter getWalletId() {
        return walletId;
    }

    public void setWalletId(LongFilter walletId) {
        this.walletId = walletId;
    }

    public LongFilter getRecurrenceId() {
        return recurrenceId;
    }

    public void setRecurrenceId(LongFilter recurrenceId) {
        this.recurrenceId = recurrenceId;
    }

    public LongFilter getSharingId() {
        return sharingId;
    }

    public void setSharingId(LongFilter sharingId) {
        this.sharingId = sharingId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SpentCriteria that = (SpentCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(label, that.label) &&
            Objects.equals(amount, that.amount) &&
            Objects.equals(spentDate, that.spentDate) &&
            Objects.equals(validated, that.validated) &&
            Objects.equals(spenderId, that.spenderId) &&
            Objects.equals(walletId, that.walletId) &&
            Objects.equals(recurrenceId, that.recurrenceId) &&
            Objects.equals(sharingId, that.sharingId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        label,
        amount,
        spentDate,
        validated,
        spenderId,
        walletId,
        recurrenceId,
        sharingId
        );
    }

    @Override
    public String toString() {
        return "SpentCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (label != null ? "label=" + label + ", " : "") +
                (amount != null ? "amount=" + amount + ", " : "") +
                (spentDate != null ? "spentDate=" + spentDate + ", " : "") +
                (validated != null ? "validated=" + validated + ", " : "") +
                (spenderId != null ? "spenderId=" + spenderId + ", " : "") +
                (walletId != null ? "walletId=" + walletId + ", " : "") +
                (recurrenceId != null ? "recurrenceId=" + recurrenceId + ", " : "") +
                (sharingId != null ? "sharingId=" + sharingId + ", " : "") +
            "}";
    }

}
