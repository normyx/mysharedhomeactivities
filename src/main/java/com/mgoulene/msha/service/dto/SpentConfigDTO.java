package com.mgoulene.msha.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.mgoulene.msha.domain.SpentConfig} entity.
 */
public class SpentConfigDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 2, max = 40)
    private String label;


    private Long walletId;

    private String walletLabel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Long getWalletId() {
        return walletId;
    }

    public void setWalletId(Long walletId) {
        this.walletId = walletId;
    }

    public String getWalletLabel() {
        return walletLabel;
    }

    public void setWalletLabel(String walletLabel) {
        this.walletLabel = walletLabel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SpentConfigDTO spentConfigDTO = (SpentConfigDTO) o;
        if (spentConfigDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), spentConfigDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SpentConfigDTO{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", wallet=" + getWalletId() +
            ", wallet='" + getWalletLabel() + "'" +
            "}";
    }
}
