package com.mgoulene.msha.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.mgoulene.msha.domain.SpentSharingConfig} entity.
 */
public class SpentSharingConfigDTO implements Serializable {

    private Long id;

    @NotNull
    private Float share;


    private Long sharingUserId;

    private String sharingUserDisplayName;

    private Long defaultSpentConfigId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getShare() {
        return share;
    }

    public void setShare(Float share) {
        this.share = share;
    }

    public Long getSharingUserId() {
        return sharingUserId;
    }

    public void setSharingUserId(Long profilId) {
        this.sharingUserId = profilId;
    }

    public String getSharingUserDisplayName() {
        return sharingUserDisplayName;
    }

    public void setSharingUserDisplayName(String profilDisplayName) {
        this.sharingUserDisplayName = profilDisplayName;
    }

    public Long getDefaultSpentConfigId() {
        return defaultSpentConfigId;
    }

    public void setDefaultSpentConfigId(Long spentConfigId) {
        this.defaultSpentConfigId = spentConfigId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SpentSharingConfigDTO spentSharingConfigDTO = (SpentSharingConfigDTO) o;
        if (spentSharingConfigDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), spentSharingConfigDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SpentSharingConfigDTO{" +
            "id=" + getId() +
            ", share=" + getShare() +
            ", sharingUser=" + getSharingUserId() +
            ", sharingUser='" + getSharingUserDisplayName() + "'" +
            ", defaultSpentConfig=" + getDefaultSpentConfigId() +
            "}";
    }
}
