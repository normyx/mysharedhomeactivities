package com.mgoulene.msha.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.msha.domain.SpentRecurrence;
import com.mgoulene.msha.domain.*; // for static metamodels
import com.mgoulene.msha.repository.SpentRecurrenceRepository;
import com.mgoulene.msha.service.dto.SpentRecurrenceCriteria;
import com.mgoulene.msha.service.dto.SpentRecurrenceDTO;
import com.mgoulene.msha.service.mapper.SpentRecurrenceMapper;

/**
 * Service for executing complex queries for {@link SpentRecurrence} entities in the database.
 * The main input is a {@link SpentRecurrenceCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SpentRecurrenceDTO} or a {@link Page} of {@link SpentRecurrenceDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SpentRecurrenceQueryService extends QueryService<SpentRecurrence> {

    private final Logger log = LoggerFactory.getLogger(SpentRecurrenceQueryService.class);

    private final SpentRecurrenceRepository spentRecurrenceRepository;

    private final SpentRecurrenceMapper spentRecurrenceMapper;

    public SpentRecurrenceQueryService(SpentRecurrenceRepository spentRecurrenceRepository, SpentRecurrenceMapper spentRecurrenceMapper) {
        this.spentRecurrenceRepository = spentRecurrenceRepository;
        this.spentRecurrenceMapper = spentRecurrenceMapper;
    }

    /**
     * Return a {@link List} of {@link SpentRecurrenceDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SpentRecurrenceDTO> findByCriteria(SpentRecurrenceCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<SpentRecurrence> specification = createSpecification(criteria);
        return spentRecurrenceMapper.toDto(spentRecurrenceRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SpentRecurrenceDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SpentRecurrenceDTO> findByCriteria(SpentRecurrenceCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<SpentRecurrence> specification = createSpecification(criteria);
        return spentRecurrenceRepository.findAll(specification, page)
            .map(spentRecurrenceMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SpentRecurrenceCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<SpentRecurrence> specification = createSpecification(criteria);
        return spentRecurrenceRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    private Specification<SpentRecurrence> createSpecification(SpentRecurrenceCriteria criteria) {
        Specification<SpentRecurrence> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SpentRecurrence_.id));
            }
            if (criteria.getAmount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAmount(), SpentRecurrence_.amount));
            }
            if (criteria.getRecurrenceFrom() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRecurrenceFrom(), SpentRecurrence_.recurrenceFrom));
            }
            if (criteria.getRecurrenceTo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRecurrenceTo(), SpentRecurrence_.recurrenceTo));
            }
            if (criteria.getDayInMonth() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDayInMonth(), SpentRecurrence_.dayInMonth));
            }
            if (criteria.getSpentId() != null) {
                specification = specification.and(buildSpecification(criteria.getSpentId(),
                    root -> root.join(SpentRecurrence_.spent, JoinType.LEFT).get(Spent_.id)));
            }
        }
        return specification;
    }
}
