package com.mgoulene.msha.service.mapper;

import com.mgoulene.msha.domain.*;
import com.mgoulene.msha.service.dto.WorkspaceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Workspace} and its DTO {@link WorkspaceDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProfilMapper.class})
public interface WorkspaceMapper extends EntityMapper<WorkspaceDTO, Workspace> {


    @Mapping(target = "removeOwner", ignore = true)
    @Mapping(target = "taskProjects", ignore = true)
    @Mapping(target = "removeTaskProject", ignore = true)
    @Mapping(target = "wallets", ignore = true)
    @Mapping(target = "removeWallet", ignore = true)
    Workspace toEntity(WorkspaceDTO workspaceDTO);

    default Workspace fromId(Long id) {
        if (id == null) {
            return null;
        }
        Workspace workspace = new Workspace();
        workspace.setId(id);
        return workspace;
    }
}
