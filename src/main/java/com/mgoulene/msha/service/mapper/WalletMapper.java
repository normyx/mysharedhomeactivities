package com.mgoulene.msha.service.mapper;

import com.mgoulene.msha.domain.*;
import com.mgoulene.msha.service.dto.WalletDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Wallet} and its DTO {@link WalletDTO}.
 */
@Mapper(componentModel = "spring", uses = {WorkspaceMapper.class, ProfilMapper.class})
public interface WalletMapper extends EntityMapper<WalletDTO, Wallet> {

    @Mapping(source = "workspace.id", target = "workspaceId")
    @Mapping(source = "workspace.label", target = "workspaceLabel")
    WalletDTO toDto(Wallet wallet);

    @Mapping(source = "workspaceId", target = "workspace")
    @Mapping(target = "removeOwner", ignore = true)
    @Mapping(target = "spents", ignore = true)
    @Mapping(target = "removeSpent", ignore = true)
    @Mapping(target = "defaultSpentConfigs", ignore = true)
    @Mapping(target = "removeDefaultSpentConfig", ignore = true)
    Wallet toEntity(WalletDTO walletDTO);

    default Wallet fromId(Long id) {
        if (id == null) {
            return null;
        }
        Wallet wallet = new Wallet();
        wallet.setId(id);
        return wallet;
    }
}
