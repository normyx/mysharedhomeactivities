package com.mgoulene.msha.service.mapper;

import com.mgoulene.msha.domain.*;
import com.mgoulene.msha.service.dto.ProfilDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Profil} and its DTO {@link ProfilDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface ProfilMapper extends EntityMapper<ProfilDTO, Profil> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    ProfilDTO toDto(Profil profil);

    @Mapping(source = "userId", target = "user")
    @Mapping(target = "wallets", ignore = true)
    @Mapping(target = "removeWallet", ignore = true)
    @Mapping(target = "taskProjects", ignore = true)
    @Mapping(target = "removeTaskProject", ignore = true)
    @Mapping(target = "workspaces", ignore = true)
    @Mapping(target = "removeWorkspace", ignore = true)
    @Mapping(target = "tasks", ignore = true)
    @Mapping(target = "removeTask", ignore = true)
    @Mapping(target = "shoppingLists", ignore = true)
    @Mapping(target = "removeShoppingList", ignore = true)
    Profil toEntity(ProfilDTO profilDTO);

    default Profil fromId(Long id) {
        if (id == null) {
            return null;
        }
        Profil profil = new Profil();
        profil.setId(id);
        return profil;
    }
}
