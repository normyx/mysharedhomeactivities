package com.mgoulene.msha.service.mapper;

import com.mgoulene.msha.domain.*;
import com.mgoulene.msha.service.dto.SpentSharingConfigDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link SpentSharingConfig} and its DTO {@link SpentSharingConfigDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProfilMapper.class, SpentConfigMapper.class})
public interface SpentSharingConfigMapper extends EntityMapper<SpentSharingConfigDTO, SpentSharingConfig> {

    @Mapping(source = "sharingUser.id", target = "sharingUserId")
    @Mapping(source = "sharingUser.displayName", target = "sharingUserDisplayName")
    @Mapping(source = "defaultSpentConfig.id", target = "defaultSpentConfigId")
    SpentSharingConfigDTO toDto(SpentSharingConfig spentSharingConfig);

    @Mapping(source = "sharingUserId", target = "sharingUser")
    @Mapping(source = "defaultSpentConfigId", target = "defaultSpentConfig")
    SpentSharingConfig toEntity(SpentSharingConfigDTO spentSharingConfigDTO);

    default SpentSharingConfig fromId(Long id) {
        if (id == null) {
            return null;
        }
        SpentSharingConfig spentSharingConfig = new SpentSharingConfig();
        spentSharingConfig.setId(id);
        return spentSharingConfig;
    }
}
