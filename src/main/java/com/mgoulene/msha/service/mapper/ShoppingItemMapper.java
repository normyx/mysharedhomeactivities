package com.mgoulene.msha.service.mapper;

import com.mgoulene.msha.domain.*;
import com.mgoulene.msha.service.dto.ShoppingItemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ShoppingItem} and its DTO {@link ShoppingItemDTO}.
 */
@Mapper(componentModel = "spring", uses = {ShoppingListMapper.class})
public interface ShoppingItemMapper extends EntityMapper<ShoppingItemDTO, ShoppingItem> {

    @Mapping(source = "list.id", target = "listId")
    @Mapping(source = "list.label", target = "listLabel")
    ShoppingItemDTO toDto(ShoppingItem shoppingItem);

    @Mapping(source = "listId", target = "list")
    ShoppingItem toEntity(ShoppingItemDTO shoppingItemDTO);

    default ShoppingItem fromId(Long id) {
        if (id == null) {
            return null;
        }
        ShoppingItem shoppingItem = new ShoppingItem();
        shoppingItem.setId(id);
        return shoppingItem;
    }
}
