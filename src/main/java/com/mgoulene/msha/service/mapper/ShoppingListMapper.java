package com.mgoulene.msha.service.mapper;

import com.mgoulene.msha.domain.*;
import com.mgoulene.msha.service.dto.ShoppingListDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ShoppingList} and its DTO {@link ShoppingListDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProfilMapper.class})
public interface ShoppingListMapper extends EntityMapper<ShoppingListDTO, ShoppingList> {


    @Mapping(target = "removeOwner", ignore = true)
    @Mapping(target = "items", ignore = true)
    @Mapping(target = "removeItem", ignore = true)
    ShoppingList toEntity(ShoppingListDTO shoppingListDTO);

    default ShoppingList fromId(Long id) {
        if (id == null) {
            return null;
        }
        ShoppingList shoppingList = new ShoppingList();
        shoppingList.setId(id);
        return shoppingList;
    }
}
