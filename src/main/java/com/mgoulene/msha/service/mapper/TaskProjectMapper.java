package com.mgoulene.msha.service.mapper;

import com.mgoulene.msha.domain.*;
import com.mgoulene.msha.service.dto.TaskProjectDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TaskProject} and its DTO {@link TaskProjectDTO}.
 */
@Mapper(componentModel = "spring", uses = {WorkspaceMapper.class, ProfilMapper.class})
public interface TaskProjectMapper extends EntityMapper<TaskProjectDTO, TaskProject> {

    @Mapping(source = "workspace.id", target = "workspaceId")
    @Mapping(source = "workspace.label", target = "workspaceLabel")
    TaskProjectDTO toDto(TaskProject taskProject);

    @Mapping(source = "workspaceId", target = "workspace")
    @Mapping(target = "removeOwner", ignore = true)
    @Mapping(target = "tasks", ignore = true)
    @Mapping(target = "removeTask", ignore = true)
    TaskProject toEntity(TaskProjectDTO taskProjectDTO);

    default TaskProject fromId(Long id) {
        if (id == null) {
            return null;
        }
        TaskProject taskProject = new TaskProject();
        taskProject.setId(id);
        return taskProject;
    }
}
