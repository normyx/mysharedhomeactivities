package com.mgoulene.msha.service.mapper;

import com.mgoulene.msha.domain.*;
import com.mgoulene.msha.service.dto.SpentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Spent} and its DTO {@link SpentDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProfilMapper.class, WalletMapper.class})
public interface SpentMapper extends EntityMapper<SpentDTO, Spent> {

    @Mapping(source = "spender.id", target = "spenderId")
    @Mapping(source = "spender.displayName", target = "spenderDisplayName")
    @Mapping(source = "wallet.id", target = "walletId")
    @Mapping(source = "wallet.label", target = "walletLabel")
    SpentDTO toDto(Spent spent);

    @Mapping(source = "spenderId", target = "spender")
    @Mapping(source = "walletId", target = "wallet")
    @Mapping(target = "recurrence", ignore = true)
    @Mapping(target = "sharings", ignore = true)
    @Mapping(target = "removeSharing", ignore = true)
    Spent toEntity(SpentDTO spentDTO);

    default Spent fromId(Long id) {
        if (id == null) {
            return null;
        }
        Spent spent = new Spent();
        spent.setId(id);
        return spent;
    }
}
