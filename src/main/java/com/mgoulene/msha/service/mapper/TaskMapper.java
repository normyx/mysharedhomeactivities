package com.mgoulene.msha.service.mapper;

import com.mgoulene.msha.domain.*;
import com.mgoulene.msha.service.dto.TaskDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Task} and its DTO {@link TaskDTO}.
 */
@Mapper(componentModel = "spring", uses = {TaskProjectMapper.class, ProfilMapper.class})
public interface TaskMapper extends EntityMapper<TaskDTO, Task> {

    @Mapping(source = "taskProject.id", target = "taskProjectId")
    @Mapping(source = "taskProject.label", target = "taskProjectLabel")
    TaskDTO toDto(Task task);

    @Mapping(source = "taskProjectId", target = "taskProject")
    @Mapping(target = "removeOwner", ignore = true)
    Task toEntity(TaskDTO taskDTO);

    default Task fromId(Long id) {
        if (id == null) {
            return null;
        }
        Task task = new Task();
        task.setId(id);
        return task;
    }
}
