package com.mgoulene.msha.service.mapper;

import com.mgoulene.msha.domain.*;
import com.mgoulene.msha.service.dto.SpentRecurrenceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link SpentRecurrence} and its DTO {@link SpentRecurrenceDTO}.
 */
@Mapper(componentModel = "spring", uses = {SpentMapper.class})
public interface SpentRecurrenceMapper extends EntityMapper<SpentRecurrenceDTO, SpentRecurrence> {

    @Mapping(source = "spent.id", target = "spentId")
    @Mapping(source = "spent.label", target = "spentLabel")
    SpentRecurrenceDTO toDto(SpentRecurrence spentRecurrence);

    @Mapping(source = "spentId", target = "spent")
    SpentRecurrence toEntity(SpentRecurrenceDTO spentRecurrenceDTO);

    default SpentRecurrence fromId(Long id) {
        if (id == null) {
            return null;
        }
        SpentRecurrence spentRecurrence = new SpentRecurrence();
        spentRecurrence.setId(id);
        return spentRecurrence;
    }
}
