package com.mgoulene.msha.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.msha.domain.SpentSharingConfig;
import com.mgoulene.msha.domain.*; // for static metamodels
import com.mgoulene.msha.repository.SpentSharingConfigRepository;
import com.mgoulene.msha.service.dto.SpentSharingConfigCriteria;
import com.mgoulene.msha.service.dto.SpentSharingConfigDTO;
import com.mgoulene.msha.service.mapper.SpentSharingConfigMapper;

/**
 * Service for executing complex queries for {@link SpentSharingConfig} entities in the database.
 * The main input is a {@link SpentSharingConfigCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SpentSharingConfigDTO} or a {@link Page} of {@link SpentSharingConfigDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SpentSharingConfigQueryService extends QueryService<SpentSharingConfig> {

    private final Logger log = LoggerFactory.getLogger(SpentSharingConfigQueryService.class);

    private final SpentSharingConfigRepository spentSharingConfigRepository;

    private final SpentSharingConfigMapper spentSharingConfigMapper;

    public SpentSharingConfigQueryService(SpentSharingConfigRepository spentSharingConfigRepository, SpentSharingConfigMapper spentSharingConfigMapper) {
        this.spentSharingConfigRepository = spentSharingConfigRepository;
        this.spentSharingConfigMapper = spentSharingConfigMapper;
    }

    /**
     * Return a {@link List} of {@link SpentSharingConfigDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SpentSharingConfigDTO> findByCriteria(SpentSharingConfigCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<SpentSharingConfig> specification = createSpecification(criteria);
        return spentSharingConfigMapper.toDto(spentSharingConfigRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SpentSharingConfigDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SpentSharingConfigDTO> findByCriteria(SpentSharingConfigCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<SpentSharingConfig> specification = createSpecification(criteria);
        return spentSharingConfigRepository.findAll(specification, page)
            .map(spentSharingConfigMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SpentSharingConfigCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<SpentSharingConfig> specification = createSpecification(criteria);
        return spentSharingConfigRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    private Specification<SpentSharingConfig> createSpecification(SpentSharingConfigCriteria criteria) {
        Specification<SpentSharingConfig> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), SpentSharingConfig_.id));
            }
            if (criteria.getShare() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getShare(), SpentSharingConfig_.share));
            }
            if (criteria.getSharingUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getSharingUserId(),
                    root -> root.join(SpentSharingConfig_.sharingUser, JoinType.LEFT).get(Profil_.id)));
            }
            if (criteria.getDefaultSpentConfigId() != null) {
                specification = specification.and(buildSpecification(criteria.getDefaultSpentConfigId(),
                    root -> root.join(SpentSharingConfig_.defaultSpentConfig, JoinType.LEFT).get(SpentConfig_.id)));
            }
        }
        return specification;
    }
}
