package com.mgoulene.msha.service;

import com.mgoulene.msha.domain.SpentRecurrence;
import com.mgoulene.msha.repository.SpentRecurrenceRepository;
import com.mgoulene.msha.service.dto.SpentRecurrenceDTO;
import com.mgoulene.msha.service.mapper.SpentRecurrenceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link SpentRecurrence}.
 */
@Service
@Transactional
public class SpentRecurrenceService {

    private final Logger log = LoggerFactory.getLogger(SpentRecurrenceService.class);

    private final SpentRecurrenceRepository spentRecurrenceRepository;

    private final SpentRecurrenceMapper spentRecurrenceMapper;

    public SpentRecurrenceService(SpentRecurrenceRepository spentRecurrenceRepository, SpentRecurrenceMapper spentRecurrenceMapper) {
        this.spentRecurrenceRepository = spentRecurrenceRepository;
        this.spentRecurrenceMapper = spentRecurrenceMapper;
    }

    /**
     * Save a spentRecurrence.
     *
     * @param spentRecurrenceDTO the entity to save.
     * @return the persisted entity.
     */
    public SpentRecurrenceDTO save(SpentRecurrenceDTO spentRecurrenceDTO) {
        log.debug("Request to save SpentRecurrence : {}", spentRecurrenceDTO);
        SpentRecurrence spentRecurrence = spentRecurrenceMapper.toEntity(spentRecurrenceDTO);
        spentRecurrence = spentRecurrenceRepository.save(spentRecurrence);
        return spentRecurrenceMapper.toDto(spentRecurrence);
    }

    /**
     * Get all the spentRecurrences.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<SpentRecurrenceDTO> findAll() {
        log.debug("Request to get all SpentRecurrences");
        return spentRecurrenceRepository.findAll().stream()
            .map(spentRecurrenceMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one spentRecurrence by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SpentRecurrenceDTO> findOne(Long id) {
        log.debug("Request to get SpentRecurrence : {}", id);
        return spentRecurrenceRepository.findById(id)
            .map(spentRecurrenceMapper::toDto);
    }

    /**
     * Delete the spentRecurrence by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete SpentRecurrence : {}", id);
        spentRecurrenceRepository.deleteById(id);
    }
}
