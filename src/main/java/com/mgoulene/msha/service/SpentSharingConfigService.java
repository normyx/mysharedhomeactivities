package com.mgoulene.msha.service;

import com.mgoulene.msha.domain.SpentSharingConfig;
import com.mgoulene.msha.repository.SpentSharingConfigRepository;
import com.mgoulene.msha.service.dto.SpentSharingConfigDTO;
import com.mgoulene.msha.service.mapper.SpentSharingConfigMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link SpentSharingConfig}.
 */
@Service
@Transactional
public class SpentSharingConfigService {

    private final Logger log = LoggerFactory.getLogger(SpentSharingConfigService.class);

    private final SpentSharingConfigRepository spentSharingConfigRepository;

    private final SpentSharingConfigMapper spentSharingConfigMapper;

    public SpentSharingConfigService(SpentSharingConfigRepository spentSharingConfigRepository, SpentSharingConfigMapper spentSharingConfigMapper) {
        this.spentSharingConfigRepository = spentSharingConfigRepository;
        this.spentSharingConfigMapper = spentSharingConfigMapper;
    }

    /**
     * Save a spentSharingConfig.
     *
     * @param spentSharingConfigDTO the entity to save.
     * @return the persisted entity.
     */
    public SpentSharingConfigDTO save(SpentSharingConfigDTO spentSharingConfigDTO) {
        log.debug("Request to save SpentSharingConfig : {}", spentSharingConfigDTO);
        SpentSharingConfig spentSharingConfig = spentSharingConfigMapper.toEntity(spentSharingConfigDTO);
        spentSharingConfig = spentSharingConfigRepository.save(spentSharingConfig);
        return spentSharingConfigMapper.toDto(spentSharingConfig);
    }

    /**
     * Get all the spentSharingConfigs.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<SpentSharingConfigDTO> findAll() {
        log.debug("Request to get all SpentSharingConfigs");
        return spentSharingConfigRepository.findAll().stream()
            .map(spentSharingConfigMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one spentSharingConfig by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SpentSharingConfigDTO> findOne(Long id) {
        log.debug("Request to get SpentSharingConfig : {}", id);
        return spentSharingConfigRepository.findById(id)
            .map(spentSharingConfigMapper::toDto);
    }

    /**
     * Delete the spentSharingConfig by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete SpentSharingConfig : {}", id);
        spentSharingConfigRepository.deleteById(id);
    }
}
