package com.mgoulene.msha.service;

import com.mgoulene.msha.domain.Spent;
import com.mgoulene.msha.repository.SpentRepository;
import com.mgoulene.msha.service.dto.SpentDTO;
import com.mgoulene.msha.service.mapper.SpentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing {@link Spent}.
 */
@Service
@Transactional
public class SpentService {

    private final Logger log = LoggerFactory.getLogger(SpentService.class);

    private final SpentRepository spentRepository;

    private final SpentMapper spentMapper;

    public SpentService(SpentRepository spentRepository, SpentMapper spentMapper) {
        this.spentRepository = spentRepository;
        this.spentMapper = spentMapper;
    }

    /**
     * Save a spent.
     *
     * @param spentDTO the entity to save.
     * @return the persisted entity.
     */
    public SpentDTO save(SpentDTO spentDTO) {
        log.debug("Request to save Spent : {}", spentDTO);
        Spent spent = spentMapper.toEntity(spentDTO);
        spent = spentRepository.save(spent);
        return spentMapper.toDto(spent);
    }

    /**
     * Get all the spents.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<SpentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Spents");
        return spentRepository.findAll(pageable)
            .map(spentMapper::toDto);
    }



    /**
    *  Get all the spents where Recurrence is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true) 
    public List<SpentDTO> findAllWhereRecurrenceIsNull() {
        log.debug("Request to get all spents where Recurrence is null");
        return StreamSupport
            .stream(spentRepository.findAll().spliterator(), false)
            .filter(spent -> spent.getRecurrence() == null)
            .map(spentMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one spent by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SpentDTO> findOne(Long id) {
        log.debug("Request to get Spent : {}", id);
        return spentRepository.findById(id)
            .map(spentMapper::toDto);
    }

    /**
     * Delete the spent by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Spent : {}", id);
        spentRepository.deleteById(id);
    }
}
