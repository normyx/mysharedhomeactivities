import { NgModule } from '@angular/core';

import { MySharedHomeActivitiesSharedLibsModule, FindLanguageFromKeyPipe, MshaAlertComponent, MshaAlertErrorComponent } from './';

@NgModule({
  imports: [MySharedHomeActivitiesSharedLibsModule],
  declarations: [FindLanguageFromKeyPipe, MshaAlertComponent, MshaAlertErrorComponent],
  exports: [MySharedHomeActivitiesSharedLibsModule, FindLanguageFromKeyPipe, MshaAlertComponent, MshaAlertErrorComponent]
})
export class MySharedHomeActivitiesSharedCommonModule {}
