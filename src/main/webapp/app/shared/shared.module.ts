import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MySharedHomeActivitiesSharedCommonModule, MshaLoginModalComponent, HasAnyAuthorityDirective, MySharedHomeActivitiesSharedLibsModule } from './';

import { MySHAMaterialModule } from './mysha-material.module';

@NgModule({
  imports: [MySharedHomeActivitiesSharedLibsModule, MySharedHomeActivitiesSharedCommonModule, MySHAMaterialModule],
  declarations: [MshaLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [MshaLoginModalComponent],
  exports: [MySharedHomeActivitiesSharedCommonModule, MySHAMaterialModule, MshaLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MySharedHomeActivitiesSharedModule {
  static forRoot() {
    return {
      ngModule: MySharedHomeActivitiesSharedModule
    };
  }
}
