export interface IShoppingItem {
  id?: number;
  label?: string;
  done?: boolean;
  listLabel?: string;
  listId?: number;
}

export class ShoppingItem implements IShoppingItem {
  constructor(public id?: number, public label?: string, public done?: boolean, public listLabel?: string, public listId?: number) {
    this.done = this.done || false;
  }
}
