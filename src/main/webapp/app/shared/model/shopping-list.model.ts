import { IProfil } from 'app/shared/model/profil.model';
import { IShoppingItem } from 'app/shared/model/shopping-item.model';

export interface IShoppingList {
  id?: number;
  label?: string;
  owners?: IProfil[];
  items?: IShoppingItem[];
}

export class ShoppingList implements IShoppingList {
  constructor(public id?: number, public label?: string, public owners?: IProfil[], public items?: IShoppingItem[]) {}
}
