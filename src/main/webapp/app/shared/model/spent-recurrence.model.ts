import { Moment } from 'moment';

export interface ISpentRecurrence {
  id?: number;
  amount?: number;
  recurrenceFrom?: Moment;
  recurrenceTo?: Moment;
  dayInMonth?: number;
  spentLabel?: string;
  spentId?: number;
}

export class SpentRecurrence implements ISpentRecurrence {
  constructor(
    public id?: number,
    public amount?: number,
    public recurrenceFrom?: Moment,
    public recurrenceTo?: Moment,
    public dayInMonth?: number,
    public spentLabel?: string,
    public spentId?: number
  ) {}
}
