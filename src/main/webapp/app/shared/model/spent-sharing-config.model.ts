export interface ISpentSharingConfig {
  id?: number;
  share?: number;
  sharingUserDisplayName?: string;
  sharingUserId?: number;
  defaultSpentConfigId?: number;
}

export class SpentSharingConfig implements ISpentSharingConfig {
  constructor(
    public id?: number,
    public share?: number,
    public sharingUserDisplayName?: string,
    public sharingUserId?: number,
    public defaultSpentConfigId?: number
  ) {}
}
