import { ISpentSharingConfig } from 'app/shared/model/spent-sharing-config.model';

export interface ISpentConfig {
  id?: number;
  label?: string;
  walletLabel?: string;
  walletId?: number;
  defaultSpentSharingConfigs?: ISpentSharingConfig[];
}

export class SpentConfig implements ISpentConfig {
  constructor(
    public id?: number,
    public label?: string,
    public walletLabel?: string,
    public walletId?: number,
    public defaultSpentSharingConfigs?: ISpentSharingConfig[]
  ) {}
}
