import { IWallet } from 'app/shared/model/wallet.model';
import { ITaskProject } from 'app/shared/model/task-project.model';
import { IWorkspace } from 'app/shared/model/workspace.model';
import { ITask } from 'app/shared/model/task.model';
import { IShoppingList } from 'app/shared/model/shopping-list.model';

export interface IProfil {
  id?: number;
  displayName?: string;
  photoContentType?: string;
  photo?: any;
  userLogin?: string;
  userId?: number;
  wallets?: IWallet[];
  taskProjects?: ITaskProject[];
  workspaces?: IWorkspace[];
  tasks?: ITask[];
  shoppingLists?: IShoppingList[];
}

export class Profil implements IProfil {
  constructor(
    public id?: number,
    public displayName?: string,
    public photoContentType?: string,
    public photo?: any,
    public userLogin?: string,
    public userId?: number,
    public wallets?: IWallet[],
    public taskProjects?: ITaskProject[],
    public workspaces?: IWorkspace[],
    public tasks?: ITask[],
    public shoppingLists?: IShoppingList[]
  ) {}
}
