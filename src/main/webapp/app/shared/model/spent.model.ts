import { Moment } from 'moment';
import { ISpentSharing } from 'app/shared/model/spent-sharing.model';

export interface ISpent {
  id?: number;
  label?: string;
  amount?: number;
  spentDate?: Moment;
  validated?: boolean;
  spenderDisplayName?: string;
  spenderId?: number;
  walletLabel?: string;
  walletId?: number;
  recurrenceId?: number;
  sharings?: ISpentSharing[];
}

export class Spent implements ISpent {
  constructor(
    public id?: number,
    public label?: string,
    public amount?: number,
    public spentDate?: Moment,
    public validated?: boolean,
    public spenderDisplayName?: string,
    public spenderId?: number,
    public walletLabel?: string,
    public walletId?: number,
    public recurrenceId?: number,
    public sharings?: ISpentSharing[]
  ) {
    this.validated = this.validated || false;
  }
}
