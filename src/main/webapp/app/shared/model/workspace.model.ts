import { IProfil } from 'app/shared/model/profil.model';
import { ITaskProject } from 'app/shared/model/task-project.model';
import { IWallet } from 'app/shared/model/wallet.model';

export interface IWorkspace {
  id?: number;
  label?: string;
  owners?: IProfil[];
  taskProjects?: ITaskProject[];
  wallets?: IWallet[];
}

export class Workspace implements IWorkspace {
  constructor(
    public id?: number,
    public label?: string,
    public owners?: IProfil[],
    public taskProjects?: ITaskProject[],
    public wallets?: IWallet[]
  ) {}
}
