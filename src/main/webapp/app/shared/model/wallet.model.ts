import { IProfil } from 'app/shared/model/profil.model';
import { ISpent } from 'app/shared/model/spent.model';
import { ISpentConfig } from 'app/shared/model/spent-config.model';

export interface IWallet {
  id?: number;
  label?: string;
  workspaceLabel?: string;
  workspaceId?: number;
  owners?: IProfil[];
  spents?: ISpent[];
  defaultSpentConfigs?: ISpentConfig[];
}

export class Wallet implements IWallet {
  constructor(
    public id?: number,
    public label?: string,
    public workspaceLabel?: string,
    public workspaceId?: number,
    public owners?: IProfil[],
    public spents?: ISpent[],
    public defaultSpentConfigs?: ISpentConfig[]
  ) {}
}
