export interface ISpentSharing {
  id?: number;
  amountShare?: number;
  share?: number;
  sharingUserDisplayName?: string;
  sharingUserId?: number;
  spentLabel?: string;
  spentId?: number;
}

export class SpentSharing implements ISpentSharing {
  constructor(
    public id?: number,
    public amountShare?: number,
    public share?: number,
    public sharingUserDisplayName?: string,
    public sharingUserId?: number,
    public spentLabel?: string,
    public spentId?: number
  ) {}
}
