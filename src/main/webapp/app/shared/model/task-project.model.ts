import { IProfil } from 'app/shared/model/profil.model';
import { ITask } from 'app/shared/model/task.model';

export interface ITaskProject {
  id?: number;
  label?: string;
  workspaceLabel?: string;
  workspaceId?: number;
  owners?: IProfil[];
  tasks?: ITask[];
}

export class TaskProject implements ITaskProject {
  constructor(
    public id?: number,
    public label?: string,
    public workspaceLabel?: string,
    public workspaceId?: number,
    public owners?: IProfil[],
    public tasks?: ITask[]
  ) {}
}
