import { Moment } from 'moment';
import { IProfil } from 'app/shared/model/profil.model';

export interface ITask {
  id?: number;
  label?: string;
  description?: string;
  done?: boolean;
  dueDate?: Moment;
  priority?: number;
  taskProjectLabel?: string;
  taskProjectId?: number;
  owners?: IProfil[];
}

export class Task implements ITask {
  constructor(
    public id?: number,
    public label?: string,
    public description?: string,
    public done?: boolean,
    public dueDate?: Moment,
    public priority?: number,
    public taskProjectLabel?: string,
    public taskProjectId?: number,
    public owners?: IProfil[]
  ) {
    this.done = this.done || false;
  }
}
