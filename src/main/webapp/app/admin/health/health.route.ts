import { Route } from '@angular/router';

import { MshaHealthCheckComponent } from './health.component';

export const healthRoute: Route = {
  path: 'msha-health',
  component: MshaHealthCheckComponent,
  data: {
    pageTitle: 'health.title'
  }
};
