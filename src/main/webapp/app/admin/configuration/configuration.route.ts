import { Route } from '@angular/router';

import { MshaConfigurationComponent } from './configuration.component';

export const configurationRoute: Route = {
  path: 'msha-configuration',
  component: MshaConfigurationComponent,
  data: {
    pageTitle: 'configuration.title'
  }
};
