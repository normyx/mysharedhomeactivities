import { Route } from '@angular/router';

import { MshaDocsComponent } from './docs.component';

export const docsRoute: Route = {
  path: 'docs',
  component: MshaDocsComponent,
  data: {
    pageTitle: 'global.menu.admin.apidocs'
  }
};
