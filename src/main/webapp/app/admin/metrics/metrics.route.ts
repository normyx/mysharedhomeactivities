import { Route } from '@angular/router';

import { MshaMetricsMonitoringComponent } from './metrics.component';

export const metricsRoute: Route = {
  path: 'msha-metrics',
  component: MshaMetricsMonitoringComponent,
  data: {
    pageTitle: 'metrics.title'
  }
};
