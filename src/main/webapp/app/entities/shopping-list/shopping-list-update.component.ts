import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IShoppingList, ShoppingList } from 'app/shared/model/shopping-list.model';
import { ShoppingListService } from './shopping-list.service';
import { IProfil } from 'app/shared/model/profil.model';
import { ProfilService } from 'app/entities/profil';

@Component({
  selector: 'msha-shopping-list-update',
  templateUrl: './shopping-list-update.component.html'
})
export class ShoppingListUpdateComponent implements OnInit {
  isSaving: boolean;

  profils: IProfil[];

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(100)]],
    owners: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected shoppingListService: ShoppingListService,
    protected profilService: ProfilService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ shoppingList }) => {
      this.updateForm(shoppingList);
    });
    this.profilService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IProfil[]>) => mayBeOk.ok),
        map((response: HttpResponse<IProfil[]>) => response.body)
      )
      .subscribe((res: IProfil[]) => (this.profils = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(shoppingList: IShoppingList) {
    this.editForm.patchValue({
      id: shoppingList.id,
      label: shoppingList.label,
      owners: shoppingList.owners
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const shoppingList = this.createFromForm();
    if (shoppingList.id !== undefined) {
      this.subscribeToSaveResponse(this.shoppingListService.update(shoppingList));
    } else {
      this.subscribeToSaveResponse(this.shoppingListService.create(shoppingList));
    }
  }

  private createFromForm(): IShoppingList {
    return {
      ...new ShoppingList(),
      id: this.editForm.get(['id']).value,
      label: this.editForm.get(['label']).value,
      owners: this.editForm.get(['owners']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShoppingList>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackProfilById(index: number, item: IProfil) {
    return item.id;
  }

  getSelected(selectedVals: Array<any>, option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
