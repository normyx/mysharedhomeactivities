import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IShoppingList } from 'app/shared/model/shopping-list.model';
import { AccountService } from 'app/core';
import { ShoppingListService } from './shopping-list.service';

@Component({
  selector: 'msha-shopping-list',
  templateUrl: './shopping-list.component.html'
})
export class ShoppingListComponent implements OnInit, OnDestroy {
  shoppingLists: IShoppingList[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected shoppingListService: ShoppingListService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.shoppingListService
      .query()
      .pipe(
        filter((res: HttpResponse<IShoppingList[]>) => res.ok),
        map((res: HttpResponse<IShoppingList[]>) => res.body)
      )
      .subscribe(
        (res: IShoppingList[]) => {
          this.shoppingLists = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInShoppingLists();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IShoppingList) {
    return item.id;
  }

  registerChangeInShoppingLists() {
    this.eventSubscriber = this.eventManager.subscribe('shoppingListListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
