import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Spent } from 'app/shared/model/spent.model';
import { SpentService } from './spent.service';
import { SpentComponent } from './spent.component';
import { SpentDetailComponent } from './spent-detail.component';
import { SpentUpdateComponent } from './spent-update.component';
import { SpentDeletePopupComponent } from './spent-delete-dialog.component';
import { ISpent } from 'app/shared/model/spent.model';

@Injectable({ providedIn: 'root' })
export class SpentResolve implements Resolve<ISpent> {
  constructor(private service: SpentService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ISpent> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Spent>) => response.ok),
        map((spent: HttpResponse<Spent>) => spent.body)
      );
    }
    return of(new Spent());
  }
}

export const spentRoute: Routes = [
  {
    path: '',
    component: SpentComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spent.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SpentDetailComponent,
    resolve: {
      spent: SpentResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spent.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SpentUpdateComponent,
    resolve: {
      spent: SpentResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spent.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SpentUpdateComponent,
    resolve: {
      spent: SpentResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spent.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const spentPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: SpentDeletePopupComponent,
    resolve: {
      spent: SpentResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spent.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
