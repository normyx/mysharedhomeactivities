import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISpent } from 'app/shared/model/spent.model';
import { SpentService } from './spent.service';

@Component({
  selector: 'msha-spent-delete-dialog',
  templateUrl: './spent-delete-dialog.component.html'
})
export class SpentDeleteDialogComponent {
  spent: ISpent;

  constructor(protected spentService: SpentService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.spentService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'spentListModification',
        content: 'Deleted an spent'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'msha-spent-delete-popup',
  template: ''
})
export class SpentDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ spent }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(SpentDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.spent = spent;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/spent', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/spent', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
