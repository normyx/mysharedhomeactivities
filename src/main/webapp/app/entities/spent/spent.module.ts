import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { MySharedHomeActivitiesSharedModule } from 'app/shared';
import {
  SpentComponent,
  SpentDetailComponent,
  SpentUpdateComponent,
  SpentDeletePopupComponent,
  SpentDeleteDialogComponent,
  spentRoute,
  spentPopupRoute
} from './';

const ENTITY_STATES = [...spentRoute, ...spentPopupRoute];

@NgModule({
  imports: [MySharedHomeActivitiesSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [SpentComponent, SpentDetailComponent, SpentUpdateComponent, SpentDeleteDialogComponent, SpentDeletePopupComponent],
  entryComponents: [SpentComponent, SpentUpdateComponent, SpentDeleteDialogComponent, SpentDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MySharedHomeActivitiesSpentModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
