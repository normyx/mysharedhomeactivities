import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISpent } from 'app/shared/model/spent.model';

@Component({
  selector: 'msha-spent-detail',
  templateUrl: './spent-detail.component.html'
})
export class SpentDetailComponent implements OnInit {
  spent: ISpent;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ spent }) => {
      this.spent = spent;
    });
  }

  previousState() {
    window.history.back();
  }
}
