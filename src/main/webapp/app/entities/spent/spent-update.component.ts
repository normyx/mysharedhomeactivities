import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { ISpent, Spent } from 'app/shared/model/spent.model';
import { SpentService } from './spent.service';
import { IProfil } from 'app/shared/model/profil.model';
import { ProfilService } from 'app/entities/profil';
import { IWallet } from 'app/shared/model/wallet.model';
import { WalletService } from 'app/entities/wallet';
import { ISpentRecurrence } from 'app/shared/model/spent-recurrence.model';
import { SpentRecurrenceService } from 'app/entities/spent-recurrence';

@Component({
  selector: 'msha-spent-update',
  templateUrl: './spent-update.component.html'
})
export class SpentUpdateComponent implements OnInit {
  isSaving: boolean;

  profils: IProfil[];

  wallets: IWallet[];

  spentrecurrences: ISpentRecurrence[];
  spentDateDp: any;

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(100)]],
    amount: [null, [Validators.required]],
    spentDate: [null, [Validators.required]],
    validated: [null, [Validators.required]],
    spenderId: [],
    walletId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected spentService: SpentService,
    protected profilService: ProfilService,
    protected walletService: WalletService,
    protected spentRecurrenceService: SpentRecurrenceService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ spent }) => {
      this.updateForm(spent);
    });
    this.profilService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IProfil[]>) => mayBeOk.ok),
        map((response: HttpResponse<IProfil[]>) => response.body)
      )
      .subscribe((res: IProfil[]) => (this.profils = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.walletService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IWallet[]>) => mayBeOk.ok),
        map((response: HttpResponse<IWallet[]>) => response.body)
      )
      .subscribe((res: IWallet[]) => (this.wallets = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.spentRecurrenceService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ISpentRecurrence[]>) => mayBeOk.ok),
        map((response: HttpResponse<ISpentRecurrence[]>) => response.body)
      )
      .subscribe((res: ISpentRecurrence[]) => (this.spentrecurrences = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(spent: ISpent) {
    this.editForm.patchValue({
      id: spent.id,
      label: spent.label,
      amount: spent.amount,
      spentDate: spent.spentDate,
      validated: spent.validated,
      spenderId: spent.spenderId,
      walletId: spent.walletId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const spent = this.createFromForm();
    if (spent.id !== undefined) {
      this.subscribeToSaveResponse(this.spentService.update(spent));
    } else {
      this.subscribeToSaveResponse(this.spentService.create(spent));
    }
  }

  private createFromForm(): ISpent {
    return {
      ...new Spent(),
      id: this.editForm.get(['id']).value,
      label: this.editForm.get(['label']).value,
      amount: this.editForm.get(['amount']).value,
      spentDate: this.editForm.get(['spentDate']).value,
      validated: this.editForm.get(['validated']).value,
      spenderId: this.editForm.get(['spenderId']).value,
      walletId: this.editForm.get(['walletId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISpent>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackProfilById(index: number, item: IProfil) {
    return item.id;
  }

  trackWalletById(index: number, item: IWallet) {
    return item.id;
  }

  trackSpentRecurrenceById(index: number, item: ISpentRecurrence) {
    return item.id;
  }
}
