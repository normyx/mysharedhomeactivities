import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { IProfil, Profil } from 'app/shared/model/profil.model';
import { ProfilService } from './profil.service';
import { IUser, UserService } from 'app/core';
import { IWallet } from 'app/shared/model/wallet.model';
import { WalletService } from 'app/entities/wallet';
import { ITaskProject } from 'app/shared/model/task-project.model';
import { TaskProjectService } from 'app/entities/task-project';
import { IWorkspace } from 'app/shared/model/workspace.model';
import { WorkspaceService } from 'app/entities/workspace';
import { ITask } from 'app/shared/model/task.model';
import { TaskService } from 'app/entities/task';
import { IShoppingList } from 'app/shared/model/shopping-list.model';
import { ShoppingListService } from 'app/entities/shopping-list';

@Component({
  selector: 'msha-profil-update',
  templateUrl: './profil-update.component.html'
})
export class ProfilUpdateComponent implements OnInit {
  isSaving: boolean;

  users: IUser[];

  wallets: IWallet[];

  taskprojects: ITaskProject[];

  workspaces: IWorkspace[];

  tasks: ITask[];

  shoppinglists: IShoppingList[];

  editForm = this.fb.group({
    id: [],
    displayName: [null, [Validators.required]],
    photo: [],
    photoContentType: [],
    userId: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected profilService: ProfilService,
    protected userService: UserService,
    protected walletService: WalletService,
    protected taskProjectService: TaskProjectService,
    protected workspaceService: WorkspaceService,
    protected taskService: TaskService,
    protected shoppingListService: ShoppingListService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ profil }) => {
      this.updateForm(profil);
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.walletService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IWallet[]>) => mayBeOk.ok),
        map((response: HttpResponse<IWallet[]>) => response.body)
      )
      .subscribe((res: IWallet[]) => (this.wallets = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.taskProjectService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ITaskProject[]>) => mayBeOk.ok),
        map((response: HttpResponse<ITaskProject[]>) => response.body)
      )
      .subscribe((res: ITaskProject[]) => (this.taskprojects = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.workspaceService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IWorkspace[]>) => mayBeOk.ok),
        map((response: HttpResponse<IWorkspace[]>) => response.body)
      )
      .subscribe((res: IWorkspace[]) => (this.workspaces = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.taskService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ITask[]>) => mayBeOk.ok),
        map((response: HttpResponse<ITask[]>) => response.body)
      )
      .subscribe((res: ITask[]) => (this.tasks = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.shoppingListService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IShoppingList[]>) => mayBeOk.ok),
        map((response: HttpResponse<IShoppingList[]>) => response.body)
      )
      .subscribe((res: IShoppingList[]) => (this.shoppinglists = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(profil: IProfil) {
    this.editForm.patchValue({
      id: profil.id,
      displayName: profil.displayName,
      photo: profil.photo,
      photoContentType: profil.photoContentType,
      userId: profil.userId
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string, isImage) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file = event.target.files[0];
        if (isImage && !/^image\//.test(file.type)) {
          reject(`File was expected to be an image but was found to be ${file.type}`);
        } else {
          const filedContentType: string = field + 'ContentType';
          this.dataUtils.toBase64(file, base64Data => {
            this.editForm.patchValue({
              [field]: base64Data,
              [filedContentType]: file.type
            });
          });
        }
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      () => console.log('blob added'), // sucess
      this.onError
    );
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string) {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const profil = this.createFromForm();
    if (profil.id !== undefined) {
      this.subscribeToSaveResponse(this.profilService.update(profil));
    } else {
      this.subscribeToSaveResponse(this.profilService.create(profil));
    }
  }

  private createFromForm(): IProfil {
    return {
      ...new Profil(),
      id: this.editForm.get(['id']).value,
      displayName: this.editForm.get(['displayName']).value,
      photoContentType: this.editForm.get(['photoContentType']).value,
      photo: this.editForm.get(['photo']).value,
      userId: this.editForm.get(['userId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProfil>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  trackWalletById(index: number, item: IWallet) {
    return item.id;
  }

  trackTaskProjectById(index: number, item: ITaskProject) {
    return item.id;
  }

  trackWorkspaceById(index: number, item: IWorkspace) {
    return item.id;
  }

  trackTaskById(index: number, item: ITask) {
    return item.id;
  }

  trackShoppingListById(index: number, item: IShoppingList) {
    return item.id;
  }

  getSelected(selectedVals: Array<any>, option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
