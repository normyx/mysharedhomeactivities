import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISpentSharingConfig } from 'app/shared/model/spent-sharing-config.model';
import { SpentSharingConfigService } from './spent-sharing-config.service';

@Component({
  selector: 'msha-spent-sharing-config-delete-dialog',
  templateUrl: './spent-sharing-config-delete-dialog.component.html'
})
export class SpentSharingConfigDeleteDialogComponent {
  spentSharingConfig: ISpentSharingConfig;

  constructor(
    protected spentSharingConfigService: SpentSharingConfigService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.spentSharingConfigService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'spentSharingConfigListModification',
        content: 'Deleted an spentSharingConfig'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'msha-spent-sharing-config-delete-popup',
  template: ''
})
export class SpentSharingConfigDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ spentSharingConfig }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(SpentSharingConfigDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.spentSharingConfig = spentSharingConfig;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/spent-sharing-config', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/spent-sharing-config', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
