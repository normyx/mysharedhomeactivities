import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { MySharedHomeActivitiesSharedModule } from 'app/shared';
import {
  SpentSharingConfigComponent,
  SpentSharingConfigDetailComponent,
  SpentSharingConfigUpdateComponent,
  SpentSharingConfigDeletePopupComponent,
  SpentSharingConfigDeleteDialogComponent,
  spentSharingConfigRoute,
  spentSharingConfigPopupRoute
} from './';

const ENTITY_STATES = [...spentSharingConfigRoute, ...spentSharingConfigPopupRoute];

@NgModule({
  imports: [MySharedHomeActivitiesSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    SpentSharingConfigComponent,
    SpentSharingConfigDetailComponent,
    SpentSharingConfigUpdateComponent,
    SpentSharingConfigDeleteDialogComponent,
    SpentSharingConfigDeletePopupComponent
  ],
  entryComponents: [
    SpentSharingConfigComponent,
    SpentSharingConfigUpdateComponent,
    SpentSharingConfigDeleteDialogComponent,
    SpentSharingConfigDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MySharedHomeActivitiesSpentSharingConfigModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
