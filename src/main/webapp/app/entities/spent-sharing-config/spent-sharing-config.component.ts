import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ISpentSharingConfig } from 'app/shared/model/spent-sharing-config.model';
import { AccountService } from 'app/core';
import { SpentSharingConfigService } from './spent-sharing-config.service';

@Component({
  selector: 'msha-spent-sharing-config',
  templateUrl: './spent-sharing-config.component.html'
})
export class SpentSharingConfigComponent implements OnInit, OnDestroy {
  spentSharingConfigs: ISpentSharingConfig[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected spentSharingConfigService: SpentSharingConfigService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.spentSharingConfigService
      .query()
      .pipe(
        filter((res: HttpResponse<ISpentSharingConfig[]>) => res.ok),
        map((res: HttpResponse<ISpentSharingConfig[]>) => res.body)
      )
      .subscribe(
        (res: ISpentSharingConfig[]) => {
          this.spentSharingConfigs = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInSpentSharingConfigs();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ISpentSharingConfig) {
    return item.id;
  }

  registerChangeInSpentSharingConfigs() {
    this.eventSubscriber = this.eventManager.subscribe('spentSharingConfigListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
