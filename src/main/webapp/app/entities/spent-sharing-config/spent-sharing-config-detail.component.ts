import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISpentSharingConfig } from 'app/shared/model/spent-sharing-config.model';

@Component({
  selector: 'msha-spent-sharing-config-detail',
  templateUrl: './spent-sharing-config-detail.component.html'
})
export class SpentSharingConfigDetailComponent implements OnInit {
  spentSharingConfig: ISpentSharingConfig;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ spentSharingConfig }) => {
      this.spentSharingConfig = spentSharingConfig;
    });
  }

  previousState() {
    window.history.back();
  }
}
