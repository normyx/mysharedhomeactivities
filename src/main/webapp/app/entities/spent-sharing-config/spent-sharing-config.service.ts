import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISpentSharingConfig } from 'app/shared/model/spent-sharing-config.model';

type EntityResponseType = HttpResponse<ISpentSharingConfig>;
type EntityArrayResponseType = HttpResponse<ISpentSharingConfig[]>;

@Injectable({ providedIn: 'root' })
export class SpentSharingConfigService {
  public resourceUrl = SERVER_API_URL + 'api/spent-sharing-configs';

  constructor(protected http: HttpClient) {}

  create(spentSharingConfig: ISpentSharingConfig): Observable<EntityResponseType> {
    return this.http.post<ISpentSharingConfig>(this.resourceUrl, spentSharingConfig, { observe: 'response' });
  }

  update(spentSharingConfig: ISpentSharingConfig): Observable<EntityResponseType> {
    return this.http.put<ISpentSharingConfig>(this.resourceUrl, spentSharingConfig, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISpentSharingConfig>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISpentSharingConfig[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
