import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ISpentSharingConfig, SpentSharingConfig } from 'app/shared/model/spent-sharing-config.model';
import { SpentSharingConfigService } from './spent-sharing-config.service';
import { IProfil } from 'app/shared/model/profil.model';
import { ProfilService } from 'app/entities/profil';
import { ISpentConfig } from 'app/shared/model/spent-config.model';
import { SpentConfigService } from 'app/entities/spent-config';

@Component({
  selector: 'msha-spent-sharing-config-update',
  templateUrl: './spent-sharing-config-update.component.html'
})
export class SpentSharingConfigUpdateComponent implements OnInit {
  isSaving: boolean;

  profils: IProfil[];

  spentconfigs: ISpentConfig[];

  editForm = this.fb.group({
    id: [],
    share: [null, [Validators.required]],
    sharingUserId: [],
    defaultSpentConfigId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected spentSharingConfigService: SpentSharingConfigService,
    protected profilService: ProfilService,
    protected spentConfigService: SpentConfigService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ spentSharingConfig }) => {
      this.updateForm(spentSharingConfig);
    });
    this.profilService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IProfil[]>) => mayBeOk.ok),
        map((response: HttpResponse<IProfil[]>) => response.body)
      )
      .subscribe((res: IProfil[]) => (this.profils = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.spentConfigService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ISpentConfig[]>) => mayBeOk.ok),
        map((response: HttpResponse<ISpentConfig[]>) => response.body)
      )
      .subscribe((res: ISpentConfig[]) => (this.spentconfigs = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(spentSharingConfig: ISpentSharingConfig) {
    this.editForm.patchValue({
      id: spentSharingConfig.id,
      share: spentSharingConfig.share,
      sharingUserId: spentSharingConfig.sharingUserId,
      defaultSpentConfigId: spentSharingConfig.defaultSpentConfigId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const spentSharingConfig = this.createFromForm();
    if (spentSharingConfig.id !== undefined) {
      this.subscribeToSaveResponse(this.spentSharingConfigService.update(spentSharingConfig));
    } else {
      this.subscribeToSaveResponse(this.spentSharingConfigService.create(spentSharingConfig));
    }
  }

  private createFromForm(): ISpentSharingConfig {
    return {
      ...new SpentSharingConfig(),
      id: this.editForm.get(['id']).value,
      share: this.editForm.get(['share']).value,
      sharingUserId: this.editForm.get(['sharingUserId']).value,
      defaultSpentConfigId: this.editForm.get(['defaultSpentConfigId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISpentSharingConfig>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackProfilById(index: number, item: IProfil) {
    return item.id;
  }

  trackSpentConfigById(index: number, item: ISpentConfig) {
    return item.id;
  }
}
