import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { SpentSharingConfig } from 'app/shared/model/spent-sharing-config.model';
import { SpentSharingConfigService } from './spent-sharing-config.service';
import { SpentSharingConfigComponent } from './spent-sharing-config.component';
import { SpentSharingConfigDetailComponent } from './spent-sharing-config-detail.component';
import { SpentSharingConfigUpdateComponent } from './spent-sharing-config-update.component';
import { SpentSharingConfigDeletePopupComponent } from './spent-sharing-config-delete-dialog.component';
import { ISpentSharingConfig } from 'app/shared/model/spent-sharing-config.model';

@Injectable({ providedIn: 'root' })
export class SpentSharingConfigResolve implements Resolve<ISpentSharingConfig> {
  constructor(private service: SpentSharingConfigService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ISpentSharingConfig> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<SpentSharingConfig>) => response.ok),
        map((spentSharingConfig: HttpResponse<SpentSharingConfig>) => spentSharingConfig.body)
      );
    }
    return of(new SpentSharingConfig());
  }
}

export const spentSharingConfigRoute: Routes = [
  {
    path: '',
    component: SpentSharingConfigComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spentSharingConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SpentSharingConfigDetailComponent,
    resolve: {
      spentSharingConfig: SpentSharingConfigResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spentSharingConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SpentSharingConfigUpdateComponent,
    resolve: {
      spentSharingConfig: SpentSharingConfigResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spentSharingConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SpentSharingConfigUpdateComponent,
    resolve: {
      spentSharingConfig: SpentSharingConfigResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spentSharingConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const spentSharingConfigPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: SpentSharingConfigDeletePopupComponent,
    resolve: {
      spentSharingConfig: SpentSharingConfigResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spentSharingConfig.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
