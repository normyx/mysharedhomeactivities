import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { SpentConfig } from 'app/shared/model/spent-config.model';
import { SpentConfigService } from './spent-config.service';
import { SpentConfigComponent } from './spent-config.component';
import { SpentConfigDetailComponent } from './spent-config-detail.component';
import { SpentConfigUpdateComponent } from './spent-config-update.component';
import { SpentConfigDeletePopupComponent } from './spent-config-delete-dialog.component';
import { ISpentConfig } from 'app/shared/model/spent-config.model';

@Injectable({ providedIn: 'root' })
export class SpentConfigResolve implements Resolve<ISpentConfig> {
  constructor(private service: SpentConfigService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ISpentConfig> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<SpentConfig>) => response.ok),
        map((spentConfig: HttpResponse<SpentConfig>) => spentConfig.body)
      );
    }
    return of(new SpentConfig());
  }
}

export const spentConfigRoute: Routes = [
  {
    path: '',
    component: SpentConfigComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spentConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SpentConfigDetailComponent,
    resolve: {
      spentConfig: SpentConfigResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spentConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SpentConfigUpdateComponent,
    resolve: {
      spentConfig: SpentConfigResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spentConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SpentConfigUpdateComponent,
    resolve: {
      spentConfig: SpentConfigResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spentConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const spentConfigPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: SpentConfigDeletePopupComponent,
    resolve: {
      spentConfig: SpentConfigResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spentConfig.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
