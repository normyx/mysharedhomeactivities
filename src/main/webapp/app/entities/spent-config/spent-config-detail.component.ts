import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISpentConfig } from 'app/shared/model/spent-config.model';

@Component({
  selector: 'msha-spent-config-detail',
  templateUrl: './spent-config-detail.component.html'
})
export class SpentConfigDetailComponent implements OnInit {
  spentConfig: ISpentConfig;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ spentConfig }) => {
      this.spentConfig = spentConfig;
    });
  }

  previousState() {
    window.history.back();
  }
}
