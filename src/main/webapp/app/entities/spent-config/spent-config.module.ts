import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { MySharedHomeActivitiesSharedModule } from 'app/shared';
import {
  SpentConfigComponent,
  SpentConfigDetailComponent,
  SpentConfigUpdateComponent,
  SpentConfigDeletePopupComponent,
  SpentConfigDeleteDialogComponent,
  spentConfigRoute,
  spentConfigPopupRoute
} from './';

const ENTITY_STATES = [...spentConfigRoute, ...spentConfigPopupRoute];

@NgModule({
  imports: [MySharedHomeActivitiesSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    SpentConfigComponent,
    SpentConfigDetailComponent,
    SpentConfigUpdateComponent,
    SpentConfigDeleteDialogComponent,
    SpentConfigDeletePopupComponent
  ],
  entryComponents: [SpentConfigComponent, SpentConfigUpdateComponent, SpentConfigDeleteDialogComponent, SpentConfigDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MySharedHomeActivitiesSpentConfigModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
