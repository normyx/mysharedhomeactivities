import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISpentConfig } from 'app/shared/model/spent-config.model';

type EntityResponseType = HttpResponse<ISpentConfig>;
type EntityArrayResponseType = HttpResponse<ISpentConfig[]>;

@Injectable({ providedIn: 'root' })
export class SpentConfigService {
  public resourceUrl = SERVER_API_URL + 'api/spent-configs';

  constructor(protected http: HttpClient) {}

  create(spentConfig: ISpentConfig): Observable<EntityResponseType> {
    return this.http.post<ISpentConfig>(this.resourceUrl, spentConfig, { observe: 'response' });
  }

  update(spentConfig: ISpentConfig): Observable<EntityResponseType> {
    return this.http.put<ISpentConfig>(this.resourceUrl, spentConfig, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISpentConfig>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISpentConfig[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
