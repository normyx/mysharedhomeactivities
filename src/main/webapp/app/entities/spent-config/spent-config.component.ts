import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ISpentConfig } from 'app/shared/model/spent-config.model';
import { AccountService } from 'app/core';
import { SpentConfigService } from './spent-config.service';

@Component({
  selector: 'msha-spent-config',
  templateUrl: './spent-config.component.html'
})
export class SpentConfigComponent implements OnInit, OnDestroy {
  spentConfigs: ISpentConfig[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected spentConfigService: SpentConfigService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.spentConfigService
      .query()
      .pipe(
        filter((res: HttpResponse<ISpentConfig[]>) => res.ok),
        map((res: HttpResponse<ISpentConfig[]>) => res.body)
      )
      .subscribe(
        (res: ISpentConfig[]) => {
          this.spentConfigs = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInSpentConfigs();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ISpentConfig) {
    return item.id;
  }

  registerChangeInSpentConfigs() {
    this.eventSubscriber = this.eventManager.subscribe('spentConfigListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
