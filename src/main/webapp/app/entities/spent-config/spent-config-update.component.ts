import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ISpentConfig, SpentConfig } from 'app/shared/model/spent-config.model';
import { SpentConfigService } from './spent-config.service';
import { IWallet } from 'app/shared/model/wallet.model';
import { WalletService } from 'app/entities/wallet';

@Component({
  selector: 'msha-spent-config-update',
  templateUrl: './spent-config-update.component.html'
})
export class SpentConfigUpdateComponent implements OnInit {
  isSaving: boolean;

  wallets: IWallet[];

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(40)]],
    walletId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected spentConfigService: SpentConfigService,
    protected walletService: WalletService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ spentConfig }) => {
      this.updateForm(spentConfig);
    });
    this.walletService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IWallet[]>) => mayBeOk.ok),
        map((response: HttpResponse<IWallet[]>) => response.body)
      )
      .subscribe((res: IWallet[]) => (this.wallets = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(spentConfig: ISpentConfig) {
    this.editForm.patchValue({
      id: spentConfig.id,
      label: spentConfig.label,
      walletId: spentConfig.walletId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const spentConfig = this.createFromForm();
    if (spentConfig.id !== undefined) {
      this.subscribeToSaveResponse(this.spentConfigService.update(spentConfig));
    } else {
      this.subscribeToSaveResponse(this.spentConfigService.create(spentConfig));
    }
  }

  private createFromForm(): ISpentConfig {
    return {
      ...new SpentConfig(),
      id: this.editForm.get(['id']).value,
      label: this.editForm.get(['label']).value,
      walletId: this.editForm.get(['walletId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISpentConfig>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackWalletById(index: number, item: IWallet) {
    return item.id;
  }
}
