import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISpentConfig } from 'app/shared/model/spent-config.model';
import { SpentConfigService } from './spent-config.service';

@Component({
  selector: 'msha-spent-config-delete-dialog',
  templateUrl: './spent-config-delete-dialog.component.html'
})
export class SpentConfigDeleteDialogComponent {
  spentConfig: ISpentConfig;

  constructor(
    protected spentConfigService: SpentConfigService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.spentConfigService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'spentConfigListModification',
        content: 'Deleted an spentConfig'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'msha-spent-config-delete-popup',
  template: ''
})
export class SpentConfigDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ spentConfig }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(SpentConfigDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.spentConfig = spentConfig;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/spent-config', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/spent-config', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
