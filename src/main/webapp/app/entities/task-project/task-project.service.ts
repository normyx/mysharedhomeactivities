import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ITaskProject } from 'app/shared/model/task-project.model';

type EntityResponseType = HttpResponse<ITaskProject>;
type EntityArrayResponseType = HttpResponse<ITaskProject[]>;

@Injectable({ providedIn: 'root' })
export class TaskProjectService {
  public resourceUrl = SERVER_API_URL + 'api/task-projects';

  constructor(protected http: HttpClient) {}

  create(taskProject: ITaskProject): Observable<EntityResponseType> {
    return this.http.post<ITaskProject>(this.resourceUrl, taskProject, { observe: 'response' });
  }

  update(taskProject: ITaskProject): Observable<EntityResponseType> {
    return this.http.put<ITaskProject>(this.resourceUrl, taskProject, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITaskProject>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITaskProject[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
