import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { TaskProject } from 'app/shared/model/task-project.model';
import { TaskProjectService } from './task-project.service';
import { TaskProjectComponent } from './task-project.component';
import { TaskProjectDetailComponent } from './task-project-detail.component';
import { TaskProjectUpdateComponent } from './task-project-update.component';
import { TaskProjectDeletePopupComponent } from './task-project-delete-dialog.component';
import { ITaskProject } from 'app/shared/model/task-project.model';

@Injectable({ providedIn: 'root' })
export class TaskProjectResolve implements Resolve<ITaskProject> {
  constructor(private service: TaskProjectService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITaskProject> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<TaskProject>) => response.ok),
        map((taskProject: HttpResponse<TaskProject>) => taskProject.body)
      );
    }
    return of(new TaskProject());
  }
}

export const taskProjectRoute: Routes = [
  {
    path: '',
    component: TaskProjectComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.taskProject.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TaskProjectDetailComponent,
    resolve: {
      taskProject: TaskProjectResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.taskProject.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TaskProjectUpdateComponent,
    resolve: {
      taskProject: TaskProjectResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.taskProject.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TaskProjectUpdateComponent,
    resolve: {
      taskProject: TaskProjectResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.taskProject.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const taskProjectPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: TaskProjectDeletePopupComponent,
    resolve: {
      taskProject: TaskProjectResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.taskProject.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
