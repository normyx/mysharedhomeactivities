import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ITaskProject, TaskProject } from 'app/shared/model/task-project.model';
import { TaskProjectService } from './task-project.service';
import { IWorkspace } from 'app/shared/model/workspace.model';
import { WorkspaceService } from 'app/entities/workspace';
import { IProfil } from 'app/shared/model/profil.model';
import { ProfilService } from 'app/entities/profil';

@Component({
  selector: 'msha-task-project-update',
  templateUrl: './task-project-update.component.html'
})
export class TaskProjectUpdateComponent implements OnInit {
  isSaving: boolean;

  workspaces: IWorkspace[];

  profils: IProfil[];

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(40)]],
    workspaceId: [],
    owners: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected taskProjectService: TaskProjectService,
    protected workspaceService: WorkspaceService,
    protected profilService: ProfilService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ taskProject }) => {
      this.updateForm(taskProject);
    });
    this.workspaceService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IWorkspace[]>) => mayBeOk.ok),
        map((response: HttpResponse<IWorkspace[]>) => response.body)
      )
      .subscribe((res: IWorkspace[]) => (this.workspaces = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.profilService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IProfil[]>) => mayBeOk.ok),
        map((response: HttpResponse<IProfil[]>) => response.body)
      )
      .subscribe((res: IProfil[]) => (this.profils = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(taskProject: ITaskProject) {
    this.editForm.patchValue({
      id: taskProject.id,
      label: taskProject.label,
      workspaceId: taskProject.workspaceId,
      owners: taskProject.owners
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const taskProject = this.createFromForm();
    if (taskProject.id !== undefined) {
      this.subscribeToSaveResponse(this.taskProjectService.update(taskProject));
    } else {
      this.subscribeToSaveResponse(this.taskProjectService.create(taskProject));
    }
  }

  private createFromForm(): ITaskProject {
    return {
      ...new TaskProject(),
      id: this.editForm.get(['id']).value,
      label: this.editForm.get(['label']).value,
      workspaceId: this.editForm.get(['workspaceId']).value,
      owners: this.editForm.get(['owners']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITaskProject>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackWorkspaceById(index: number, item: IWorkspace) {
    return item.id;
  }

  trackProfilById(index: number, item: IProfil) {
    return item.id;
  }

  getSelected(selectedVals: Array<any>, option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
