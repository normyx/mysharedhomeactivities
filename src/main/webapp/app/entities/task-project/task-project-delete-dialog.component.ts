import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITaskProject } from 'app/shared/model/task-project.model';
import { TaskProjectService } from './task-project.service';

@Component({
  selector: 'msha-task-project-delete-dialog',
  templateUrl: './task-project-delete-dialog.component.html'
})
export class TaskProjectDeleteDialogComponent {
  taskProject: ITaskProject;

  constructor(
    protected taskProjectService: TaskProjectService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.taskProjectService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'taskProjectListModification',
        content: 'Deleted an taskProject'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'msha-task-project-delete-popup',
  template: ''
})
export class TaskProjectDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ taskProject }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(TaskProjectDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.taskProject = taskProject;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/task-project', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/task-project', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
