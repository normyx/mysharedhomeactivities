import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITaskProject } from 'app/shared/model/task-project.model';

@Component({
  selector: 'msha-task-project-detail',
  templateUrl: './task-project-detail.component.html'
})
export class TaskProjectDetailComponent implements OnInit {
  taskProject: ITaskProject;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ taskProject }) => {
      this.taskProject = taskProject;
    });
  }

  previousState() {
    window.history.back();
  }
}
