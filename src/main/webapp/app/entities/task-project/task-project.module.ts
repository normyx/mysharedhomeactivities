import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { MySharedHomeActivitiesSharedModule } from 'app/shared';
import {
  TaskProjectComponent,
  TaskProjectDetailComponent,
  TaskProjectUpdateComponent,
  TaskProjectDeletePopupComponent,
  TaskProjectDeleteDialogComponent,
  taskProjectRoute,
  taskProjectPopupRoute
} from './';

const ENTITY_STATES = [...taskProjectRoute, ...taskProjectPopupRoute];

@NgModule({
  imports: [MySharedHomeActivitiesSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    TaskProjectComponent,
    TaskProjectDetailComponent,
    TaskProjectUpdateComponent,
    TaskProjectDeleteDialogComponent,
    TaskProjectDeletePopupComponent
  ],
  entryComponents: [TaskProjectComponent, TaskProjectUpdateComponent, TaskProjectDeleteDialogComponent, TaskProjectDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MySharedHomeActivitiesTaskProjectModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
