export * from './task-project.service';
export * from './task-project-update.component';
export * from './task-project-delete-dialog.component';
export * from './task-project-detail.component';
export * from './task-project.component';
export * from './task-project.route';
