import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { SpentSharing } from 'app/shared/model/spent-sharing.model';
import { SpentSharingService } from './spent-sharing.service';
import { SpentSharingComponent } from './spent-sharing.component';
import { SpentSharingDetailComponent } from './spent-sharing-detail.component';
import { SpentSharingUpdateComponent } from './spent-sharing-update.component';
import { SpentSharingDeletePopupComponent } from './spent-sharing-delete-dialog.component';
import { ISpentSharing } from 'app/shared/model/spent-sharing.model';

@Injectable({ providedIn: 'root' })
export class SpentSharingResolve implements Resolve<ISpentSharing> {
  constructor(private service: SpentSharingService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ISpentSharing> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<SpentSharing>) => response.ok),
        map((spentSharing: HttpResponse<SpentSharing>) => spentSharing.body)
      );
    }
    return of(new SpentSharing());
  }
}

export const spentSharingRoute: Routes = [
  {
    path: '',
    component: SpentSharingComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spentSharing.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SpentSharingDetailComponent,
    resolve: {
      spentSharing: SpentSharingResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spentSharing.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SpentSharingUpdateComponent,
    resolve: {
      spentSharing: SpentSharingResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spentSharing.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SpentSharingUpdateComponent,
    resolve: {
      spentSharing: SpentSharingResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spentSharing.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const spentSharingPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: SpentSharingDeletePopupComponent,
    resolve: {
      spentSharing: SpentSharingResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spentSharing.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
