import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ISpentSharing } from 'app/shared/model/spent-sharing.model';
import { AccountService } from 'app/core';
import { SpentSharingService } from './spent-sharing.service';

@Component({
  selector: 'msha-spent-sharing',
  templateUrl: './spent-sharing.component.html'
})
export class SpentSharingComponent implements OnInit, OnDestroy {
  spentSharings: ISpentSharing[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected spentSharingService: SpentSharingService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.spentSharingService
      .query()
      .pipe(
        filter((res: HttpResponse<ISpentSharing[]>) => res.ok),
        map((res: HttpResponse<ISpentSharing[]>) => res.body)
      )
      .subscribe(
        (res: ISpentSharing[]) => {
          this.spentSharings = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInSpentSharings();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ISpentSharing) {
    return item.id;
  }

  registerChangeInSpentSharings() {
    this.eventSubscriber = this.eventManager.subscribe('spentSharingListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
