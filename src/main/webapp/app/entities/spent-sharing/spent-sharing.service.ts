import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISpentSharing } from 'app/shared/model/spent-sharing.model';

type EntityResponseType = HttpResponse<ISpentSharing>;
type EntityArrayResponseType = HttpResponse<ISpentSharing[]>;

@Injectable({ providedIn: 'root' })
export class SpentSharingService {
  public resourceUrl = SERVER_API_URL + 'api/spent-sharings';

  constructor(protected http: HttpClient) {}

  create(spentSharing: ISpentSharing): Observable<EntityResponseType> {
    return this.http.post<ISpentSharing>(this.resourceUrl, spentSharing, { observe: 'response' });
  }

  update(spentSharing: ISpentSharing): Observable<EntityResponseType> {
    return this.http.put<ISpentSharing>(this.resourceUrl, spentSharing, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISpentSharing>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISpentSharing[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
