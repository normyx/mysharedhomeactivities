import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISpentSharing } from 'app/shared/model/spent-sharing.model';
import { SpentSharingService } from './spent-sharing.service';

@Component({
  selector: 'msha-spent-sharing-delete-dialog',
  templateUrl: './spent-sharing-delete-dialog.component.html'
})
export class SpentSharingDeleteDialogComponent {
  spentSharing: ISpentSharing;

  constructor(
    protected spentSharingService: SpentSharingService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.spentSharingService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'spentSharingListModification',
        content: 'Deleted an spentSharing'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'msha-spent-sharing-delete-popup',
  template: ''
})
export class SpentSharingDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ spentSharing }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(SpentSharingDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.spentSharing = spentSharing;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/spent-sharing', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/spent-sharing', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
