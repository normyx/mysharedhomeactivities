import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISpentSharing } from 'app/shared/model/spent-sharing.model';

@Component({
  selector: 'msha-spent-sharing-detail',
  templateUrl: './spent-sharing-detail.component.html'
})
export class SpentSharingDetailComponent implements OnInit {
  spentSharing: ISpentSharing;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ spentSharing }) => {
      this.spentSharing = spentSharing;
    });
  }

  previousState() {
    window.history.back();
  }
}
