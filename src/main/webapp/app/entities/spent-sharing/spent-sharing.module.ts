import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { MySharedHomeActivitiesSharedModule } from 'app/shared';
import {
  SpentSharingComponent,
  SpentSharingDetailComponent,
  SpentSharingUpdateComponent,
  SpentSharingDeletePopupComponent,
  SpentSharingDeleteDialogComponent,
  spentSharingRoute,
  spentSharingPopupRoute
} from './';

const ENTITY_STATES = [...spentSharingRoute, ...spentSharingPopupRoute];

@NgModule({
  imports: [MySharedHomeActivitiesSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    SpentSharingComponent,
    SpentSharingDetailComponent,
    SpentSharingUpdateComponent,
    SpentSharingDeleteDialogComponent,
    SpentSharingDeletePopupComponent
  ],
  entryComponents: [
    SpentSharingComponent,
    SpentSharingUpdateComponent,
    SpentSharingDeleteDialogComponent,
    SpentSharingDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MySharedHomeActivitiesSpentSharingModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
