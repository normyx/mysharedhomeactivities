import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ISpentSharing, SpentSharing } from 'app/shared/model/spent-sharing.model';
import { SpentSharingService } from './spent-sharing.service';
import { IProfil } from 'app/shared/model/profil.model';
import { ProfilService } from 'app/entities/profil';
import { ISpent } from 'app/shared/model/spent.model';
import { SpentService } from 'app/entities/spent';

@Component({
  selector: 'msha-spent-sharing-update',
  templateUrl: './spent-sharing-update.component.html'
})
export class SpentSharingUpdateComponent implements OnInit {
  isSaving: boolean;

  profils: IProfil[];

  spents: ISpent[];

  editForm = this.fb.group({
    id: [],
    amountShare: [null, [Validators.required]],
    share: [null, [Validators.required]],
    sharingUserId: [],
    spentId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected spentSharingService: SpentSharingService,
    protected profilService: ProfilService,
    protected spentService: SpentService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ spentSharing }) => {
      this.updateForm(spentSharing);
    });
    this.profilService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IProfil[]>) => mayBeOk.ok),
        map((response: HttpResponse<IProfil[]>) => response.body)
      )
      .subscribe((res: IProfil[]) => (this.profils = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.spentService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ISpent[]>) => mayBeOk.ok),
        map((response: HttpResponse<ISpent[]>) => response.body)
      )
      .subscribe((res: ISpent[]) => (this.spents = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(spentSharing: ISpentSharing) {
    this.editForm.patchValue({
      id: spentSharing.id,
      amountShare: spentSharing.amountShare,
      share: spentSharing.share,
      sharingUserId: spentSharing.sharingUserId,
      spentId: spentSharing.spentId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const spentSharing = this.createFromForm();
    if (spentSharing.id !== undefined) {
      this.subscribeToSaveResponse(this.spentSharingService.update(spentSharing));
    } else {
      this.subscribeToSaveResponse(this.spentSharingService.create(spentSharing));
    }
  }

  private createFromForm(): ISpentSharing {
    return {
      ...new SpentSharing(),
      id: this.editForm.get(['id']).value,
      amountShare: this.editForm.get(['amountShare']).value,
      share: this.editForm.get(['share']).value,
      sharingUserId: this.editForm.get(['sharingUserId']).value,
      spentId: this.editForm.get(['spentId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISpentSharing>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackProfilById(index: number, item: IProfil) {
    return item.id;
  }

  trackSpentById(index: number, item: ISpent) {
    return item.id;
  }
}
