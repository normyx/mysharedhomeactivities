import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { ITask, Task } from 'app/shared/model/task.model';
import { TaskService } from './task.service';
import { ITaskProject } from 'app/shared/model/task-project.model';
import { TaskProjectService } from 'app/entities/task-project';
import { IProfil } from 'app/shared/model/profil.model';
import { ProfilService } from 'app/entities/profil';

@Component({
  selector: 'msha-task-update',
  templateUrl: './task-update.component.html'
})
export class TaskUpdateComponent implements OnInit {
  isSaving: boolean;

  taskprojects: ITaskProject[];

  profils: IProfil[];
  dueDateDp: any;

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(40)]],
    description: [null, [Validators.maxLength(4000)]],
    done: [null, [Validators.required]],
    dueDate: [],
    priority: [null, [Validators.required, Validators.min(0), Validators.max(4)]],
    taskProjectId: [],
    owners: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected taskService: TaskService,
    protected taskProjectService: TaskProjectService,
    protected profilService: ProfilService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ task }) => {
      this.updateForm(task);
    });
    this.taskProjectService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ITaskProject[]>) => mayBeOk.ok),
        map((response: HttpResponse<ITaskProject[]>) => response.body)
      )
      .subscribe((res: ITaskProject[]) => (this.taskprojects = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.profilService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IProfil[]>) => mayBeOk.ok),
        map((response: HttpResponse<IProfil[]>) => response.body)
      )
      .subscribe((res: IProfil[]) => (this.profils = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(task: ITask) {
    this.editForm.patchValue({
      id: task.id,
      label: task.label,
      description: task.description,
      done: task.done,
      dueDate: task.dueDate,
      priority: task.priority,
      taskProjectId: task.taskProjectId,
      owners: task.owners
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const task = this.createFromForm();
    if (task.id !== undefined) {
      this.subscribeToSaveResponse(this.taskService.update(task));
    } else {
      this.subscribeToSaveResponse(this.taskService.create(task));
    }
  }

  private createFromForm(): ITask {
    return {
      ...new Task(),
      id: this.editForm.get(['id']).value,
      label: this.editForm.get(['label']).value,
      description: this.editForm.get(['description']).value,
      done: this.editForm.get(['done']).value,
      dueDate: this.editForm.get(['dueDate']).value,
      priority: this.editForm.get(['priority']).value,
      taskProjectId: this.editForm.get(['taskProjectId']).value,
      owners: this.editForm.get(['owners']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITask>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackTaskProjectById(index: number, item: ITaskProject) {
    return item.id;
  }

  trackProfilById(index: number, item: IProfil) {
    return item.id;
  }

  getSelected(selectedVals: Array<any>, option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
