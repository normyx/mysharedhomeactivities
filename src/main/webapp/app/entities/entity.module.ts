import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'profil',
        loadChildren: './profil/profil.module#MySharedHomeActivitiesProfilModule'
      },
      {
        path: 'workspace',
        loadChildren: './workspace/workspace.module#MySharedHomeActivitiesWorkspaceModule'
      },
      {
        path: 'wallet',
        loadChildren: './wallet/wallet.module#MySharedHomeActivitiesWalletModule'
      },
      {
        path: 'spent-config',
        loadChildren: './spent-config/spent-config.module#MySharedHomeActivitiesSpentConfigModule'
      },
      {
        path: 'spent-sharing-config',
        loadChildren: './spent-sharing-config/spent-sharing-config.module#MySharedHomeActivitiesSpentSharingConfigModule'
      },
      {
        path: 'spent',
        loadChildren: './spent/spent.module#MySharedHomeActivitiesSpentModule'
      },
      {
        path: 'spent-sharing',
        loadChildren: './spent-sharing/spent-sharing.module#MySharedHomeActivitiesSpentSharingModule'
      },
      {
        path: 'spent-recurrence',
        loadChildren: './spent-recurrence/spent-recurrence.module#MySharedHomeActivitiesSpentRecurrenceModule'
      },
      {
        path: 'task-project',
        loadChildren: './task-project/task-project.module#MySharedHomeActivitiesTaskProjectModule'
      },
      {
        path: 'task',
        loadChildren: './task/task.module#MySharedHomeActivitiesTaskModule'
      },
      {
        path: 'shopping-list',
        loadChildren: './shopping-list/shopping-list.module#MySharedHomeActivitiesShoppingListModule'
      },
      {
        path: 'shopping-item',
        loadChildren: './shopping-item/shopping-item.module#MySharedHomeActivitiesShoppingItemModule'
      },
      {
        path: 'task',
        loadChildren: './task/task.module#MySharedHomeActivitiesTaskModule'
      },
      {
        path: 'profil',
        loadChildren: './profil/profil.module#MySharedHomeActivitiesProfilModule'
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MySharedHomeActivitiesEntityModule {}
