import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IShoppingItem } from 'app/shared/model/shopping-item.model';

type EntityResponseType = HttpResponse<IShoppingItem>;
type EntityArrayResponseType = HttpResponse<IShoppingItem[]>;

@Injectable({ providedIn: 'root' })
export class ShoppingItemService {
  public resourceUrl = SERVER_API_URL + 'api/shopping-items';

  constructor(protected http: HttpClient) {}

  create(shoppingItem: IShoppingItem): Observable<EntityResponseType> {
    return this.http.post<IShoppingItem>(this.resourceUrl, shoppingItem, { observe: 'response' });
  }

  update(shoppingItem: IShoppingItem): Observable<EntityResponseType> {
    return this.http.put<IShoppingItem>(this.resourceUrl, shoppingItem, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IShoppingItem>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IShoppingItem[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
