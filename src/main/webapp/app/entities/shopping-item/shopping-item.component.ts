import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IShoppingItem } from 'app/shared/model/shopping-item.model';
import { AccountService } from 'app/core';
import { ShoppingItemService } from './shopping-item.service';

@Component({
  selector: 'msha-shopping-item',
  templateUrl: './shopping-item.component.html'
})
export class ShoppingItemComponent implements OnInit, OnDestroy {
  shoppingItems: IShoppingItem[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected shoppingItemService: ShoppingItemService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.shoppingItemService
      .query()
      .pipe(
        filter((res: HttpResponse<IShoppingItem[]>) => res.ok),
        map((res: HttpResponse<IShoppingItem[]>) => res.body)
      )
      .subscribe(
        (res: IShoppingItem[]) => {
          this.shoppingItems = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInShoppingItems();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IShoppingItem) {
    return item.id;
  }

  registerChangeInShoppingItems() {
    this.eventSubscriber = this.eventManager.subscribe('shoppingItemListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
