import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ShoppingItem } from 'app/shared/model/shopping-item.model';
import { ShoppingItemService } from './shopping-item.service';
import { ShoppingItemComponent } from './shopping-item.component';
import { ShoppingItemDetailComponent } from './shopping-item-detail.component';
import { ShoppingItemUpdateComponent } from './shopping-item-update.component';
import { ShoppingItemDeletePopupComponent } from './shopping-item-delete-dialog.component';
import { IShoppingItem } from 'app/shared/model/shopping-item.model';

@Injectable({ providedIn: 'root' })
export class ShoppingItemResolve implements Resolve<IShoppingItem> {
  constructor(private service: ShoppingItemService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IShoppingItem> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ShoppingItem>) => response.ok),
        map((shoppingItem: HttpResponse<ShoppingItem>) => shoppingItem.body)
      );
    }
    return of(new ShoppingItem());
  }
}

export const shoppingItemRoute: Routes = [
  {
    path: '',
    component: ShoppingItemComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.shoppingItem.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ShoppingItemDetailComponent,
    resolve: {
      shoppingItem: ShoppingItemResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.shoppingItem.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ShoppingItemUpdateComponent,
    resolve: {
      shoppingItem: ShoppingItemResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.shoppingItem.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ShoppingItemUpdateComponent,
    resolve: {
      shoppingItem: ShoppingItemResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.shoppingItem.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const shoppingItemPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ShoppingItemDeletePopupComponent,
    resolve: {
      shoppingItem: ShoppingItemResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.shoppingItem.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
