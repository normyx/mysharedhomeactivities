import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IShoppingItem, ShoppingItem } from 'app/shared/model/shopping-item.model';
import { ShoppingItemService } from './shopping-item.service';
import { IShoppingList } from 'app/shared/model/shopping-list.model';
import { ShoppingListService } from 'app/entities/shopping-list';

@Component({
  selector: 'msha-shopping-item-update',
  templateUrl: './shopping-item-update.component.html'
})
export class ShoppingItemUpdateComponent implements OnInit {
  isSaving: boolean;

  shoppinglists: IShoppingList[];

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(100)]],
    done: [null, [Validators.required]],
    listId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected shoppingItemService: ShoppingItemService,
    protected shoppingListService: ShoppingListService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ shoppingItem }) => {
      this.updateForm(shoppingItem);
    });
    this.shoppingListService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IShoppingList[]>) => mayBeOk.ok),
        map((response: HttpResponse<IShoppingList[]>) => response.body)
      )
      .subscribe((res: IShoppingList[]) => (this.shoppinglists = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(shoppingItem: IShoppingItem) {
    this.editForm.patchValue({
      id: shoppingItem.id,
      label: shoppingItem.label,
      done: shoppingItem.done,
      listId: shoppingItem.listId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const shoppingItem = this.createFromForm();
    if (shoppingItem.id !== undefined) {
      this.subscribeToSaveResponse(this.shoppingItemService.update(shoppingItem));
    } else {
      this.subscribeToSaveResponse(this.shoppingItemService.create(shoppingItem));
    }
  }

  private createFromForm(): IShoppingItem {
    return {
      ...new ShoppingItem(),
      id: this.editForm.get(['id']).value,
      label: this.editForm.get(['label']).value,
      done: this.editForm.get(['done']).value,
      listId: this.editForm.get(['listId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShoppingItem>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackShoppingListById(index: number, item: IShoppingList) {
    return item.id;
  }
}
