import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { MySharedHomeActivitiesSharedModule } from 'app/shared';
import {
  ShoppingItemComponent,
  ShoppingItemDetailComponent,
  ShoppingItemUpdateComponent,
  ShoppingItemDeletePopupComponent,
  ShoppingItemDeleteDialogComponent,
  shoppingItemRoute,
  shoppingItemPopupRoute
} from './';

const ENTITY_STATES = [...shoppingItemRoute, ...shoppingItemPopupRoute];

@NgModule({
  imports: [MySharedHomeActivitiesSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ShoppingItemComponent,
    ShoppingItemDetailComponent,
    ShoppingItemUpdateComponent,
    ShoppingItemDeleteDialogComponent,
    ShoppingItemDeletePopupComponent
  ],
  entryComponents: [
    ShoppingItemComponent,
    ShoppingItemUpdateComponent,
    ShoppingItemDeleteDialogComponent,
    ShoppingItemDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MySharedHomeActivitiesShoppingItemModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
