import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IShoppingItem } from 'app/shared/model/shopping-item.model';

@Component({
  selector: 'msha-shopping-item-detail',
  templateUrl: './shopping-item-detail.component.html'
})
export class ShoppingItemDetailComponent implements OnInit {
  shoppingItem: IShoppingItem;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ shoppingItem }) => {
      this.shoppingItem = shoppingItem;
    });
  }

  previousState() {
    window.history.back();
  }
}
