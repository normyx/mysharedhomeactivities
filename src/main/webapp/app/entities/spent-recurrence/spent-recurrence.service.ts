import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISpentRecurrence } from 'app/shared/model/spent-recurrence.model';

type EntityResponseType = HttpResponse<ISpentRecurrence>;
type EntityArrayResponseType = HttpResponse<ISpentRecurrence[]>;

@Injectable({ providedIn: 'root' })
export class SpentRecurrenceService {
  public resourceUrl = SERVER_API_URL + 'api/spent-recurrences';

  constructor(protected http: HttpClient) {}

  create(spentRecurrence: ISpentRecurrence): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(spentRecurrence);
    return this.http
      .post<ISpentRecurrence>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(spentRecurrence: ISpentRecurrence): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(spentRecurrence);
    return this.http
      .put<ISpentRecurrence>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ISpentRecurrence>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ISpentRecurrence[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(spentRecurrence: ISpentRecurrence): ISpentRecurrence {
    const copy: ISpentRecurrence = Object.assign({}, spentRecurrence, {
      recurrenceFrom:
        spentRecurrence.recurrenceFrom != null && spentRecurrence.recurrenceFrom.isValid()
          ? spentRecurrence.recurrenceFrom.format(DATE_FORMAT)
          : null,
      recurrenceTo:
        spentRecurrence.recurrenceTo != null && spentRecurrence.recurrenceTo.isValid()
          ? spentRecurrence.recurrenceTo.format(DATE_FORMAT)
          : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.recurrenceFrom = res.body.recurrenceFrom != null ? moment(res.body.recurrenceFrom) : null;
      res.body.recurrenceTo = res.body.recurrenceTo != null ? moment(res.body.recurrenceTo) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((spentRecurrence: ISpentRecurrence) => {
        spentRecurrence.recurrenceFrom = spentRecurrence.recurrenceFrom != null ? moment(spentRecurrence.recurrenceFrom) : null;
        spentRecurrence.recurrenceTo = spentRecurrence.recurrenceTo != null ? moment(spentRecurrence.recurrenceTo) : null;
      });
    }
    return res;
  }
}
