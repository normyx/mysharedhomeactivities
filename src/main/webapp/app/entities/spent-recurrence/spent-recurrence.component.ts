import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ISpentRecurrence } from 'app/shared/model/spent-recurrence.model';
import { AccountService } from 'app/core';
import { SpentRecurrenceService } from './spent-recurrence.service';

@Component({
  selector: 'msha-spent-recurrence',
  templateUrl: './spent-recurrence.component.html'
})
export class SpentRecurrenceComponent implements OnInit, OnDestroy {
  spentRecurrences: ISpentRecurrence[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected spentRecurrenceService: SpentRecurrenceService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.spentRecurrenceService
      .query()
      .pipe(
        filter((res: HttpResponse<ISpentRecurrence[]>) => res.ok),
        map((res: HttpResponse<ISpentRecurrence[]>) => res.body)
      )
      .subscribe(
        (res: ISpentRecurrence[]) => {
          this.spentRecurrences = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInSpentRecurrences();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ISpentRecurrence) {
    return item.id;
  }

  registerChangeInSpentRecurrences() {
    this.eventSubscriber = this.eventManager.subscribe('spentRecurrenceListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
