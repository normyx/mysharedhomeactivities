import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { SpentRecurrence } from 'app/shared/model/spent-recurrence.model';
import { SpentRecurrenceService } from './spent-recurrence.service';
import { SpentRecurrenceComponent } from './spent-recurrence.component';
import { SpentRecurrenceDetailComponent } from './spent-recurrence-detail.component';
import { SpentRecurrenceUpdateComponent } from './spent-recurrence-update.component';
import { SpentRecurrenceDeletePopupComponent } from './spent-recurrence-delete-dialog.component';
import { ISpentRecurrence } from 'app/shared/model/spent-recurrence.model';

@Injectable({ providedIn: 'root' })
export class SpentRecurrenceResolve implements Resolve<ISpentRecurrence> {
  constructor(private service: SpentRecurrenceService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ISpentRecurrence> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<SpentRecurrence>) => response.ok),
        map((spentRecurrence: HttpResponse<SpentRecurrence>) => spentRecurrence.body)
      );
    }
    return of(new SpentRecurrence());
  }
}

export const spentRecurrenceRoute: Routes = [
  {
    path: '',
    component: SpentRecurrenceComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spentRecurrence.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SpentRecurrenceDetailComponent,
    resolve: {
      spentRecurrence: SpentRecurrenceResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spentRecurrence.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SpentRecurrenceUpdateComponent,
    resolve: {
      spentRecurrence: SpentRecurrenceResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spentRecurrence.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SpentRecurrenceUpdateComponent,
    resolve: {
      spentRecurrence: SpentRecurrenceResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spentRecurrence.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const spentRecurrencePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: SpentRecurrenceDeletePopupComponent,
    resolve: {
      spentRecurrence: SpentRecurrenceResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.spentRecurrence.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
