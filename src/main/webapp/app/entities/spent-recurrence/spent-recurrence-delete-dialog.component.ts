import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISpentRecurrence } from 'app/shared/model/spent-recurrence.model';
import { SpentRecurrenceService } from './spent-recurrence.service';

@Component({
  selector: 'msha-spent-recurrence-delete-dialog',
  templateUrl: './spent-recurrence-delete-dialog.component.html'
})
export class SpentRecurrenceDeleteDialogComponent {
  spentRecurrence: ISpentRecurrence;

  constructor(
    protected spentRecurrenceService: SpentRecurrenceService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.spentRecurrenceService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'spentRecurrenceListModification',
        content: 'Deleted an spentRecurrence'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'msha-spent-recurrence-delete-popup',
  template: ''
})
export class SpentRecurrenceDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ spentRecurrence }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(SpentRecurrenceDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.spentRecurrence = spentRecurrence;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/spent-recurrence', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/spent-recurrence', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
