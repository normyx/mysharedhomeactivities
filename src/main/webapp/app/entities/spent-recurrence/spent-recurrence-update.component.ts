import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { ISpentRecurrence, SpentRecurrence } from 'app/shared/model/spent-recurrence.model';
import { SpentRecurrenceService } from './spent-recurrence.service';
import { ISpent } from 'app/shared/model/spent.model';
import { SpentService } from 'app/entities/spent';

@Component({
  selector: 'msha-spent-recurrence-update',
  templateUrl: './spent-recurrence-update.component.html'
})
export class SpentRecurrenceUpdateComponent implements OnInit {
  isSaving: boolean;

  spents: ISpent[];
  recurrenceFromDp: any;
  recurrenceToDp: any;

  editForm = this.fb.group({
    id: [],
    amount: [null, [Validators.required]],
    recurrenceFrom: [null, [Validators.required]],
    recurrenceTo: [],
    dayInMonth: [null, [Validators.required, Validators.min(1), Validators.max(31)]],
    spentId: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected spentRecurrenceService: SpentRecurrenceService,
    protected spentService: SpentService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ spentRecurrence }) => {
      this.updateForm(spentRecurrence);
    });
    this.spentService
      .query({ 'recurrenceId.specified': 'false' })
      .pipe(
        filter((mayBeOk: HttpResponse<ISpent[]>) => mayBeOk.ok),
        map((response: HttpResponse<ISpent[]>) => response.body)
      )
      .subscribe(
        (res: ISpent[]) => {
          if (!!this.editForm.get('spentId').value) {
            this.spents = res;
          } else {
            this.spentService
              .find(this.editForm.get('spentId').value)
              .pipe(
                filter((subResMayBeOk: HttpResponse<ISpent>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<ISpent>) => subResponse.body)
              )
              .subscribe(
                (subRes: ISpent) => (this.spents = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  updateForm(spentRecurrence: ISpentRecurrence) {
    this.editForm.patchValue({
      id: spentRecurrence.id,
      amount: spentRecurrence.amount,
      recurrenceFrom: spentRecurrence.recurrenceFrom,
      recurrenceTo: spentRecurrence.recurrenceTo,
      dayInMonth: spentRecurrence.dayInMonth,
      spentId: spentRecurrence.spentId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const spentRecurrence = this.createFromForm();
    if (spentRecurrence.id !== undefined) {
      this.subscribeToSaveResponse(this.spentRecurrenceService.update(spentRecurrence));
    } else {
      this.subscribeToSaveResponse(this.spentRecurrenceService.create(spentRecurrence));
    }
  }

  private createFromForm(): ISpentRecurrence {
    return {
      ...new SpentRecurrence(),
      id: this.editForm.get(['id']).value,
      amount: this.editForm.get(['amount']).value,
      recurrenceFrom: this.editForm.get(['recurrenceFrom']).value,
      recurrenceTo: this.editForm.get(['recurrenceTo']).value,
      dayInMonth: this.editForm.get(['dayInMonth']).value,
      spentId: this.editForm.get(['spentId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISpentRecurrence>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackSpentById(index: number, item: ISpent) {
    return item.id;
  }
}
