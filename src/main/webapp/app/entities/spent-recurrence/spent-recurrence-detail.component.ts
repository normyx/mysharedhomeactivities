import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISpentRecurrence } from 'app/shared/model/spent-recurrence.model';

@Component({
  selector: 'msha-spent-recurrence-detail',
  templateUrl: './spent-recurrence-detail.component.html'
})
export class SpentRecurrenceDetailComponent implements OnInit {
  spentRecurrence: ISpentRecurrence;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ spentRecurrence }) => {
      this.spentRecurrence = spentRecurrence;
    });
  }

  previousState() {
    window.history.back();
  }
}
