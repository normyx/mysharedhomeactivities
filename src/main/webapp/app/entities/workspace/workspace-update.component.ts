import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IWorkspace, Workspace } from 'app/shared/model/workspace.model';
import { WorkspaceService } from './workspace.service';
import { IProfil } from 'app/shared/model/profil.model';
import { ProfilService } from 'app/entities/profil';

@Component({
  selector: 'msha-workspace-update',
  templateUrl: './workspace-update.component.html'
})
export class WorkspaceUpdateComponent implements OnInit {
  isSaving: boolean;

  profils: IProfil[];

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(40)]],
    owners: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected workspaceService: WorkspaceService,
    protected profilService: ProfilService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ workspace }) => {
      this.updateForm(workspace);
    });
    this.profilService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IProfil[]>) => mayBeOk.ok),
        map((response: HttpResponse<IProfil[]>) => response.body)
      )
      .subscribe((res: IProfil[]) => (this.profils = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(workspace: IWorkspace) {
    this.editForm.patchValue({
      id: workspace.id,
      label: workspace.label,
      owners: workspace.owners
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const workspace = this.createFromForm();
    if (workspace.id !== undefined) {
      this.subscribeToSaveResponse(this.workspaceService.update(workspace));
    } else {
      this.subscribeToSaveResponse(this.workspaceService.create(workspace));
    }
  }

  private createFromForm(): IWorkspace {
    return {
      ...new Workspace(),
      id: this.editForm.get(['id']).value,
      label: this.editForm.get(['label']).value,
      owners: this.editForm.get(['owners']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWorkspace>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackProfilById(index: number, item: IProfil) {
    return item.id;
  }

  getSelected(selectedVals: Array<any>, option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
