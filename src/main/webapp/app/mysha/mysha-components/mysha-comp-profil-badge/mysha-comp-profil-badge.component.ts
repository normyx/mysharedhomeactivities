import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils, JhiAlertService } from 'ng-jhipster';
import { MySHAProfilBasgeService } from './mysha-comp-profil-badge.service';

import { IProfil, Profil } from 'app/shared/model/profil.model';
import { filter, map } from 'rxjs/operators';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'msha-mysha-comp-profil-badge',
  styleUrls: ['mysha-comp-profil-badge.scss'],
  templateUrl: './mysha-comp-profil-badge.component.html'
})
export class MySHACompProfilBadgeComponent implements OnInit, OnChanges {

  profil: IProfil;
  @Input() profilId: number;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute, protected jhiAlertService: JhiAlertService, protected profilService: MySHAProfilBasgeService) { }

  ngOnInit() {

  }

  loadAll() {
    this.profilService.find(this.profilId)
      .pipe(
        filter((res: HttpResponse<IProfil>) => res.ok),
        map((res: HttpResponse<IProfil>) => res.body)
      )
      .subscribe(
        (res: IProfil) => {
          this.profil = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnChanges(): void {
    this.loadAll();
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
