import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'mysha-task-project',
        loadChildren: './mysha-task-project/mysha-task-project.module#MySHAMySharedHomeActivitiesTaskProjectModule'
      },
      {
        path: 'mysha-task',
        loadChildren: './mysha-task/mysha-task.module#MySHAMySharedHomeActivitiesTaskModule'
      },
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MySHAMySharedHomeActivitiesEntityModule {}
