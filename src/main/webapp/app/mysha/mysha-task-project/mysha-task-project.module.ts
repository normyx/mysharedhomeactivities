import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';
// import { MySHAMySharedHomeActivitiesTaskModule } from 'app/mysha/mysha-task/mysha-task.module';

import { MySharedHomeActivitiesSharedModule } from 'app/shared';
import {
  MySHATaskProjectComponent,
  MySHATaskProjectDetailComponent,
  mySHATaskProjectRoute,
  mySHATaskProjectPopupRoute
} from './';

import { MySHAMySharedHomeActivitiesTaskModule } from '../mysha-task/mysha-task.module';

const ENTITY_STATES = [...mySHATaskProjectRoute, ...mySHATaskProjectPopupRoute];

@NgModule({
  imports: [
    MySharedHomeActivitiesSharedModule,
    MySHAMySharedHomeActivitiesTaskModule,
    RouterModule.forChild(ENTITY_STATES)
  ],
  declarations: [
    MySHATaskProjectComponent,
    MySHATaskProjectDetailComponent
  ],
  entryComponents: [MySHATaskProjectComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MySHAMySharedHomeActivitiesTaskProjectModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
