import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { TaskProject } from 'app/shared/model/task-project.model';
import { MySHATaskProjectService } from './mysha-task-project.service';
import { ITaskProject } from 'app/shared/model/task-project.model';
import { MySHATaskProjectDetailComponent } from './mysha-task-project-detail.component';
import { MySHATaskProjectComponent } from './mysha-task-project.component';

@Injectable({ providedIn: 'root' })
export class MySHATaskProjectResolve implements Resolve<ITaskProject> {
  constructor(private service: MySHATaskProjectService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITaskProject> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<TaskProject>) => response.ok),
        map((taskProject: HttpResponse<TaskProject>) => taskProject.body)
      );
    }
    return of(new TaskProject());
  }
}

export const mySHATaskProjectRoute: Routes = [
  {
    path: '',
    component: MySHATaskProjectComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.taskProject.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: MySHATaskProjectDetailComponent,
    resolve: {
      taskProject: MySHATaskProjectResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'mySharedHomeActivitiesApp.taskProject.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
];

export const mySHATaskProjectPopupRoute: Routes = [
];
