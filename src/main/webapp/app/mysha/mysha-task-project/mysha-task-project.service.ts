import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { ITaskProject } from 'app/shared/model/task-project.model';
import { TaskProjectService } from 'app/entities/task-project';

type EntityResponseType = HttpResponse<ITaskProject>;
type EntityArrayResponseType = HttpResponse<ITaskProject[]>;

@Injectable({ providedIn: 'root' })
export class MySHATaskProjectService extends TaskProjectService {

  constructor(protected http: HttpClient) {
    super(http);
  }

  findOwnedByLoggedUser(): Observable<EntityArrayResponseType> {
    return this.http.get<ITaskProject[]>(SERVER_API_URL + 'api/mysha-task-projects-owned-by-logged-user', { observe: 'response' });
  }
}
