import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ITaskProject } from 'app/shared/model/task-project.model';
import { AccountService } from 'app/core';
import { MySHATaskProjectService } from './mysha-task-project.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'msha-mysha-task-project',
  styleUrls: ['mysha-task-project.scss'],
  templateUrl: './mysha-task-project.component.html',
})
export class MySHATaskProjectComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['label', 'owners'];
  taskProjects: ITaskProject[];
  currentAccount: any;
  eventSubscriber: Subscription;
  taskProjectsDataSource: MatTableDataSource<ITaskProject>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(
    protected taskProjectService: MySHATaskProjectService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) { }

  loadAll() {
    this.taskProjectService
      .findOwnedByLoggedUser()
      .pipe(
        filter((res: HttpResponse<ITaskProject[]>) => res.ok),
        map((res: HttpResponse<ITaskProject[]>) => res.body)
      )
      .subscribe(
        (res: ITaskProject[]) => {
          this.taskProjects = res;
          this.taskProjectsDataSource = new MatTableDataSource(this.taskProjects);
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInTaskProjects();
  }

  applyFilter(filterValue: string) {
    this.taskProjectsDataSource.filter = filterValue.trim().toLowerCase();

    if (this.taskProjectsDataSource.paginator) {
      this.taskProjectsDataSource.paginator.firstPage();
    }
  }
  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ITaskProject) {
    return item.id;
  }

  registerChangeInTaskProjects() {
    this.eventSubscriber = this.eventManager.subscribe('taskProjectListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
