import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ITask } from 'app/shared/model/task.model';
import { TaskService } from 'app/entities/task';

type EntityResponseType = HttpResponse<ITask>;
type EntityArrayResponseType = HttpResponse<ITask[]>;

@Injectable({ providedIn: 'root' })
export class MySHACompTaskService extends TaskService {
  public myshaResourceUrl = SERVER_API_URL + 'api/mysha-tasks';

  constructor(protected http: HttpClient) {
    super(http);
  }

  findWhereProjectTaskId(projectTaskId: number): Observable<EntityArrayResponseType> {
    return this.http
      .get<ITask[]>(SERVER_API_URL + 'api/mysha-tasks-where-task-project' + `/${projectTaskId}`, { observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

}
