import { Component, OnInit, OnDestroy, Input, OnChanges, ViewChild } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ITask } from 'app/shared/model/task.model';
import { AccountService } from 'app/core';
import { MySHACompTaskService } from './mysha-task.service';
import { MatDialog } from '@angular/material';
import { MySHATaskUpdateDialogComponent } from 'app/mysha/mysha-task';
import { ITaskProject } from 'app/shared/model/task-project.model';
import { MySHATaskProjectService } from '../mysha-task-project';

@Component({
  selector: 'msha-mysha-task-list',
  styleUrls: ['mysha-task-list.scss'],
  templateUrl: './mysha-task-list.component.html'
})
export class MySHATaskListComponent implements OnInit, OnChanges, OnDestroy {
  tasks: ITask[];
  @Input() taskProject: ITaskProject;
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected taskService: MySHACompTaskService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService,
    public dialog: MatDialog
  ) { }

  loadAll() {
    if (this.taskProject) {
      this.taskService
        .findWhereProjectTaskId(this.taskProject.id)
        .pipe(
          filter((res: HttpResponse<ITask[]>) => res.ok),
          map((res: HttpResponse<ITask[]>) => res.body)
        )
        .subscribe(
          (res: ITask[]) => {
            this.tasks = res;
          },
          (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInTasks();
  }

  ngOnChanges() {
    this.loadAll();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ITask) {
    return item.id;
  }

  registerChangeInTasks() {
    this.eventSubscriber = this.eventManager.subscribe('myshaTaskListModification', response => this.loadAll());
  }

  updateTask(task: ITask) {
    this.taskService.update(task).subscribe((res: HttpResponse<ITask>) => { }, (res: HttpErrorResponse) => { });
  }

  openUpdateDialogTask(task: ITask): void {
    const dialogRef = this.dialog.open(MySHATaskUpdateDialogComponent, {
      autoFocus: true,
      maxWidth: '400px',
      width: '80%',
      data: Object.assign({}, task)
    });
    dialogRef.beforeClosed().subscribe(result => {
      this.updateTask(result);
    });
    dialogRef.afterClosed().subscribe(result => {
      this.eventManager.broadcast({
        name: 'myshaTaskListModification',
        content: 'Modify a task'
      });
    });
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
