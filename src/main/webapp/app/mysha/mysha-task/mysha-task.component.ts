import { Component, OnInit, OnDestroy, Input, OnChanges } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ITask } from 'app/shared/model/task.model';
import { AccountService } from 'app/core';

@Component({
  selector: 'msha-mysha-task',
  templateUrl: './mysha-task.component.html'
})
export class MySHATaskComponent implements OnInit {
  @Input() taskProjectId: number;

  constructor(  ) { }

  loadAll() {
  }

  ngOnInit() {
    this.loadAll();
  }

}
