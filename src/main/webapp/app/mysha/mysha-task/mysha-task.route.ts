import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Task } from 'app/shared/model/task.model';
import { MySHACompTaskService } from 'app/mysha/mysha-task/mysha-task.service';
import { MySHATaskComponent } from './mysha-task.component';
import { ITask } from 'app/shared/model/task.model';

@Injectable({ providedIn: 'root' })
export class MySHACompTaskResolve implements Resolve<ITask> {
  constructor(private service: MySHACompTaskService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITask> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Task>) => response.ok),
        map((task: HttpResponse<Task>) => task.body)
      );
    }
    return of(new Task());
  }
}

export const mySHATaskRoute: Routes = [
];

export const mySHATaskPopupRoute: Routes = [
];
