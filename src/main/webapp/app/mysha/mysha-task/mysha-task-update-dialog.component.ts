import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDatepickerInputEvent } from '@angular/material';
import { ITask } from 'app/shared/model/task.model';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

import { FormControl } from '@angular/forms';

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'ddd D MMM YY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'msha-mysha-task-update-dialog',
  templateUrl: 'mysha-task-update-dialog.component.html',
  providers: [
    // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
    // `MatMomentDateModule` in your applications root module. We provide it at the component level
    // here, due to limitations of our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class MySHATaskUpdateDialogComponent {

  dateFormControl: FormControl;

  constructor(
    public dialogRef: MatDialogRef<MySHATaskUpdateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public task: ITask) {
      this.dateFormControl = new FormControl({ value: this.task.dueDate, disabled: true });
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  dueDateChanged(event: MatDatepickerInputEvent<Date>) {
    this.task.dueDate = this.dateFormControl.value;
  }
}
