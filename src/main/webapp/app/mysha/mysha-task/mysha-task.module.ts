import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { MySharedHomeActivitiesSharedModule } from 'app/shared';
import {
  MySHATaskComponent,
  MySHATaskListComponent,
  MySHATaskUpdateDialogComponent,
  mySHATaskRoute,
  mySHATaskPopupRoute
} from './';

import { MySHAMySharedHomeActivitiesCompProfilBadgeModule } from '../mysha-components/mysha-comp-profil-badge/mysha-comp-profil-badge.module';

const ENTITY_STATES = [...mySHATaskRoute, ...mySHATaskPopupRoute];

@NgModule({
  imports: [MySharedHomeActivitiesSharedModule, RouterModule.forChild(ENTITY_STATES), MySHAMySharedHomeActivitiesCompProfilBadgeModule],
  declarations: [MySHATaskComponent, MySHATaskUpdateDialogComponent, MySHATaskListComponent],
  entryComponents: [MySHATaskComponent, MySHATaskUpdateDialogComponent, MySHATaskListComponent],
  exports: [MySHATaskListComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MySHAMySharedHomeActivitiesTaskModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
