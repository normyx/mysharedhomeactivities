import './vendor.ts';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { NgJhipsterModule } from 'ng-jhipster';
import { MatIconModule } from '@angular/material/icon';

import { AuthInterceptor } from './blocks/interceptor/auth.interceptor';
import { AuthExpiredInterceptor } from './blocks/interceptor/auth-expired.interceptor';
import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { MySharedHomeActivitiesSharedModule } from 'app/shared';
import { MySharedHomeActivitiesCoreModule } from 'app/core';
import { MySharedHomeActivitiesAppRoutingModule } from './app-routing.module';
import { MySharedHomeActivitiesHomeModule } from './home/home.module';
import { MySharedHomeActivitiesAccountModule } from './account/account.module';
import { MySharedHomeActivitiesEntityModule } from './entities/entity.module';
import * as moment from 'moment';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MshaMainComponent, NavbarComponent, FooterComponent, PageRibbonComponent, ActiveMenuDirective, ErrorComponent } from './layouts';
import { MySHAMySharedHomeActivitiesEntityModule } from './mysha/mysha.module';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NgxWebstorageModule.forRoot({ prefix: 'msha', separator: '-' }),
    NgJhipsterModule.forRoot({
      // set below to true to make alerts look like toast
      alertAsToast: false,
      alertTimeout: 5000,
      i18nEnabled: true,
      defaultI18nLang: 'fr'
    }),
    MySharedHomeActivitiesSharedModule.forRoot(),
    MySharedHomeActivitiesCoreModule,
    MySharedHomeActivitiesHomeModule,
    MySharedHomeActivitiesAccountModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    MySharedHomeActivitiesEntityModule,
    MySHAMySharedHomeActivitiesEntityModule,
    MySharedHomeActivitiesAppRoutingModule,
    MatIconModule
  ],
  declarations: [MshaMainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthExpiredInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorHandlerInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: NotificationInterceptor,
      multi: true
    }
  ],
  bootstrap: [MshaMainComponent]
})
export class MySharedHomeActivitiesAppModule {
  constructor(private dpConfig: NgbDatepickerConfig) {
    this.dpConfig.minDate = { year: moment().year() - 100, month: 1, day: 1 };
  }
}
