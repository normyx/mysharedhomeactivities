package com.mgoulene.msha.web.rest;

import com.mgoulene.msha.MySharedHomeActivitiesApp;
import com.mgoulene.msha.domain.ShoppingItem;
import com.mgoulene.msha.domain.ShoppingList;
import com.mgoulene.msha.repository.ShoppingItemRepository;
import com.mgoulene.msha.service.ShoppingItemService;
import com.mgoulene.msha.service.dto.ShoppingItemDTO;
import com.mgoulene.msha.service.mapper.ShoppingItemMapper;
import com.mgoulene.msha.web.rest.errors.ExceptionTranslator;
import com.mgoulene.msha.service.dto.ShoppingItemCriteria;
import com.mgoulene.msha.service.ShoppingItemQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.mgoulene.msha.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ShoppingItemResource} REST controller.
 */
@SpringBootTest(classes = MySharedHomeActivitiesApp.class)
public class ShoppingItemResourceIT {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final Boolean DEFAULT_DONE = false;
    private static final Boolean UPDATED_DONE = true;

    @Autowired
    private ShoppingItemRepository shoppingItemRepository;

    @Autowired
    private ShoppingItemMapper shoppingItemMapper;

    @Autowired
    private ShoppingItemService shoppingItemService;

    @Autowired
    private ShoppingItemQueryService shoppingItemQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restShoppingItemMockMvc;

    private ShoppingItem shoppingItem;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ShoppingItemResource shoppingItemResource = new ShoppingItemResource(shoppingItemService, shoppingItemQueryService);
        this.restShoppingItemMockMvc = MockMvcBuilders.standaloneSetup(shoppingItemResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShoppingItem createEntity(EntityManager em) {
        ShoppingItem shoppingItem = new ShoppingItem()
            .label(DEFAULT_LABEL)
            .done(DEFAULT_DONE);
        return shoppingItem;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShoppingItem createUpdatedEntity(EntityManager em) {
        ShoppingItem shoppingItem = new ShoppingItem()
            .label(UPDATED_LABEL)
            .done(UPDATED_DONE);
        return shoppingItem;
    }

    @BeforeEach
    public void initTest() {
        shoppingItem = createEntity(em);
    }

    @Test
    @Transactional
    public void createShoppingItem() throws Exception {
        int databaseSizeBeforeCreate = shoppingItemRepository.findAll().size();

        // Create the ShoppingItem
        ShoppingItemDTO shoppingItemDTO = shoppingItemMapper.toDto(shoppingItem);
        restShoppingItemMockMvc.perform(post("/api/shopping-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shoppingItemDTO)))
            .andExpect(status().isCreated());

        // Validate the ShoppingItem in the database
        List<ShoppingItem> shoppingItemList = shoppingItemRepository.findAll();
        assertThat(shoppingItemList).hasSize(databaseSizeBeforeCreate + 1);
        ShoppingItem testShoppingItem = shoppingItemList.get(shoppingItemList.size() - 1);
        assertThat(testShoppingItem.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testShoppingItem.isDone()).isEqualTo(DEFAULT_DONE);
    }

    @Test
    @Transactional
    public void createShoppingItemWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = shoppingItemRepository.findAll().size();

        // Create the ShoppingItem with an existing ID
        shoppingItem.setId(1L);
        ShoppingItemDTO shoppingItemDTO = shoppingItemMapper.toDto(shoppingItem);

        // An entity with an existing ID cannot be created, so this API call must fail
        restShoppingItemMockMvc.perform(post("/api/shopping-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shoppingItemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ShoppingItem in the database
        List<ShoppingItem> shoppingItemList = shoppingItemRepository.findAll();
        assertThat(shoppingItemList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = shoppingItemRepository.findAll().size();
        // set the field null
        shoppingItem.setLabel(null);

        // Create the ShoppingItem, which fails.
        ShoppingItemDTO shoppingItemDTO = shoppingItemMapper.toDto(shoppingItem);

        restShoppingItemMockMvc.perform(post("/api/shopping-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shoppingItemDTO)))
            .andExpect(status().isBadRequest());

        List<ShoppingItem> shoppingItemList = shoppingItemRepository.findAll();
        assertThat(shoppingItemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDoneIsRequired() throws Exception {
        int databaseSizeBeforeTest = shoppingItemRepository.findAll().size();
        // set the field null
        shoppingItem.setDone(null);

        // Create the ShoppingItem, which fails.
        ShoppingItemDTO shoppingItemDTO = shoppingItemMapper.toDto(shoppingItem);

        restShoppingItemMockMvc.perform(post("/api/shopping-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shoppingItemDTO)))
            .andExpect(status().isBadRequest());

        List<ShoppingItem> shoppingItemList = shoppingItemRepository.findAll();
        assertThat(shoppingItemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllShoppingItems() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList
        restShoppingItemMockMvc.perform(get("/api/shopping-items?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shoppingItem.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL.toString())))
            .andExpect(jsonPath("$.[*].done").value(hasItem(DEFAULT_DONE.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getShoppingItem() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get the shoppingItem
        restShoppingItemMockMvc.perform(get("/api/shopping-items/{id}", shoppingItem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(shoppingItem.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL.toString()))
            .andExpect(jsonPath("$.done").value(DEFAULT_DONE.booleanValue()));
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByLabelIsEqualToSomething() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where label equals to DEFAULT_LABEL
        defaultShoppingItemShouldBeFound("label.equals=" + DEFAULT_LABEL);

        // Get all the shoppingItemList where label equals to UPDATED_LABEL
        defaultShoppingItemShouldNotBeFound("label.equals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByLabelIsInShouldWork() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where label in DEFAULT_LABEL or UPDATED_LABEL
        defaultShoppingItemShouldBeFound("label.in=" + DEFAULT_LABEL + "," + UPDATED_LABEL);

        // Get all the shoppingItemList where label equals to UPDATED_LABEL
        defaultShoppingItemShouldNotBeFound("label.in=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByLabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where label is not null
        defaultShoppingItemShouldBeFound("label.specified=true");

        // Get all the shoppingItemList where label is null
        defaultShoppingItemShouldNotBeFound("label.specified=false");
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByDoneIsEqualToSomething() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where done equals to DEFAULT_DONE
        defaultShoppingItemShouldBeFound("done.equals=" + DEFAULT_DONE);

        // Get all the shoppingItemList where done equals to UPDATED_DONE
        defaultShoppingItemShouldNotBeFound("done.equals=" + UPDATED_DONE);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByDoneIsInShouldWork() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where done in DEFAULT_DONE or UPDATED_DONE
        defaultShoppingItemShouldBeFound("done.in=" + DEFAULT_DONE + "," + UPDATED_DONE);

        // Get all the shoppingItemList where done equals to UPDATED_DONE
        defaultShoppingItemShouldNotBeFound("done.in=" + UPDATED_DONE);
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByDoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        // Get all the shoppingItemList where done is not null
        defaultShoppingItemShouldBeFound("done.specified=true");

        // Get all the shoppingItemList where done is null
        defaultShoppingItemShouldNotBeFound("done.specified=false");
    }

    @Test
    @Transactional
    public void getAllShoppingItemsByListIsEqualToSomething() throws Exception {
        // Initialize the database
        ShoppingList list = ShoppingListResourceIT.createEntity(em);
        em.persist(list);
        em.flush();
        shoppingItem.setList(list);
        shoppingItemRepository.saveAndFlush(shoppingItem);
        Long listId = list.getId();

        // Get all the shoppingItemList where list equals to listId
        defaultShoppingItemShouldBeFound("listId.equals=" + listId);

        // Get all the shoppingItemList where list equals to listId + 1
        defaultShoppingItemShouldNotBeFound("listId.equals=" + (listId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultShoppingItemShouldBeFound(String filter) throws Exception {
        restShoppingItemMockMvc.perform(get("/api/shopping-items?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shoppingItem.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].done").value(hasItem(DEFAULT_DONE.booleanValue())));

        // Check, that the count call also returns 1
        restShoppingItemMockMvc.perform(get("/api/shopping-items/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultShoppingItemShouldNotBeFound(String filter) throws Exception {
        restShoppingItemMockMvc.perform(get("/api/shopping-items?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restShoppingItemMockMvc.perform(get("/api/shopping-items/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingShoppingItem() throws Exception {
        // Get the shoppingItem
        restShoppingItemMockMvc.perform(get("/api/shopping-items/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateShoppingItem() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        int databaseSizeBeforeUpdate = shoppingItemRepository.findAll().size();

        // Update the shoppingItem
        ShoppingItem updatedShoppingItem = shoppingItemRepository.findById(shoppingItem.getId()).get();
        // Disconnect from session so that the updates on updatedShoppingItem are not directly saved in db
        em.detach(updatedShoppingItem);
        updatedShoppingItem
            .label(UPDATED_LABEL)
            .done(UPDATED_DONE);
        ShoppingItemDTO shoppingItemDTO = shoppingItemMapper.toDto(updatedShoppingItem);

        restShoppingItemMockMvc.perform(put("/api/shopping-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shoppingItemDTO)))
            .andExpect(status().isOk());

        // Validate the ShoppingItem in the database
        List<ShoppingItem> shoppingItemList = shoppingItemRepository.findAll();
        assertThat(shoppingItemList).hasSize(databaseSizeBeforeUpdate);
        ShoppingItem testShoppingItem = shoppingItemList.get(shoppingItemList.size() - 1);
        assertThat(testShoppingItem.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testShoppingItem.isDone()).isEqualTo(UPDATED_DONE);
    }

    @Test
    @Transactional
    public void updateNonExistingShoppingItem() throws Exception {
        int databaseSizeBeforeUpdate = shoppingItemRepository.findAll().size();

        // Create the ShoppingItem
        ShoppingItemDTO shoppingItemDTO = shoppingItemMapper.toDto(shoppingItem);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restShoppingItemMockMvc.perform(put("/api/shopping-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shoppingItemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ShoppingItem in the database
        List<ShoppingItem> shoppingItemList = shoppingItemRepository.findAll();
        assertThat(shoppingItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteShoppingItem() throws Exception {
        // Initialize the database
        shoppingItemRepository.saveAndFlush(shoppingItem);

        int databaseSizeBeforeDelete = shoppingItemRepository.findAll().size();

        // Delete the shoppingItem
        restShoppingItemMockMvc.perform(delete("/api/shopping-items/{id}", shoppingItem.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ShoppingItem> shoppingItemList = shoppingItemRepository.findAll();
        assertThat(shoppingItemList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShoppingItem.class);
        ShoppingItem shoppingItem1 = new ShoppingItem();
        shoppingItem1.setId(1L);
        ShoppingItem shoppingItem2 = new ShoppingItem();
        shoppingItem2.setId(shoppingItem1.getId());
        assertThat(shoppingItem1).isEqualTo(shoppingItem2);
        shoppingItem2.setId(2L);
        assertThat(shoppingItem1).isNotEqualTo(shoppingItem2);
        shoppingItem1.setId(null);
        assertThat(shoppingItem1).isNotEqualTo(shoppingItem2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShoppingItemDTO.class);
        ShoppingItemDTO shoppingItemDTO1 = new ShoppingItemDTO();
        shoppingItemDTO1.setId(1L);
        ShoppingItemDTO shoppingItemDTO2 = new ShoppingItemDTO();
        assertThat(shoppingItemDTO1).isNotEqualTo(shoppingItemDTO2);
        shoppingItemDTO2.setId(shoppingItemDTO1.getId());
        assertThat(shoppingItemDTO1).isEqualTo(shoppingItemDTO2);
        shoppingItemDTO2.setId(2L);
        assertThat(shoppingItemDTO1).isNotEqualTo(shoppingItemDTO2);
        shoppingItemDTO1.setId(null);
        assertThat(shoppingItemDTO1).isNotEqualTo(shoppingItemDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(shoppingItemMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(shoppingItemMapper.fromId(null)).isNull();
    }
}
