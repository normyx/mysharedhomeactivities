package com.mgoulene.msha.web.rest;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import com.mgoulene.msha.MySharedHomeActivitiesApp;
import com.mgoulene.msha.domain.Task;
import com.mgoulene.msha.domain.TaskProject;
import com.mgoulene.msha.domain.User;
import com.mgoulene.msha.domain.Profil;
import com.mgoulene.msha.repository.MySHATaskProjectRepository;
import com.mgoulene.msha.repository.MySHATaskRepository;
import com.mgoulene.msha.repository.ProfilRepository;
import com.mgoulene.msha.repository.TaskProjectRepository;
import com.mgoulene.msha.repository.TaskRepository;
import com.mgoulene.msha.service.TaskService;
import com.mgoulene.msha.service.UserService;
import com.mgoulene.msha.service.dto.TaskDTO;
import com.mgoulene.msha.service.dto.TaskProjectDTO;
import com.mgoulene.msha.service.mapper.ProfilMapper;
import com.mgoulene.msha.service.mapper.TaskMapper;
import com.mgoulene.msha.service.mapper.TaskProjectMapper;
import com.mgoulene.msha.web.rest.errors.ExceptionTranslator;
import com.mgoulene.msha.service.dto.ProfilDTO;
import com.mgoulene.msha.service.dto.TaskCriteria;
import com.mgoulene.msha.service.MySHATaskProjectService;
import com.mgoulene.msha.service.MySHATaskService;
import com.mgoulene.msha.service.ProfilQueryService;
import com.mgoulene.msha.service.ProfilService;
import com.mgoulene.msha.service.TaskProjectQueryService;
import com.mgoulene.msha.service.TaskProjectService;
import com.mgoulene.msha.service.TaskQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;
import org.springframework.web.context.WebApplicationContext;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static com.mgoulene.msha.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link TaskResource} REST controller.
 */
@SpringBootTest(classes = MySharedHomeActivitiesApp.class)
public class MySHATaskResourceIT {

    @Autowired
    private MySHATaskRepository mySHATaskRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private TaskProjectRepository taskProjectRepository;

    @Autowired
    private MySHATaskProjectRepository mySHATaskProjectRepository;

    @Mock
    private MySHATaskRepository mySHATaskRepositoryMock;

    @Autowired
    private TaskMapper taskMapper;

    @Autowired
    private TaskProjectMapper taskProjectMapper;

    @Mock
    private MySHATaskService mySHATaskServiceMock;

    @Autowired
    private MySHATaskService mySHATaskService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private TaskQueryService taskQueryService;

    @Autowired
    private TaskProjectService taskProjectService;

    @Autowired
    private TaskProjectQueryService taskProjectQueryService;

    @Autowired
    private MySHATaskProjectService mySHATaskProjectService;

    @Autowired
    private UserService userService;

    @Autowired
    private ProfilRepository profilRepository;

    @Autowired
    private ProfilMapper profilMapper;

    @Autowired
    private ProfilService profilService;

    @Autowired
    private ProfilQueryService profilQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private Validator validator;

    @Autowired
    private WebApplicationContext context;

    private MockMvc restMySHATaskMockMvc;

    private MockMvc restTaskMockMvc;

    private MockMvc restTaskProjectMockMvc;

    private MockMvc restMySHATaskProjectMockMvc;

    private MockMvc restProfilMockMvc;

    // private Task task;

    // private TaskProject taskProject;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TaskResource taskResource = new TaskResource(taskService, taskQueryService);
        this.restTaskMockMvc = MockMvcBuilders.standaloneSetup(taskResource)
                .setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
                .setConversionService(createFormattingConversionService()).setMessageConverters(jacksonMessageConverter)
                .setValidator(validator).build();
        final MySHATaskResource mySHATaskResource = new MySHATaskResource(taskService, taskQueryService,
                mySHATaskService);
        this.restMySHATaskMockMvc = MockMvcBuilders.standaloneSetup(mySHATaskResource)
                .setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
                .setConversionService(createFormattingConversionService()).setMessageConverters(jacksonMessageConverter)
                .setValidator(validator).build();
        final TaskProjectResource taskProjectResource = new TaskProjectResource(taskProjectService,
                taskProjectQueryService);
        this.restTaskProjectMockMvc = MockMvcBuilders.standaloneSetup(taskProjectResource)
                .setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
                .setConversionService(createFormattingConversionService()).setMessageConverters(jacksonMessageConverter)
                .setValidator(validator).build();
        final MySHATaskProjectResource mySHATaskProjectResource = new MySHATaskProjectResource(taskProjectService,
                userService, mySHATaskProjectService);
        this.restMySHATaskProjectMockMvc = MockMvcBuilders.standaloneSetup(mySHATaskProjectResource)
                .setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
                .setConversionService(createFormattingConversionService()).setMessageConverters(jacksonMessageConverter)
                .setValidator(validator).build();
        final ProfilResource profilResource = new ProfilResource(profilService, profilQueryService);
        this.restProfilMockMvc = MockMvcBuilders.standaloneSetup(profilResource)
                .setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
                .setConversionService(createFormattingConversionService()).setMessageConverters(jacksonMessageConverter)
                .setValidator(validator).build();
    }

    @BeforeEach
    public void initTest() {
        // task = createTask(em);
        // taskProject = createTaskProject(em);
    }

    @Test
    @Transactional
    public void createEntities() throws Exception {
        int databaseSizeBeforeCreateTask = taskRepository.findAll().size();
        int databaseSizeBeforeCreateTaskProject = taskProjectRepository.findAll().size();
        int databaseSizeBeforeCreateProfil = profilRepository.findAll().size();

        User user = userService.getUserWithAuthoritiesByLogin("admin").get();
        Profil profil = new Profil().displayName("profil").user(user);
        ProfilDTO profilDTO = profilMapper.toDto(profil);
        restProfilMockMvc.perform(post("/api/profils").contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(profilDTO))).andExpect(status().isCreated());
        // Validate the Profil in the database
        List<Profil> profilList = profilRepository.findAll();
        assertThat(profilList).hasSize(databaseSizeBeforeCreateProfil + 1);
        Profil testProfil = profilList.get(profilList.size() - 1);
        assertThat(testProfil.getDisplayName()).isEqualTo("profil");

        TaskProject taskProject = new TaskProject().label("label").addOwner(testProfil);
        TaskProjectDTO taskProjectDTO = taskProjectMapper.toDto(taskProject);

        restTaskProjectMockMvc.perform(post("/api/task-projects").contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(taskProjectDTO))).andExpect(status().isCreated());

        // Validate the TaskProject in the database
        List<TaskProject> taskProjectList = taskProjectRepository.findAll();
        assertThat(taskProjectList).hasSize(databaseSizeBeforeCreateTaskProject + 1);
        TaskProject testTaskProject = taskProjectList.get(taskProjectList.size() - 1);
        assertThat(testTaskProject.getLabel()).isEqualTo("label");

        Task task = new Task().label("label").description("description").done(false).dueDate(LocalDate.ofEpochDay(0L)).priority(0)
                .taskProject(testTaskProject);
        // Create the Task
        TaskDTO taskDTO = taskMapper.toDto(task);
        restTaskMockMvc.perform(post("/api/tasks").contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(taskDTO))).andExpect(status().isCreated());

        // Validate the Task in the database
        List<Task> taskList = taskRepository.findAll();
        assertThat(taskList).hasSize(databaseSizeBeforeCreateTask + 1);
        Task testTask = taskList.get(taskList.size() - 1);
        assertThat(testTask.getLabel()).isEqualTo("label");
        assertThat(testTask.getDescription()).isEqualTo("description");
        assertThat(testTask.isDone()).isEqualTo(false);
        assertThat(testTask.getPriority()).isEqualTo(0);
        assertThat(testTask.getDueDate()).isEqualTo(LocalDate.ofEpochDay(0L));

        // Initialize the database
        taskProjectRepository.saveAndFlush(taskProject);

        // Get all the taskProjectList
        MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build()
                .perform(get("/api/mysha-task-projects-owned-by-logged-user").with(user("admin")))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(testTaskProject.getId().intValue())))
                .andExpect(jsonPath("$.[*].label").value(hasItem("label"))).andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").isNotEmpty());
        MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build()
                .perform(get("/api/mysha-task-projects-owned-by-logged-user").with(user("user")))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$").isArray()).andExpect(jsonPath("$").isEmpty());

        restMySHATaskMockMvc
                .perform(get("/api/mysha-tasks-where-task-project/{taskProjectId}", testTaskProject.getId()))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(testTask.getId().intValue()));
    }

}
