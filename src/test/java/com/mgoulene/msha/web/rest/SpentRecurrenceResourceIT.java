package com.mgoulene.msha.web.rest;

import com.mgoulene.msha.MySharedHomeActivitiesApp;
import com.mgoulene.msha.domain.SpentRecurrence;
import com.mgoulene.msha.domain.Spent;
import com.mgoulene.msha.repository.SpentRecurrenceRepository;
import com.mgoulene.msha.service.SpentRecurrenceService;
import com.mgoulene.msha.service.dto.SpentRecurrenceDTO;
import com.mgoulene.msha.service.mapper.SpentRecurrenceMapper;
import com.mgoulene.msha.web.rest.errors.ExceptionTranslator;
import com.mgoulene.msha.service.dto.SpentRecurrenceCriteria;
import com.mgoulene.msha.service.SpentRecurrenceQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.mgoulene.msha.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link SpentRecurrenceResource} REST controller.
 */
@SpringBootTest(classes = MySharedHomeActivitiesApp.class)
public class SpentRecurrenceResourceIT {

    private static final Float DEFAULT_AMOUNT = 1F;
    private static final Float UPDATED_AMOUNT = 2F;

    private static final LocalDate DEFAULT_RECURRENCE_FROM = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_RECURRENCE_FROM = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_RECURRENCE_TO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_RECURRENCE_TO = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_DAY_IN_MONTH = 1;
    private static final Integer UPDATED_DAY_IN_MONTH = 2;

    @Autowired
    private SpentRecurrenceRepository spentRecurrenceRepository;

    @Autowired
    private SpentRecurrenceMapper spentRecurrenceMapper;

    @Autowired
    private SpentRecurrenceService spentRecurrenceService;

    @Autowired
    private SpentRecurrenceQueryService spentRecurrenceQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSpentRecurrenceMockMvc;

    private SpentRecurrence spentRecurrence;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SpentRecurrenceResource spentRecurrenceResource = new SpentRecurrenceResource(spentRecurrenceService, spentRecurrenceQueryService);
        this.restSpentRecurrenceMockMvc = MockMvcBuilders.standaloneSetup(spentRecurrenceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SpentRecurrence createEntity(EntityManager em) {
        SpentRecurrence spentRecurrence = new SpentRecurrence()
            .amount(DEFAULT_AMOUNT)
            .recurrenceFrom(DEFAULT_RECURRENCE_FROM)
            .recurrenceTo(DEFAULT_RECURRENCE_TO)
            .dayInMonth(DEFAULT_DAY_IN_MONTH);
        // Add required entity
        Spent spent;
        if (TestUtil.findAll(em, Spent.class).isEmpty()) {
            spent = SpentResourceIT.createEntity(em);
            em.persist(spent);
            em.flush();
        } else {
            spent = TestUtil.findAll(em, Spent.class).get(0);
        }
        spentRecurrence.setSpent(spent);
        return spentRecurrence;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SpentRecurrence createUpdatedEntity(EntityManager em) {
        SpentRecurrence spentRecurrence = new SpentRecurrence()
            .amount(UPDATED_AMOUNT)
            .recurrenceFrom(UPDATED_RECURRENCE_FROM)
            .recurrenceTo(UPDATED_RECURRENCE_TO)
            .dayInMonth(UPDATED_DAY_IN_MONTH);
        // Add required entity
        Spent spent;
        if (TestUtil.findAll(em, Spent.class).isEmpty()) {
            spent = SpentResourceIT.createUpdatedEntity(em);
            em.persist(spent);
            em.flush();
        } else {
            spent = TestUtil.findAll(em, Spent.class).get(0);
        }
        spentRecurrence.setSpent(spent);
        return spentRecurrence;
    }

    @BeforeEach
    public void initTest() {
        spentRecurrence = createEntity(em);
    }

    @Test
    @Transactional
    public void createSpentRecurrence() throws Exception {
        int databaseSizeBeforeCreate = spentRecurrenceRepository.findAll().size();

        // Create the SpentRecurrence
        SpentRecurrenceDTO spentRecurrenceDTO = spentRecurrenceMapper.toDto(spentRecurrence);
        restSpentRecurrenceMockMvc.perform(post("/api/spent-recurrences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentRecurrenceDTO)))
            .andExpect(status().isCreated());

        // Validate the SpentRecurrence in the database
        List<SpentRecurrence> spentRecurrenceList = spentRecurrenceRepository.findAll();
        assertThat(spentRecurrenceList).hasSize(databaseSizeBeforeCreate + 1);
        SpentRecurrence testSpentRecurrence = spentRecurrenceList.get(spentRecurrenceList.size() - 1);
        assertThat(testSpentRecurrence.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testSpentRecurrence.getRecurrenceFrom()).isEqualTo(DEFAULT_RECURRENCE_FROM);
        assertThat(testSpentRecurrence.getRecurrenceTo()).isEqualTo(DEFAULT_RECURRENCE_TO);
        assertThat(testSpentRecurrence.getDayInMonth()).isEqualTo(DEFAULT_DAY_IN_MONTH);
    }

    @Test
    @Transactional
    public void createSpentRecurrenceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = spentRecurrenceRepository.findAll().size();

        // Create the SpentRecurrence with an existing ID
        spentRecurrence.setId(1L);
        SpentRecurrenceDTO spentRecurrenceDTO = spentRecurrenceMapper.toDto(spentRecurrence);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSpentRecurrenceMockMvc.perform(post("/api/spent-recurrences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentRecurrenceDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SpentRecurrence in the database
        List<SpentRecurrence> spentRecurrenceList = spentRecurrenceRepository.findAll();
        assertThat(spentRecurrenceList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkAmountIsRequired() throws Exception {
        int databaseSizeBeforeTest = spentRecurrenceRepository.findAll().size();
        // set the field null
        spentRecurrence.setAmount(null);

        // Create the SpentRecurrence, which fails.
        SpentRecurrenceDTO spentRecurrenceDTO = spentRecurrenceMapper.toDto(spentRecurrence);

        restSpentRecurrenceMockMvc.perform(post("/api/spent-recurrences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentRecurrenceDTO)))
            .andExpect(status().isBadRequest());

        List<SpentRecurrence> spentRecurrenceList = spentRecurrenceRepository.findAll();
        assertThat(spentRecurrenceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRecurrenceFromIsRequired() throws Exception {
        int databaseSizeBeforeTest = spentRecurrenceRepository.findAll().size();
        // set the field null
        spentRecurrence.setRecurrenceFrom(null);

        // Create the SpentRecurrence, which fails.
        SpentRecurrenceDTO spentRecurrenceDTO = spentRecurrenceMapper.toDto(spentRecurrence);

        restSpentRecurrenceMockMvc.perform(post("/api/spent-recurrences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentRecurrenceDTO)))
            .andExpect(status().isBadRequest());

        List<SpentRecurrence> spentRecurrenceList = spentRecurrenceRepository.findAll();
        assertThat(spentRecurrenceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDayInMonthIsRequired() throws Exception {
        int databaseSizeBeforeTest = spentRecurrenceRepository.findAll().size();
        // set the field null
        spentRecurrence.setDayInMonth(null);

        // Create the SpentRecurrence, which fails.
        SpentRecurrenceDTO spentRecurrenceDTO = spentRecurrenceMapper.toDto(spentRecurrence);

        restSpentRecurrenceMockMvc.perform(post("/api/spent-recurrences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentRecurrenceDTO)))
            .andExpect(status().isBadRequest());

        List<SpentRecurrence> spentRecurrenceList = spentRecurrenceRepository.findAll();
        assertThat(spentRecurrenceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSpentRecurrences() throws Exception {
        // Initialize the database
        spentRecurrenceRepository.saveAndFlush(spentRecurrence);

        // Get all the spentRecurrenceList
        restSpentRecurrenceMockMvc.perform(get("/api/spent-recurrences?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(spentRecurrence.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].recurrenceFrom").value(hasItem(DEFAULT_RECURRENCE_FROM.toString())))
            .andExpect(jsonPath("$.[*].recurrenceTo").value(hasItem(DEFAULT_RECURRENCE_TO.toString())))
            .andExpect(jsonPath("$.[*].dayInMonth").value(hasItem(DEFAULT_DAY_IN_MONTH)));
    }
    
    @Test
    @Transactional
    public void getSpentRecurrence() throws Exception {
        // Initialize the database
        spentRecurrenceRepository.saveAndFlush(spentRecurrence);

        // Get the spentRecurrence
        restSpentRecurrenceMockMvc.perform(get("/api/spent-recurrences/{id}", spentRecurrence.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(spentRecurrence.getId().intValue()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.recurrenceFrom").value(DEFAULT_RECURRENCE_FROM.toString()))
            .andExpect(jsonPath("$.recurrenceTo").value(DEFAULT_RECURRENCE_TO.toString()))
            .andExpect(jsonPath("$.dayInMonth").value(DEFAULT_DAY_IN_MONTH));
    }

    @Test
    @Transactional
    public void getAllSpentRecurrencesByAmountIsEqualToSomething() throws Exception {
        // Initialize the database
        spentRecurrenceRepository.saveAndFlush(spentRecurrence);

        // Get all the spentRecurrenceList where amount equals to DEFAULT_AMOUNT
        defaultSpentRecurrenceShouldBeFound("amount.equals=" + DEFAULT_AMOUNT);

        // Get all the spentRecurrenceList where amount equals to UPDATED_AMOUNT
        defaultSpentRecurrenceShouldNotBeFound("amount.equals=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllSpentRecurrencesByAmountIsInShouldWork() throws Exception {
        // Initialize the database
        spentRecurrenceRepository.saveAndFlush(spentRecurrence);

        // Get all the spentRecurrenceList where amount in DEFAULT_AMOUNT or UPDATED_AMOUNT
        defaultSpentRecurrenceShouldBeFound("amount.in=" + DEFAULT_AMOUNT + "," + UPDATED_AMOUNT);

        // Get all the spentRecurrenceList where amount equals to UPDATED_AMOUNT
        defaultSpentRecurrenceShouldNotBeFound("amount.in=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllSpentRecurrencesByAmountIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentRecurrenceRepository.saveAndFlush(spentRecurrence);

        // Get all the spentRecurrenceList where amount is not null
        defaultSpentRecurrenceShouldBeFound("amount.specified=true");

        // Get all the spentRecurrenceList where amount is null
        defaultSpentRecurrenceShouldNotBeFound("amount.specified=false");
    }

    @Test
    @Transactional
    public void getAllSpentRecurrencesByRecurrenceFromIsEqualToSomething() throws Exception {
        // Initialize the database
        spentRecurrenceRepository.saveAndFlush(spentRecurrence);

        // Get all the spentRecurrenceList where recurrenceFrom equals to DEFAULT_RECURRENCE_FROM
        defaultSpentRecurrenceShouldBeFound("recurrenceFrom.equals=" + DEFAULT_RECURRENCE_FROM);

        // Get all the spentRecurrenceList where recurrenceFrom equals to UPDATED_RECURRENCE_FROM
        defaultSpentRecurrenceShouldNotBeFound("recurrenceFrom.equals=" + UPDATED_RECURRENCE_FROM);
    }

    @Test
    @Transactional
    public void getAllSpentRecurrencesByRecurrenceFromIsInShouldWork() throws Exception {
        // Initialize the database
        spentRecurrenceRepository.saveAndFlush(spentRecurrence);

        // Get all the spentRecurrenceList where recurrenceFrom in DEFAULT_RECURRENCE_FROM or UPDATED_RECURRENCE_FROM
        defaultSpentRecurrenceShouldBeFound("recurrenceFrom.in=" + DEFAULT_RECURRENCE_FROM + "," + UPDATED_RECURRENCE_FROM);

        // Get all the spentRecurrenceList where recurrenceFrom equals to UPDATED_RECURRENCE_FROM
        defaultSpentRecurrenceShouldNotBeFound("recurrenceFrom.in=" + UPDATED_RECURRENCE_FROM);
    }

    @Test
    @Transactional
    public void getAllSpentRecurrencesByRecurrenceFromIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentRecurrenceRepository.saveAndFlush(spentRecurrence);

        // Get all the spentRecurrenceList where recurrenceFrom is not null
        defaultSpentRecurrenceShouldBeFound("recurrenceFrom.specified=true");

        // Get all the spentRecurrenceList where recurrenceFrom is null
        defaultSpentRecurrenceShouldNotBeFound("recurrenceFrom.specified=false");
    }

    @Test
    @Transactional
    public void getAllSpentRecurrencesByRecurrenceFromIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentRecurrenceRepository.saveAndFlush(spentRecurrence);

        // Get all the spentRecurrenceList where recurrenceFrom greater than or equals to DEFAULT_RECURRENCE_FROM
        defaultSpentRecurrenceShouldBeFound("recurrenceFrom.greaterOrEqualThan=" + DEFAULT_RECURRENCE_FROM);

        // Get all the spentRecurrenceList where recurrenceFrom greater than or equals to UPDATED_RECURRENCE_FROM
        defaultSpentRecurrenceShouldNotBeFound("recurrenceFrom.greaterOrEqualThan=" + UPDATED_RECURRENCE_FROM);
    }

    @Test
    @Transactional
    public void getAllSpentRecurrencesByRecurrenceFromIsLessThanSomething() throws Exception {
        // Initialize the database
        spentRecurrenceRepository.saveAndFlush(spentRecurrence);

        // Get all the spentRecurrenceList where recurrenceFrom less than or equals to DEFAULT_RECURRENCE_FROM
        defaultSpentRecurrenceShouldNotBeFound("recurrenceFrom.lessThan=" + DEFAULT_RECURRENCE_FROM);

        // Get all the spentRecurrenceList where recurrenceFrom less than or equals to UPDATED_RECURRENCE_FROM
        defaultSpentRecurrenceShouldBeFound("recurrenceFrom.lessThan=" + UPDATED_RECURRENCE_FROM);
    }


    @Test
    @Transactional
    public void getAllSpentRecurrencesByRecurrenceToIsEqualToSomething() throws Exception {
        // Initialize the database
        spentRecurrenceRepository.saveAndFlush(spentRecurrence);

        // Get all the spentRecurrenceList where recurrenceTo equals to DEFAULT_RECURRENCE_TO
        defaultSpentRecurrenceShouldBeFound("recurrenceTo.equals=" + DEFAULT_RECURRENCE_TO);

        // Get all the spentRecurrenceList where recurrenceTo equals to UPDATED_RECURRENCE_TO
        defaultSpentRecurrenceShouldNotBeFound("recurrenceTo.equals=" + UPDATED_RECURRENCE_TO);
    }

    @Test
    @Transactional
    public void getAllSpentRecurrencesByRecurrenceToIsInShouldWork() throws Exception {
        // Initialize the database
        spentRecurrenceRepository.saveAndFlush(spentRecurrence);

        // Get all the spentRecurrenceList where recurrenceTo in DEFAULT_RECURRENCE_TO or UPDATED_RECURRENCE_TO
        defaultSpentRecurrenceShouldBeFound("recurrenceTo.in=" + DEFAULT_RECURRENCE_TO + "," + UPDATED_RECURRENCE_TO);

        // Get all the spentRecurrenceList where recurrenceTo equals to UPDATED_RECURRENCE_TO
        defaultSpentRecurrenceShouldNotBeFound("recurrenceTo.in=" + UPDATED_RECURRENCE_TO);
    }

    @Test
    @Transactional
    public void getAllSpentRecurrencesByRecurrenceToIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentRecurrenceRepository.saveAndFlush(spentRecurrence);

        // Get all the spentRecurrenceList where recurrenceTo is not null
        defaultSpentRecurrenceShouldBeFound("recurrenceTo.specified=true");

        // Get all the spentRecurrenceList where recurrenceTo is null
        defaultSpentRecurrenceShouldNotBeFound("recurrenceTo.specified=false");
    }

    @Test
    @Transactional
    public void getAllSpentRecurrencesByRecurrenceToIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentRecurrenceRepository.saveAndFlush(spentRecurrence);

        // Get all the spentRecurrenceList where recurrenceTo greater than or equals to DEFAULT_RECURRENCE_TO
        defaultSpentRecurrenceShouldBeFound("recurrenceTo.greaterOrEqualThan=" + DEFAULT_RECURRENCE_TO);

        // Get all the spentRecurrenceList where recurrenceTo greater than or equals to UPDATED_RECURRENCE_TO
        defaultSpentRecurrenceShouldNotBeFound("recurrenceTo.greaterOrEqualThan=" + UPDATED_RECURRENCE_TO);
    }

    @Test
    @Transactional
    public void getAllSpentRecurrencesByRecurrenceToIsLessThanSomething() throws Exception {
        // Initialize the database
        spentRecurrenceRepository.saveAndFlush(spentRecurrence);

        // Get all the spentRecurrenceList where recurrenceTo less than or equals to DEFAULT_RECURRENCE_TO
        defaultSpentRecurrenceShouldNotBeFound("recurrenceTo.lessThan=" + DEFAULT_RECURRENCE_TO);

        // Get all the spentRecurrenceList where recurrenceTo less than or equals to UPDATED_RECURRENCE_TO
        defaultSpentRecurrenceShouldBeFound("recurrenceTo.lessThan=" + UPDATED_RECURRENCE_TO);
    }


    @Test
    @Transactional
    public void getAllSpentRecurrencesByDayInMonthIsEqualToSomething() throws Exception {
        // Initialize the database
        spentRecurrenceRepository.saveAndFlush(spentRecurrence);

        // Get all the spentRecurrenceList where dayInMonth equals to DEFAULT_DAY_IN_MONTH
        defaultSpentRecurrenceShouldBeFound("dayInMonth.equals=" + DEFAULT_DAY_IN_MONTH);

        // Get all the spentRecurrenceList where dayInMonth equals to UPDATED_DAY_IN_MONTH
        defaultSpentRecurrenceShouldNotBeFound("dayInMonth.equals=" + UPDATED_DAY_IN_MONTH);
    }

    @Test
    @Transactional
    public void getAllSpentRecurrencesByDayInMonthIsInShouldWork() throws Exception {
        // Initialize the database
        spentRecurrenceRepository.saveAndFlush(spentRecurrence);

        // Get all the spentRecurrenceList where dayInMonth in DEFAULT_DAY_IN_MONTH or UPDATED_DAY_IN_MONTH
        defaultSpentRecurrenceShouldBeFound("dayInMonth.in=" + DEFAULT_DAY_IN_MONTH + "," + UPDATED_DAY_IN_MONTH);

        // Get all the spentRecurrenceList where dayInMonth equals to UPDATED_DAY_IN_MONTH
        defaultSpentRecurrenceShouldNotBeFound("dayInMonth.in=" + UPDATED_DAY_IN_MONTH);
    }

    @Test
    @Transactional
    public void getAllSpentRecurrencesByDayInMonthIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentRecurrenceRepository.saveAndFlush(spentRecurrence);

        // Get all the spentRecurrenceList where dayInMonth is not null
        defaultSpentRecurrenceShouldBeFound("dayInMonth.specified=true");

        // Get all the spentRecurrenceList where dayInMonth is null
        defaultSpentRecurrenceShouldNotBeFound("dayInMonth.specified=false");
    }

    @Test
    @Transactional
    public void getAllSpentRecurrencesByDayInMonthIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentRecurrenceRepository.saveAndFlush(spentRecurrence);

        // Get all the spentRecurrenceList where dayInMonth greater than or equals to DEFAULT_DAY_IN_MONTH
        defaultSpentRecurrenceShouldBeFound("dayInMonth.greaterOrEqualThan=" + DEFAULT_DAY_IN_MONTH);

        // Get all the spentRecurrenceList where dayInMonth greater than or equals to (DEFAULT_DAY_IN_MONTH + 1)
        defaultSpentRecurrenceShouldNotBeFound("dayInMonth.greaterOrEqualThan=" + (DEFAULT_DAY_IN_MONTH + 1));
    }

    @Test
    @Transactional
    public void getAllSpentRecurrencesByDayInMonthIsLessThanSomething() throws Exception {
        // Initialize the database
        spentRecurrenceRepository.saveAndFlush(spentRecurrence);

        // Get all the spentRecurrenceList where dayInMonth less than or equals to DEFAULT_DAY_IN_MONTH
        defaultSpentRecurrenceShouldNotBeFound("dayInMonth.lessThan=" + DEFAULT_DAY_IN_MONTH);

        // Get all the spentRecurrenceList where dayInMonth less than or equals to (DEFAULT_DAY_IN_MONTH + 1)
        defaultSpentRecurrenceShouldBeFound("dayInMonth.lessThan=" + (DEFAULT_DAY_IN_MONTH + 1));
    }


    @Test
    @Transactional
    public void getAllSpentRecurrencesBySpentIsEqualToSomething() throws Exception {
        // Get already existing entity
        Spent spent = spentRecurrence.getSpent();
        spentRecurrenceRepository.saveAndFlush(spentRecurrence);
        Long spentId = spent.getId();

        // Get all the spentRecurrenceList where spent equals to spentId
        defaultSpentRecurrenceShouldBeFound("spentId.equals=" + spentId);

        // Get all the spentRecurrenceList where spent equals to spentId + 1
        defaultSpentRecurrenceShouldNotBeFound("spentId.equals=" + (spentId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSpentRecurrenceShouldBeFound(String filter) throws Exception {
        restSpentRecurrenceMockMvc.perform(get("/api/spent-recurrences?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(spentRecurrence.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].recurrenceFrom").value(hasItem(DEFAULT_RECURRENCE_FROM.toString())))
            .andExpect(jsonPath("$.[*].recurrenceTo").value(hasItem(DEFAULT_RECURRENCE_TO.toString())))
            .andExpect(jsonPath("$.[*].dayInMonth").value(hasItem(DEFAULT_DAY_IN_MONTH)));

        // Check, that the count call also returns 1
        restSpentRecurrenceMockMvc.perform(get("/api/spent-recurrences/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSpentRecurrenceShouldNotBeFound(String filter) throws Exception {
        restSpentRecurrenceMockMvc.perform(get("/api/spent-recurrences?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSpentRecurrenceMockMvc.perform(get("/api/spent-recurrences/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingSpentRecurrence() throws Exception {
        // Get the spentRecurrence
        restSpentRecurrenceMockMvc.perform(get("/api/spent-recurrences/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSpentRecurrence() throws Exception {
        // Initialize the database
        spentRecurrenceRepository.saveAndFlush(spentRecurrence);

        int databaseSizeBeforeUpdate = spentRecurrenceRepository.findAll().size();

        // Update the spentRecurrence
        SpentRecurrence updatedSpentRecurrence = spentRecurrenceRepository.findById(spentRecurrence.getId()).get();
        // Disconnect from session so that the updates on updatedSpentRecurrence are not directly saved in db
        em.detach(updatedSpentRecurrence);
        updatedSpentRecurrence
            .amount(UPDATED_AMOUNT)
            .recurrenceFrom(UPDATED_RECURRENCE_FROM)
            .recurrenceTo(UPDATED_RECURRENCE_TO)
            .dayInMonth(UPDATED_DAY_IN_MONTH);
        SpentRecurrenceDTO spentRecurrenceDTO = spentRecurrenceMapper.toDto(updatedSpentRecurrence);

        restSpentRecurrenceMockMvc.perform(put("/api/spent-recurrences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentRecurrenceDTO)))
            .andExpect(status().isOk());

        // Validate the SpentRecurrence in the database
        List<SpentRecurrence> spentRecurrenceList = spentRecurrenceRepository.findAll();
        assertThat(spentRecurrenceList).hasSize(databaseSizeBeforeUpdate);
        SpentRecurrence testSpentRecurrence = spentRecurrenceList.get(spentRecurrenceList.size() - 1);
        assertThat(testSpentRecurrence.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testSpentRecurrence.getRecurrenceFrom()).isEqualTo(UPDATED_RECURRENCE_FROM);
        assertThat(testSpentRecurrence.getRecurrenceTo()).isEqualTo(UPDATED_RECURRENCE_TO);
        assertThat(testSpentRecurrence.getDayInMonth()).isEqualTo(UPDATED_DAY_IN_MONTH);
    }

    @Test
    @Transactional
    public void updateNonExistingSpentRecurrence() throws Exception {
        int databaseSizeBeforeUpdate = spentRecurrenceRepository.findAll().size();

        // Create the SpentRecurrence
        SpentRecurrenceDTO spentRecurrenceDTO = spentRecurrenceMapper.toDto(spentRecurrence);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSpentRecurrenceMockMvc.perform(put("/api/spent-recurrences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentRecurrenceDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SpentRecurrence in the database
        List<SpentRecurrence> spentRecurrenceList = spentRecurrenceRepository.findAll();
        assertThat(spentRecurrenceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSpentRecurrence() throws Exception {
        // Initialize the database
        spentRecurrenceRepository.saveAndFlush(spentRecurrence);

        int databaseSizeBeforeDelete = spentRecurrenceRepository.findAll().size();

        // Delete the spentRecurrence
        restSpentRecurrenceMockMvc.perform(delete("/api/spent-recurrences/{id}", spentRecurrence.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SpentRecurrence> spentRecurrenceList = spentRecurrenceRepository.findAll();
        assertThat(spentRecurrenceList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpentRecurrence.class);
        SpentRecurrence spentRecurrence1 = new SpentRecurrence();
        spentRecurrence1.setId(1L);
        SpentRecurrence spentRecurrence2 = new SpentRecurrence();
        spentRecurrence2.setId(spentRecurrence1.getId());
        assertThat(spentRecurrence1).isEqualTo(spentRecurrence2);
        spentRecurrence2.setId(2L);
        assertThat(spentRecurrence1).isNotEqualTo(spentRecurrence2);
        spentRecurrence1.setId(null);
        assertThat(spentRecurrence1).isNotEqualTo(spentRecurrence2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpentRecurrenceDTO.class);
        SpentRecurrenceDTO spentRecurrenceDTO1 = new SpentRecurrenceDTO();
        spentRecurrenceDTO1.setId(1L);
        SpentRecurrenceDTO spentRecurrenceDTO2 = new SpentRecurrenceDTO();
        assertThat(spentRecurrenceDTO1).isNotEqualTo(spentRecurrenceDTO2);
        spentRecurrenceDTO2.setId(spentRecurrenceDTO1.getId());
        assertThat(spentRecurrenceDTO1).isEqualTo(spentRecurrenceDTO2);
        spentRecurrenceDTO2.setId(2L);
        assertThat(spentRecurrenceDTO1).isNotEqualTo(spentRecurrenceDTO2);
        spentRecurrenceDTO1.setId(null);
        assertThat(spentRecurrenceDTO1).isNotEqualTo(spentRecurrenceDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(spentRecurrenceMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(spentRecurrenceMapper.fromId(null)).isNull();
    }
}
