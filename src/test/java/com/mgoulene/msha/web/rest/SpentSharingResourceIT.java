package com.mgoulene.msha.web.rest;

import com.mgoulene.msha.MySharedHomeActivitiesApp;
import com.mgoulene.msha.domain.SpentSharing;
import com.mgoulene.msha.domain.Profil;
import com.mgoulene.msha.domain.Spent;
import com.mgoulene.msha.repository.SpentSharingRepository;
import com.mgoulene.msha.service.SpentSharingService;
import com.mgoulene.msha.service.dto.SpentSharingDTO;
import com.mgoulene.msha.service.mapper.SpentSharingMapper;
import com.mgoulene.msha.web.rest.errors.ExceptionTranslator;
import com.mgoulene.msha.service.dto.SpentSharingCriteria;
import com.mgoulene.msha.service.SpentSharingQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.mgoulene.msha.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link SpentSharingResource} REST controller.
 */
@SpringBootTest(classes = MySharedHomeActivitiesApp.class)
public class SpentSharingResourceIT {

    private static final Float DEFAULT_AMOUNT_SHARE = 1F;
    private static final Float UPDATED_AMOUNT_SHARE = 2F;

    private static final Float DEFAULT_SHARE = 1F;
    private static final Float UPDATED_SHARE = 2F;

    @Autowired
    private SpentSharingRepository spentSharingRepository;

    @Autowired
    private SpentSharingMapper spentSharingMapper;

    @Autowired
    private SpentSharingService spentSharingService;

    @Autowired
    private SpentSharingQueryService spentSharingQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSpentSharingMockMvc;

    private SpentSharing spentSharing;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SpentSharingResource spentSharingResource = new SpentSharingResource(spentSharingService, spentSharingQueryService);
        this.restSpentSharingMockMvc = MockMvcBuilders.standaloneSetup(spentSharingResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SpentSharing createEntity(EntityManager em) {
        SpentSharing spentSharing = new SpentSharing()
            .amountShare(DEFAULT_AMOUNT_SHARE)
            .share(DEFAULT_SHARE);
        return spentSharing;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SpentSharing createUpdatedEntity(EntityManager em) {
        SpentSharing spentSharing = new SpentSharing()
            .amountShare(UPDATED_AMOUNT_SHARE)
            .share(UPDATED_SHARE);
        return spentSharing;
    }

    @BeforeEach
    public void initTest() {
        spentSharing = createEntity(em);
    }

    @Test
    @Transactional
    public void createSpentSharing() throws Exception {
        int databaseSizeBeforeCreate = spentSharingRepository.findAll().size();

        // Create the SpentSharing
        SpentSharingDTO spentSharingDTO = spentSharingMapper.toDto(spentSharing);
        restSpentSharingMockMvc.perform(post("/api/spent-sharings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingDTO)))
            .andExpect(status().isCreated());

        // Validate the SpentSharing in the database
        List<SpentSharing> spentSharingList = spentSharingRepository.findAll();
        assertThat(spentSharingList).hasSize(databaseSizeBeforeCreate + 1);
        SpentSharing testSpentSharing = spentSharingList.get(spentSharingList.size() - 1);
        assertThat(testSpentSharing.getAmountShare()).isEqualTo(DEFAULT_AMOUNT_SHARE);
        assertThat(testSpentSharing.getShare()).isEqualTo(DEFAULT_SHARE);
    }

    @Test
    @Transactional
    public void createSpentSharingWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = spentSharingRepository.findAll().size();

        // Create the SpentSharing with an existing ID
        spentSharing.setId(1L);
        SpentSharingDTO spentSharingDTO = spentSharingMapper.toDto(spentSharing);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSpentSharingMockMvc.perform(post("/api/spent-sharings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SpentSharing in the database
        List<SpentSharing> spentSharingList = spentSharingRepository.findAll();
        assertThat(spentSharingList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkAmountShareIsRequired() throws Exception {
        int databaseSizeBeforeTest = spentSharingRepository.findAll().size();
        // set the field null
        spentSharing.setAmountShare(null);

        // Create the SpentSharing, which fails.
        SpentSharingDTO spentSharingDTO = spentSharingMapper.toDto(spentSharing);

        restSpentSharingMockMvc.perform(post("/api/spent-sharings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingDTO)))
            .andExpect(status().isBadRequest());

        List<SpentSharing> spentSharingList = spentSharingRepository.findAll();
        assertThat(spentSharingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkShareIsRequired() throws Exception {
        int databaseSizeBeforeTest = spentSharingRepository.findAll().size();
        // set the field null
        spentSharing.setShare(null);

        // Create the SpentSharing, which fails.
        SpentSharingDTO spentSharingDTO = spentSharingMapper.toDto(spentSharing);

        restSpentSharingMockMvc.perform(post("/api/spent-sharings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingDTO)))
            .andExpect(status().isBadRequest());

        List<SpentSharing> spentSharingList = spentSharingRepository.findAll();
        assertThat(spentSharingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSpentSharings() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList
        restSpentSharingMockMvc.perform(get("/api/spent-sharings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(spentSharing.getId().intValue())))
            .andExpect(jsonPath("$.[*].amountShare").value(hasItem(DEFAULT_AMOUNT_SHARE.doubleValue())))
            .andExpect(jsonPath("$.[*].share").value(hasItem(DEFAULT_SHARE.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getSpentSharing() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get the spentSharing
        restSpentSharingMockMvc.perform(get("/api/spent-sharings/{id}", spentSharing.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(spentSharing.getId().intValue()))
            .andExpect(jsonPath("$.amountShare").value(DEFAULT_AMOUNT_SHARE.doubleValue()))
            .andExpect(jsonPath("$.share").value(DEFAULT_SHARE.doubleValue()));
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByAmountShareIsEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where amountShare equals to DEFAULT_AMOUNT_SHARE
        defaultSpentSharingShouldBeFound("amountShare.equals=" + DEFAULT_AMOUNT_SHARE);

        // Get all the spentSharingList where amountShare equals to UPDATED_AMOUNT_SHARE
        defaultSpentSharingShouldNotBeFound("amountShare.equals=" + UPDATED_AMOUNT_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByAmountShareIsInShouldWork() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where amountShare in DEFAULT_AMOUNT_SHARE or UPDATED_AMOUNT_SHARE
        defaultSpentSharingShouldBeFound("amountShare.in=" + DEFAULT_AMOUNT_SHARE + "," + UPDATED_AMOUNT_SHARE);

        // Get all the spentSharingList where amountShare equals to UPDATED_AMOUNT_SHARE
        defaultSpentSharingShouldNotBeFound("amountShare.in=" + UPDATED_AMOUNT_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByAmountShareIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where amountShare is not null
        defaultSpentSharingShouldBeFound("amountShare.specified=true");

        // Get all the spentSharingList where amountShare is null
        defaultSpentSharingShouldNotBeFound("amountShare.specified=false");
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByShareIsEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where share equals to DEFAULT_SHARE
        defaultSpentSharingShouldBeFound("share.equals=" + DEFAULT_SHARE);

        // Get all the spentSharingList where share equals to UPDATED_SHARE
        defaultSpentSharingShouldNotBeFound("share.equals=" + UPDATED_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByShareIsInShouldWork() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where share in DEFAULT_SHARE or UPDATED_SHARE
        defaultSpentSharingShouldBeFound("share.in=" + DEFAULT_SHARE + "," + UPDATED_SHARE);

        // Get all the spentSharingList where share equals to UPDATED_SHARE
        defaultSpentSharingShouldNotBeFound("share.in=" + UPDATED_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingsByShareIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        // Get all the spentSharingList where share is not null
        defaultSpentSharingShouldBeFound("share.specified=true");

        // Get all the spentSharingList where share is null
        defaultSpentSharingShouldNotBeFound("share.specified=false");
    }

    @Test
    @Transactional
    public void getAllSpentSharingsBySharingUserIsEqualToSomething() throws Exception {
        // Initialize the database
        Profil sharingUser = ProfilResourceIT.createEntity(em);
        em.persist(sharingUser);
        em.flush();
        spentSharing.setSharingUser(sharingUser);
        spentSharingRepository.saveAndFlush(spentSharing);
        Long sharingUserId = sharingUser.getId();

        // Get all the spentSharingList where sharingUser equals to sharingUserId
        defaultSpentSharingShouldBeFound("sharingUserId.equals=" + sharingUserId);

        // Get all the spentSharingList where sharingUser equals to sharingUserId + 1
        defaultSpentSharingShouldNotBeFound("sharingUserId.equals=" + (sharingUserId + 1));
    }


    @Test
    @Transactional
    public void getAllSpentSharingsBySpentIsEqualToSomething() throws Exception {
        // Initialize the database
        Spent spent = SpentResourceIT.createEntity(em);
        em.persist(spent);
        em.flush();
        spentSharing.setSpent(spent);
        spentSharingRepository.saveAndFlush(spentSharing);
        Long spentId = spent.getId();

        // Get all the spentSharingList where spent equals to spentId
        defaultSpentSharingShouldBeFound("spentId.equals=" + spentId);

        // Get all the spentSharingList where spent equals to spentId + 1
        defaultSpentSharingShouldNotBeFound("spentId.equals=" + (spentId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSpentSharingShouldBeFound(String filter) throws Exception {
        restSpentSharingMockMvc.perform(get("/api/spent-sharings?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(spentSharing.getId().intValue())))
            .andExpect(jsonPath("$.[*].amountShare").value(hasItem(DEFAULT_AMOUNT_SHARE.doubleValue())))
            .andExpect(jsonPath("$.[*].share").value(hasItem(DEFAULT_SHARE.doubleValue())));

        // Check, that the count call also returns 1
        restSpentSharingMockMvc.perform(get("/api/spent-sharings/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSpentSharingShouldNotBeFound(String filter) throws Exception {
        restSpentSharingMockMvc.perform(get("/api/spent-sharings?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSpentSharingMockMvc.perform(get("/api/spent-sharings/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingSpentSharing() throws Exception {
        // Get the spentSharing
        restSpentSharingMockMvc.perform(get("/api/spent-sharings/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSpentSharing() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        int databaseSizeBeforeUpdate = spentSharingRepository.findAll().size();

        // Update the spentSharing
        SpentSharing updatedSpentSharing = spentSharingRepository.findById(spentSharing.getId()).get();
        // Disconnect from session so that the updates on updatedSpentSharing are not directly saved in db
        em.detach(updatedSpentSharing);
        updatedSpentSharing
            .amountShare(UPDATED_AMOUNT_SHARE)
            .share(UPDATED_SHARE);
        SpentSharingDTO spentSharingDTO = spentSharingMapper.toDto(updatedSpentSharing);

        restSpentSharingMockMvc.perform(put("/api/spent-sharings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingDTO)))
            .andExpect(status().isOk());

        // Validate the SpentSharing in the database
        List<SpentSharing> spentSharingList = spentSharingRepository.findAll();
        assertThat(spentSharingList).hasSize(databaseSizeBeforeUpdate);
        SpentSharing testSpentSharing = spentSharingList.get(spentSharingList.size() - 1);
        assertThat(testSpentSharing.getAmountShare()).isEqualTo(UPDATED_AMOUNT_SHARE);
        assertThat(testSpentSharing.getShare()).isEqualTo(UPDATED_SHARE);
    }

    @Test
    @Transactional
    public void updateNonExistingSpentSharing() throws Exception {
        int databaseSizeBeforeUpdate = spentSharingRepository.findAll().size();

        // Create the SpentSharing
        SpentSharingDTO spentSharingDTO = spentSharingMapper.toDto(spentSharing);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSpentSharingMockMvc.perform(put("/api/spent-sharings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SpentSharing in the database
        List<SpentSharing> spentSharingList = spentSharingRepository.findAll();
        assertThat(spentSharingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSpentSharing() throws Exception {
        // Initialize the database
        spentSharingRepository.saveAndFlush(spentSharing);

        int databaseSizeBeforeDelete = spentSharingRepository.findAll().size();

        // Delete the spentSharing
        restSpentSharingMockMvc.perform(delete("/api/spent-sharings/{id}", spentSharing.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SpentSharing> spentSharingList = spentSharingRepository.findAll();
        assertThat(spentSharingList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpentSharing.class);
        SpentSharing spentSharing1 = new SpentSharing();
        spentSharing1.setId(1L);
        SpentSharing spentSharing2 = new SpentSharing();
        spentSharing2.setId(spentSharing1.getId());
        assertThat(spentSharing1).isEqualTo(spentSharing2);
        spentSharing2.setId(2L);
        assertThat(spentSharing1).isNotEqualTo(spentSharing2);
        spentSharing1.setId(null);
        assertThat(spentSharing1).isNotEqualTo(spentSharing2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpentSharingDTO.class);
        SpentSharingDTO spentSharingDTO1 = new SpentSharingDTO();
        spentSharingDTO1.setId(1L);
        SpentSharingDTO spentSharingDTO2 = new SpentSharingDTO();
        assertThat(spentSharingDTO1).isNotEqualTo(spentSharingDTO2);
        spentSharingDTO2.setId(spentSharingDTO1.getId());
        assertThat(spentSharingDTO1).isEqualTo(spentSharingDTO2);
        spentSharingDTO2.setId(2L);
        assertThat(spentSharingDTO1).isNotEqualTo(spentSharingDTO2);
        spentSharingDTO1.setId(null);
        assertThat(spentSharingDTO1).isNotEqualTo(spentSharingDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(spentSharingMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(spentSharingMapper.fromId(null)).isNull();
    }
}
