package com.mgoulene.msha.web.rest;

import com.mgoulene.msha.MySharedHomeActivitiesApp;
import com.mgoulene.msha.domain.SpentConfig;
import com.mgoulene.msha.domain.Wallet;
import com.mgoulene.msha.domain.SpentSharingConfig;
import com.mgoulene.msha.repository.SpentConfigRepository;
import com.mgoulene.msha.service.SpentConfigService;
import com.mgoulene.msha.service.dto.SpentConfigDTO;
import com.mgoulene.msha.service.mapper.SpentConfigMapper;
import com.mgoulene.msha.web.rest.errors.ExceptionTranslator;
import com.mgoulene.msha.service.dto.SpentConfigCriteria;
import com.mgoulene.msha.service.SpentConfigQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.mgoulene.msha.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link SpentConfigResource} REST controller.
 */
@SpringBootTest(classes = MySharedHomeActivitiesApp.class)
public class SpentConfigResourceIT {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    @Autowired
    private SpentConfigRepository spentConfigRepository;

    @Autowired
    private SpentConfigMapper spentConfigMapper;

    @Autowired
    private SpentConfigService spentConfigService;

    @Autowired
    private SpentConfigQueryService spentConfigQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSpentConfigMockMvc;

    private SpentConfig spentConfig;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SpentConfigResource spentConfigResource = new SpentConfigResource(spentConfigService, spentConfigQueryService);
        this.restSpentConfigMockMvc = MockMvcBuilders.standaloneSetup(spentConfigResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SpentConfig createEntity(EntityManager em) {
        SpentConfig spentConfig = new SpentConfig()
            .label(DEFAULT_LABEL);
        return spentConfig;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SpentConfig createUpdatedEntity(EntityManager em) {
        SpentConfig spentConfig = new SpentConfig()
            .label(UPDATED_LABEL);
        return spentConfig;
    }

    @BeforeEach
    public void initTest() {
        spentConfig = createEntity(em);
    }

    @Test
    @Transactional
    public void createSpentConfig() throws Exception {
        int databaseSizeBeforeCreate = spentConfigRepository.findAll().size();

        // Create the SpentConfig
        SpentConfigDTO spentConfigDTO = spentConfigMapper.toDto(spentConfig);
        restSpentConfigMockMvc.perform(post("/api/spent-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentConfigDTO)))
            .andExpect(status().isCreated());

        // Validate the SpentConfig in the database
        List<SpentConfig> spentConfigList = spentConfigRepository.findAll();
        assertThat(spentConfigList).hasSize(databaseSizeBeforeCreate + 1);
        SpentConfig testSpentConfig = spentConfigList.get(spentConfigList.size() - 1);
        assertThat(testSpentConfig.getLabel()).isEqualTo(DEFAULT_LABEL);
    }

    @Test
    @Transactional
    public void createSpentConfigWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = spentConfigRepository.findAll().size();

        // Create the SpentConfig with an existing ID
        spentConfig.setId(1L);
        SpentConfigDTO spentConfigDTO = spentConfigMapper.toDto(spentConfig);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSpentConfigMockMvc.perform(post("/api/spent-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentConfigDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SpentConfig in the database
        List<SpentConfig> spentConfigList = spentConfigRepository.findAll();
        assertThat(spentConfigList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = spentConfigRepository.findAll().size();
        // set the field null
        spentConfig.setLabel(null);

        // Create the SpentConfig, which fails.
        SpentConfigDTO spentConfigDTO = spentConfigMapper.toDto(spentConfig);

        restSpentConfigMockMvc.perform(post("/api/spent-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentConfigDTO)))
            .andExpect(status().isBadRequest());

        List<SpentConfig> spentConfigList = spentConfigRepository.findAll();
        assertThat(spentConfigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSpentConfigs() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList
        restSpentConfigMockMvc.perform(get("/api/spent-configs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(spentConfig.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL.toString())));
    }
    
    @Test
    @Transactional
    public void getSpentConfig() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get the spentConfig
        restSpentConfigMockMvc.perform(get("/api/spent-configs/{id}", spentConfig.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(spentConfig.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL.toString()));
    }

    @Test
    @Transactional
    public void getAllSpentConfigsByLabelIsEqualToSomething() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where label equals to DEFAULT_LABEL
        defaultSpentConfigShouldBeFound("label.equals=" + DEFAULT_LABEL);

        // Get all the spentConfigList where label equals to UPDATED_LABEL
        defaultSpentConfigShouldNotBeFound("label.equals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllSpentConfigsByLabelIsInShouldWork() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where label in DEFAULT_LABEL or UPDATED_LABEL
        defaultSpentConfigShouldBeFound("label.in=" + DEFAULT_LABEL + "," + UPDATED_LABEL);

        // Get all the spentConfigList where label equals to UPDATED_LABEL
        defaultSpentConfigShouldNotBeFound("label.in=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllSpentConfigsByLabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        // Get all the spentConfigList where label is not null
        defaultSpentConfigShouldBeFound("label.specified=true");

        // Get all the spentConfigList where label is null
        defaultSpentConfigShouldNotBeFound("label.specified=false");
    }

    @Test
    @Transactional
    public void getAllSpentConfigsByWalletIsEqualToSomething() throws Exception {
        // Initialize the database
        Wallet wallet = WalletResourceIT.createEntity(em);
        em.persist(wallet);
        em.flush();
        spentConfig.setWallet(wallet);
        spentConfigRepository.saveAndFlush(spentConfig);
        Long walletId = wallet.getId();

        // Get all the spentConfigList where wallet equals to walletId
        defaultSpentConfigShouldBeFound("walletId.equals=" + walletId);

        // Get all the spentConfigList where wallet equals to walletId + 1
        defaultSpentConfigShouldNotBeFound("walletId.equals=" + (walletId + 1));
    }


    @Test
    @Transactional
    public void getAllSpentConfigsByDefaultSpentSharingConfigIsEqualToSomething() throws Exception {
        // Initialize the database
        SpentSharingConfig defaultSpentSharingConfig = SpentSharingConfigResourceIT.createEntity(em);
        em.persist(defaultSpentSharingConfig);
        em.flush();
        spentConfig.addDefaultSpentSharingConfig(defaultSpentSharingConfig);
        spentConfigRepository.saveAndFlush(spentConfig);
        Long defaultSpentSharingConfigId = defaultSpentSharingConfig.getId();

        // Get all the spentConfigList where defaultSpentSharingConfig equals to defaultSpentSharingConfigId
        defaultSpentConfigShouldBeFound("defaultSpentSharingConfigId.equals=" + defaultSpentSharingConfigId);

        // Get all the spentConfigList where defaultSpentSharingConfig equals to defaultSpentSharingConfigId + 1
        defaultSpentConfigShouldNotBeFound("defaultSpentSharingConfigId.equals=" + (defaultSpentSharingConfigId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSpentConfigShouldBeFound(String filter) throws Exception {
        restSpentConfigMockMvc.perform(get("/api/spent-configs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(spentConfig.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)));

        // Check, that the count call also returns 1
        restSpentConfigMockMvc.perform(get("/api/spent-configs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSpentConfigShouldNotBeFound(String filter) throws Exception {
        restSpentConfigMockMvc.perform(get("/api/spent-configs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSpentConfigMockMvc.perform(get("/api/spent-configs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingSpentConfig() throws Exception {
        // Get the spentConfig
        restSpentConfigMockMvc.perform(get("/api/spent-configs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSpentConfig() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        int databaseSizeBeforeUpdate = spentConfigRepository.findAll().size();

        // Update the spentConfig
        SpentConfig updatedSpentConfig = spentConfigRepository.findById(spentConfig.getId()).get();
        // Disconnect from session so that the updates on updatedSpentConfig are not directly saved in db
        em.detach(updatedSpentConfig);
        updatedSpentConfig
            .label(UPDATED_LABEL);
        SpentConfigDTO spentConfigDTO = spentConfigMapper.toDto(updatedSpentConfig);

        restSpentConfigMockMvc.perform(put("/api/spent-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentConfigDTO)))
            .andExpect(status().isOk());

        // Validate the SpentConfig in the database
        List<SpentConfig> spentConfigList = spentConfigRepository.findAll();
        assertThat(spentConfigList).hasSize(databaseSizeBeforeUpdate);
        SpentConfig testSpentConfig = spentConfigList.get(spentConfigList.size() - 1);
        assertThat(testSpentConfig.getLabel()).isEqualTo(UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void updateNonExistingSpentConfig() throws Exception {
        int databaseSizeBeforeUpdate = spentConfigRepository.findAll().size();

        // Create the SpentConfig
        SpentConfigDTO spentConfigDTO = spentConfigMapper.toDto(spentConfig);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSpentConfigMockMvc.perform(put("/api/spent-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentConfigDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SpentConfig in the database
        List<SpentConfig> spentConfigList = spentConfigRepository.findAll();
        assertThat(spentConfigList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSpentConfig() throws Exception {
        // Initialize the database
        spentConfigRepository.saveAndFlush(spentConfig);

        int databaseSizeBeforeDelete = spentConfigRepository.findAll().size();

        // Delete the spentConfig
        restSpentConfigMockMvc.perform(delete("/api/spent-configs/{id}", spentConfig.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SpentConfig> spentConfigList = spentConfigRepository.findAll();
        assertThat(spentConfigList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpentConfig.class);
        SpentConfig spentConfig1 = new SpentConfig();
        spentConfig1.setId(1L);
        SpentConfig spentConfig2 = new SpentConfig();
        spentConfig2.setId(spentConfig1.getId());
        assertThat(spentConfig1).isEqualTo(spentConfig2);
        spentConfig2.setId(2L);
        assertThat(spentConfig1).isNotEqualTo(spentConfig2);
        spentConfig1.setId(null);
        assertThat(spentConfig1).isNotEqualTo(spentConfig2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpentConfigDTO.class);
        SpentConfigDTO spentConfigDTO1 = new SpentConfigDTO();
        spentConfigDTO1.setId(1L);
        SpentConfigDTO spentConfigDTO2 = new SpentConfigDTO();
        assertThat(spentConfigDTO1).isNotEqualTo(spentConfigDTO2);
        spentConfigDTO2.setId(spentConfigDTO1.getId());
        assertThat(spentConfigDTO1).isEqualTo(spentConfigDTO2);
        spentConfigDTO2.setId(2L);
        assertThat(spentConfigDTO1).isNotEqualTo(spentConfigDTO2);
        spentConfigDTO1.setId(null);
        assertThat(spentConfigDTO1).isNotEqualTo(spentConfigDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(spentConfigMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(spentConfigMapper.fromId(null)).isNull();
    }
}
