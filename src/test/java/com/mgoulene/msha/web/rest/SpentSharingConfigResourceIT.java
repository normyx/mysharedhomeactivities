package com.mgoulene.msha.web.rest;

import com.mgoulene.msha.MySharedHomeActivitiesApp;
import com.mgoulene.msha.domain.SpentSharingConfig;
import com.mgoulene.msha.domain.Profil;
import com.mgoulene.msha.domain.SpentConfig;
import com.mgoulene.msha.repository.SpentSharingConfigRepository;
import com.mgoulene.msha.service.SpentSharingConfigService;
import com.mgoulene.msha.service.dto.SpentSharingConfigDTO;
import com.mgoulene.msha.service.mapper.SpentSharingConfigMapper;
import com.mgoulene.msha.web.rest.errors.ExceptionTranslator;
import com.mgoulene.msha.service.dto.SpentSharingConfigCriteria;
import com.mgoulene.msha.service.SpentSharingConfigQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.mgoulene.msha.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link SpentSharingConfigResource} REST controller.
 */
@SpringBootTest(classes = MySharedHomeActivitiesApp.class)
public class SpentSharingConfigResourceIT {

    private static final Float DEFAULT_SHARE = 1F;
    private static final Float UPDATED_SHARE = 2F;

    @Autowired
    private SpentSharingConfigRepository spentSharingConfigRepository;

    @Autowired
    private SpentSharingConfigMapper spentSharingConfigMapper;

    @Autowired
    private SpentSharingConfigService spentSharingConfigService;

    @Autowired
    private SpentSharingConfigQueryService spentSharingConfigQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSpentSharingConfigMockMvc;

    private SpentSharingConfig spentSharingConfig;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SpentSharingConfigResource spentSharingConfigResource = new SpentSharingConfigResource(spentSharingConfigService, spentSharingConfigQueryService);
        this.restSpentSharingConfigMockMvc = MockMvcBuilders.standaloneSetup(spentSharingConfigResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SpentSharingConfig createEntity(EntityManager em) {
        SpentSharingConfig spentSharingConfig = new SpentSharingConfig()
            .share(DEFAULT_SHARE);
        return spentSharingConfig;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SpentSharingConfig createUpdatedEntity(EntityManager em) {
        SpentSharingConfig spentSharingConfig = new SpentSharingConfig()
            .share(UPDATED_SHARE);
        return spentSharingConfig;
    }

    @BeforeEach
    public void initTest() {
        spentSharingConfig = createEntity(em);
    }

    @Test
    @Transactional
    public void createSpentSharingConfig() throws Exception {
        int databaseSizeBeforeCreate = spentSharingConfigRepository.findAll().size();

        // Create the SpentSharingConfig
        SpentSharingConfigDTO spentSharingConfigDTO = spentSharingConfigMapper.toDto(spentSharingConfig);
        restSpentSharingConfigMockMvc.perform(post("/api/spent-sharing-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingConfigDTO)))
            .andExpect(status().isCreated());

        // Validate the SpentSharingConfig in the database
        List<SpentSharingConfig> spentSharingConfigList = spentSharingConfigRepository.findAll();
        assertThat(spentSharingConfigList).hasSize(databaseSizeBeforeCreate + 1);
        SpentSharingConfig testSpentSharingConfig = spentSharingConfigList.get(spentSharingConfigList.size() - 1);
        assertThat(testSpentSharingConfig.getShare()).isEqualTo(DEFAULT_SHARE);
    }

    @Test
    @Transactional
    public void createSpentSharingConfigWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = spentSharingConfigRepository.findAll().size();

        // Create the SpentSharingConfig with an existing ID
        spentSharingConfig.setId(1L);
        SpentSharingConfigDTO spentSharingConfigDTO = spentSharingConfigMapper.toDto(spentSharingConfig);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSpentSharingConfigMockMvc.perform(post("/api/spent-sharing-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingConfigDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SpentSharingConfig in the database
        List<SpentSharingConfig> spentSharingConfigList = spentSharingConfigRepository.findAll();
        assertThat(spentSharingConfigList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkShareIsRequired() throws Exception {
        int databaseSizeBeforeTest = spentSharingConfigRepository.findAll().size();
        // set the field null
        spentSharingConfig.setShare(null);

        // Create the SpentSharingConfig, which fails.
        SpentSharingConfigDTO spentSharingConfigDTO = spentSharingConfigMapper.toDto(spentSharingConfig);

        restSpentSharingConfigMockMvc.perform(post("/api/spent-sharing-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingConfigDTO)))
            .andExpect(status().isBadRequest());

        List<SpentSharingConfig> spentSharingConfigList = spentSharingConfigRepository.findAll();
        assertThat(spentSharingConfigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSpentSharingConfigs() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        // Get all the spentSharingConfigList
        restSpentSharingConfigMockMvc.perform(get("/api/spent-sharing-configs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(spentSharingConfig.getId().intValue())))
            .andExpect(jsonPath("$.[*].share").value(hasItem(DEFAULT_SHARE.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getSpentSharingConfig() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        // Get the spentSharingConfig
        restSpentSharingConfigMockMvc.perform(get("/api/spent-sharing-configs/{id}", spentSharingConfig.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(spentSharingConfig.getId().intValue()))
            .andExpect(jsonPath("$.share").value(DEFAULT_SHARE.doubleValue()));
    }

    @Test
    @Transactional
    public void getAllSpentSharingConfigsByShareIsEqualToSomething() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        // Get all the spentSharingConfigList where share equals to DEFAULT_SHARE
        defaultSpentSharingConfigShouldBeFound("share.equals=" + DEFAULT_SHARE);

        // Get all the spentSharingConfigList where share equals to UPDATED_SHARE
        defaultSpentSharingConfigShouldNotBeFound("share.equals=" + UPDATED_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingConfigsByShareIsInShouldWork() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        // Get all the spentSharingConfigList where share in DEFAULT_SHARE or UPDATED_SHARE
        defaultSpentSharingConfigShouldBeFound("share.in=" + DEFAULT_SHARE + "," + UPDATED_SHARE);

        // Get all the spentSharingConfigList where share equals to UPDATED_SHARE
        defaultSpentSharingConfigShouldNotBeFound("share.in=" + UPDATED_SHARE);
    }

    @Test
    @Transactional
    public void getAllSpentSharingConfigsByShareIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        // Get all the spentSharingConfigList where share is not null
        defaultSpentSharingConfigShouldBeFound("share.specified=true");

        // Get all the spentSharingConfigList where share is null
        defaultSpentSharingConfigShouldNotBeFound("share.specified=false");
    }

    @Test
    @Transactional
    public void getAllSpentSharingConfigsBySharingUserIsEqualToSomething() throws Exception {
        // Initialize the database
        Profil sharingUser = ProfilResourceIT.createEntity(em);
        em.persist(sharingUser);
        em.flush();
        spentSharingConfig.setSharingUser(sharingUser);
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);
        Long sharingUserId = sharingUser.getId();

        // Get all the spentSharingConfigList where sharingUser equals to sharingUserId
        defaultSpentSharingConfigShouldBeFound("sharingUserId.equals=" + sharingUserId);

        // Get all the spentSharingConfigList where sharingUser equals to sharingUserId + 1
        defaultSpentSharingConfigShouldNotBeFound("sharingUserId.equals=" + (sharingUserId + 1));
    }


    @Test
    @Transactional
    public void getAllSpentSharingConfigsByDefaultSpentConfigIsEqualToSomething() throws Exception {
        // Initialize the database
        SpentConfig defaultSpentConfig = SpentConfigResourceIT.createEntity(em);
        em.persist(defaultSpentConfig);
        em.flush();
        spentSharingConfig.setDefaultSpentConfig(defaultSpentConfig);
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);
        Long defaultSpentConfigId = defaultSpentConfig.getId();

        // Get all the spentSharingConfigList where defaultSpentConfig equals to defaultSpentConfigId
        defaultSpentSharingConfigShouldBeFound("defaultSpentConfigId.equals=" + defaultSpentConfigId);

        // Get all the spentSharingConfigList where defaultSpentConfig equals to defaultSpentConfigId + 1
        defaultSpentSharingConfigShouldNotBeFound("defaultSpentConfigId.equals=" + (defaultSpentConfigId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSpentSharingConfigShouldBeFound(String filter) throws Exception {
        restSpentSharingConfigMockMvc.perform(get("/api/spent-sharing-configs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(spentSharingConfig.getId().intValue())))
            .andExpect(jsonPath("$.[*].share").value(hasItem(DEFAULT_SHARE.doubleValue())));

        // Check, that the count call also returns 1
        restSpentSharingConfigMockMvc.perform(get("/api/spent-sharing-configs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSpentSharingConfigShouldNotBeFound(String filter) throws Exception {
        restSpentSharingConfigMockMvc.perform(get("/api/spent-sharing-configs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSpentSharingConfigMockMvc.perform(get("/api/spent-sharing-configs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingSpentSharingConfig() throws Exception {
        // Get the spentSharingConfig
        restSpentSharingConfigMockMvc.perform(get("/api/spent-sharing-configs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSpentSharingConfig() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        int databaseSizeBeforeUpdate = spentSharingConfigRepository.findAll().size();

        // Update the spentSharingConfig
        SpentSharingConfig updatedSpentSharingConfig = spentSharingConfigRepository.findById(spentSharingConfig.getId()).get();
        // Disconnect from session so that the updates on updatedSpentSharingConfig are not directly saved in db
        em.detach(updatedSpentSharingConfig);
        updatedSpentSharingConfig
            .share(UPDATED_SHARE);
        SpentSharingConfigDTO spentSharingConfigDTO = spentSharingConfigMapper.toDto(updatedSpentSharingConfig);

        restSpentSharingConfigMockMvc.perform(put("/api/spent-sharing-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingConfigDTO)))
            .andExpect(status().isOk());

        // Validate the SpentSharingConfig in the database
        List<SpentSharingConfig> spentSharingConfigList = spentSharingConfigRepository.findAll();
        assertThat(spentSharingConfigList).hasSize(databaseSizeBeforeUpdate);
        SpentSharingConfig testSpentSharingConfig = spentSharingConfigList.get(spentSharingConfigList.size() - 1);
        assertThat(testSpentSharingConfig.getShare()).isEqualTo(UPDATED_SHARE);
    }

    @Test
    @Transactional
    public void updateNonExistingSpentSharingConfig() throws Exception {
        int databaseSizeBeforeUpdate = spentSharingConfigRepository.findAll().size();

        // Create the SpentSharingConfig
        SpentSharingConfigDTO spentSharingConfigDTO = spentSharingConfigMapper.toDto(spentSharingConfig);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSpentSharingConfigMockMvc.perform(put("/api/spent-sharing-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentSharingConfigDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SpentSharingConfig in the database
        List<SpentSharingConfig> spentSharingConfigList = spentSharingConfigRepository.findAll();
        assertThat(spentSharingConfigList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSpentSharingConfig() throws Exception {
        // Initialize the database
        spentSharingConfigRepository.saveAndFlush(spentSharingConfig);

        int databaseSizeBeforeDelete = spentSharingConfigRepository.findAll().size();

        // Delete the spentSharingConfig
        restSpentSharingConfigMockMvc.perform(delete("/api/spent-sharing-configs/{id}", spentSharingConfig.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SpentSharingConfig> spentSharingConfigList = spentSharingConfigRepository.findAll();
        assertThat(spentSharingConfigList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpentSharingConfig.class);
        SpentSharingConfig spentSharingConfig1 = new SpentSharingConfig();
        spentSharingConfig1.setId(1L);
        SpentSharingConfig spentSharingConfig2 = new SpentSharingConfig();
        spentSharingConfig2.setId(spentSharingConfig1.getId());
        assertThat(spentSharingConfig1).isEqualTo(spentSharingConfig2);
        spentSharingConfig2.setId(2L);
        assertThat(spentSharingConfig1).isNotEqualTo(spentSharingConfig2);
        spentSharingConfig1.setId(null);
        assertThat(spentSharingConfig1).isNotEqualTo(spentSharingConfig2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpentSharingConfigDTO.class);
        SpentSharingConfigDTO spentSharingConfigDTO1 = new SpentSharingConfigDTO();
        spentSharingConfigDTO1.setId(1L);
        SpentSharingConfigDTO spentSharingConfigDTO2 = new SpentSharingConfigDTO();
        assertThat(spentSharingConfigDTO1).isNotEqualTo(spentSharingConfigDTO2);
        spentSharingConfigDTO2.setId(spentSharingConfigDTO1.getId());
        assertThat(spentSharingConfigDTO1).isEqualTo(spentSharingConfigDTO2);
        spentSharingConfigDTO2.setId(2L);
        assertThat(spentSharingConfigDTO1).isNotEqualTo(spentSharingConfigDTO2);
        spentSharingConfigDTO1.setId(null);
        assertThat(spentSharingConfigDTO1).isNotEqualTo(spentSharingConfigDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(spentSharingConfigMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(spentSharingConfigMapper.fromId(null)).isNull();
    }
}
