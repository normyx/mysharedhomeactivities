package com.mgoulene.msha.web.rest;

import com.mgoulene.msha.MySharedHomeActivitiesApp;
import com.mgoulene.msha.domain.Spent;
import com.mgoulene.msha.domain.Profil;
import com.mgoulene.msha.domain.Wallet;
import com.mgoulene.msha.domain.SpentRecurrence;
import com.mgoulene.msha.domain.SpentSharing;
import com.mgoulene.msha.repository.SpentRepository;
import com.mgoulene.msha.service.SpentService;
import com.mgoulene.msha.service.dto.SpentDTO;
import com.mgoulene.msha.service.mapper.SpentMapper;
import com.mgoulene.msha.web.rest.errors.ExceptionTranslator;
import com.mgoulene.msha.service.dto.SpentCriteria;
import com.mgoulene.msha.service.SpentQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.mgoulene.msha.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link SpentResource} REST controller.
 */
@SpringBootTest(classes = MySharedHomeActivitiesApp.class)
public class SpentResourceIT {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final Float DEFAULT_AMOUNT = 1F;
    private static final Float UPDATED_AMOUNT = 2F;

    private static final LocalDate DEFAULT_SPENT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_SPENT_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_VALIDATED = false;
    private static final Boolean UPDATED_VALIDATED = true;

    @Autowired
    private SpentRepository spentRepository;

    @Autowired
    private SpentMapper spentMapper;

    @Autowired
    private SpentService spentService;

    @Autowired
    private SpentQueryService spentQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSpentMockMvc;

    private Spent spent;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SpentResource spentResource = new SpentResource(spentService, spentQueryService);
        this.restSpentMockMvc = MockMvcBuilders.standaloneSetup(spentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Spent createEntity(EntityManager em) {
        Spent spent = new Spent()
            .label(DEFAULT_LABEL)
            .amount(DEFAULT_AMOUNT)
            .spentDate(DEFAULT_SPENT_DATE)
            .validated(DEFAULT_VALIDATED);
        return spent;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Spent createUpdatedEntity(EntityManager em) {
        Spent spent = new Spent()
            .label(UPDATED_LABEL)
            .amount(UPDATED_AMOUNT)
            .spentDate(UPDATED_SPENT_DATE)
            .validated(UPDATED_VALIDATED);
        return spent;
    }

    @BeforeEach
    public void initTest() {
        spent = createEntity(em);
    }

    @Test
    @Transactional
    public void createSpent() throws Exception {
        int databaseSizeBeforeCreate = spentRepository.findAll().size();

        // Create the Spent
        SpentDTO spentDTO = spentMapper.toDto(spent);
        restSpentMockMvc.perform(post("/api/spents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentDTO)))
            .andExpect(status().isCreated());

        // Validate the Spent in the database
        List<Spent> spentList = spentRepository.findAll();
        assertThat(spentList).hasSize(databaseSizeBeforeCreate + 1);
        Spent testSpent = spentList.get(spentList.size() - 1);
        assertThat(testSpent.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testSpent.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testSpent.getSpentDate()).isEqualTo(DEFAULT_SPENT_DATE);
        assertThat(testSpent.isValidated()).isEqualTo(DEFAULT_VALIDATED);
    }

    @Test
    @Transactional
    public void createSpentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = spentRepository.findAll().size();

        // Create the Spent with an existing ID
        spent.setId(1L);
        SpentDTO spentDTO = spentMapper.toDto(spent);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSpentMockMvc.perform(post("/api/spents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Spent in the database
        List<Spent> spentList = spentRepository.findAll();
        assertThat(spentList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = spentRepository.findAll().size();
        // set the field null
        spent.setLabel(null);

        // Create the Spent, which fails.
        SpentDTO spentDTO = spentMapper.toDto(spent);

        restSpentMockMvc.perform(post("/api/spents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentDTO)))
            .andExpect(status().isBadRequest());

        List<Spent> spentList = spentRepository.findAll();
        assertThat(spentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAmountIsRequired() throws Exception {
        int databaseSizeBeforeTest = spentRepository.findAll().size();
        // set the field null
        spent.setAmount(null);

        // Create the Spent, which fails.
        SpentDTO spentDTO = spentMapper.toDto(spent);

        restSpentMockMvc.perform(post("/api/spents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentDTO)))
            .andExpect(status().isBadRequest());

        List<Spent> spentList = spentRepository.findAll();
        assertThat(spentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSpentDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = spentRepository.findAll().size();
        // set the field null
        spent.setSpentDate(null);

        // Create the Spent, which fails.
        SpentDTO spentDTO = spentMapper.toDto(spent);

        restSpentMockMvc.perform(post("/api/spents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentDTO)))
            .andExpect(status().isBadRequest());

        List<Spent> spentList = spentRepository.findAll();
        assertThat(spentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValidatedIsRequired() throws Exception {
        int databaseSizeBeforeTest = spentRepository.findAll().size();
        // set the field null
        spent.setValidated(null);

        // Create the Spent, which fails.
        SpentDTO spentDTO = spentMapper.toDto(spent);

        restSpentMockMvc.perform(post("/api/spents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentDTO)))
            .andExpect(status().isBadRequest());

        List<Spent> spentList = spentRepository.findAll();
        assertThat(spentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSpents() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList
        restSpentMockMvc.perform(get("/api/spents?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(spent.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL.toString())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].spentDate").value(hasItem(DEFAULT_SPENT_DATE.toString())))
            .andExpect(jsonPath("$.[*].validated").value(hasItem(DEFAULT_VALIDATED.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getSpent() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get the spent
        restSpentMockMvc.perform(get("/api/spents/{id}", spent.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(spent.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL.toString()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.spentDate").value(DEFAULT_SPENT_DATE.toString()))
            .andExpect(jsonPath("$.validated").value(DEFAULT_VALIDATED.booleanValue()));
    }

    @Test
    @Transactional
    public void getAllSpentsByLabelIsEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where label equals to DEFAULT_LABEL
        defaultSpentShouldBeFound("label.equals=" + DEFAULT_LABEL);

        // Get all the spentList where label equals to UPDATED_LABEL
        defaultSpentShouldNotBeFound("label.equals=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllSpentsByLabelIsInShouldWork() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where label in DEFAULT_LABEL or UPDATED_LABEL
        defaultSpentShouldBeFound("label.in=" + DEFAULT_LABEL + "," + UPDATED_LABEL);

        // Get all the spentList where label equals to UPDATED_LABEL
        defaultSpentShouldNotBeFound("label.in=" + UPDATED_LABEL);
    }

    @Test
    @Transactional
    public void getAllSpentsByLabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where label is not null
        defaultSpentShouldBeFound("label.specified=true");

        // Get all the spentList where label is null
        defaultSpentShouldNotBeFound("label.specified=false");
    }

    @Test
    @Transactional
    public void getAllSpentsByAmountIsEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where amount equals to DEFAULT_AMOUNT
        defaultSpentShouldBeFound("amount.equals=" + DEFAULT_AMOUNT);

        // Get all the spentList where amount equals to UPDATED_AMOUNT
        defaultSpentShouldNotBeFound("amount.equals=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllSpentsByAmountIsInShouldWork() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where amount in DEFAULT_AMOUNT or UPDATED_AMOUNT
        defaultSpentShouldBeFound("amount.in=" + DEFAULT_AMOUNT + "," + UPDATED_AMOUNT);

        // Get all the spentList where amount equals to UPDATED_AMOUNT
        defaultSpentShouldNotBeFound("amount.in=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllSpentsByAmountIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where amount is not null
        defaultSpentShouldBeFound("amount.specified=true");

        // Get all the spentList where amount is null
        defaultSpentShouldNotBeFound("amount.specified=false");
    }

    @Test
    @Transactional
    public void getAllSpentsBySpentDateIsEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where spentDate equals to DEFAULT_SPENT_DATE
        defaultSpentShouldBeFound("spentDate.equals=" + DEFAULT_SPENT_DATE);

        // Get all the spentList where spentDate equals to UPDATED_SPENT_DATE
        defaultSpentShouldNotBeFound("spentDate.equals=" + UPDATED_SPENT_DATE);
    }

    @Test
    @Transactional
    public void getAllSpentsBySpentDateIsInShouldWork() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where spentDate in DEFAULT_SPENT_DATE or UPDATED_SPENT_DATE
        defaultSpentShouldBeFound("spentDate.in=" + DEFAULT_SPENT_DATE + "," + UPDATED_SPENT_DATE);

        // Get all the spentList where spentDate equals to UPDATED_SPENT_DATE
        defaultSpentShouldNotBeFound("spentDate.in=" + UPDATED_SPENT_DATE);
    }

    @Test
    @Transactional
    public void getAllSpentsBySpentDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where spentDate is not null
        defaultSpentShouldBeFound("spentDate.specified=true");

        // Get all the spentList where spentDate is null
        defaultSpentShouldNotBeFound("spentDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllSpentsBySpentDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where spentDate greater than or equals to DEFAULT_SPENT_DATE
        defaultSpentShouldBeFound("spentDate.greaterOrEqualThan=" + DEFAULT_SPENT_DATE);

        // Get all the spentList where spentDate greater than or equals to UPDATED_SPENT_DATE
        defaultSpentShouldNotBeFound("spentDate.greaterOrEqualThan=" + UPDATED_SPENT_DATE);
    }

    @Test
    @Transactional
    public void getAllSpentsBySpentDateIsLessThanSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where spentDate less than or equals to DEFAULT_SPENT_DATE
        defaultSpentShouldNotBeFound("spentDate.lessThan=" + DEFAULT_SPENT_DATE);

        // Get all the spentList where spentDate less than or equals to UPDATED_SPENT_DATE
        defaultSpentShouldBeFound("spentDate.lessThan=" + UPDATED_SPENT_DATE);
    }


    @Test
    @Transactional
    public void getAllSpentsByValidatedIsEqualToSomething() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where validated equals to DEFAULT_VALIDATED
        defaultSpentShouldBeFound("validated.equals=" + DEFAULT_VALIDATED);

        // Get all the spentList where validated equals to UPDATED_VALIDATED
        defaultSpentShouldNotBeFound("validated.equals=" + UPDATED_VALIDATED);
    }

    @Test
    @Transactional
    public void getAllSpentsByValidatedIsInShouldWork() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where validated in DEFAULT_VALIDATED or UPDATED_VALIDATED
        defaultSpentShouldBeFound("validated.in=" + DEFAULT_VALIDATED + "," + UPDATED_VALIDATED);

        // Get all the spentList where validated equals to UPDATED_VALIDATED
        defaultSpentShouldNotBeFound("validated.in=" + UPDATED_VALIDATED);
    }

    @Test
    @Transactional
    public void getAllSpentsByValidatedIsNullOrNotNull() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        // Get all the spentList where validated is not null
        defaultSpentShouldBeFound("validated.specified=true");

        // Get all the spentList where validated is null
        defaultSpentShouldNotBeFound("validated.specified=false");
    }

    @Test
    @Transactional
    public void getAllSpentsBySpenderIsEqualToSomething() throws Exception {
        // Initialize the database
        Profil spender = ProfilResourceIT.createEntity(em);
        em.persist(spender);
        em.flush();
        spent.setSpender(spender);
        spentRepository.saveAndFlush(spent);
        Long spenderId = spender.getId();

        // Get all the spentList where spender equals to spenderId
        defaultSpentShouldBeFound("spenderId.equals=" + spenderId);

        // Get all the spentList where spender equals to spenderId + 1
        defaultSpentShouldNotBeFound("spenderId.equals=" + (spenderId + 1));
    }


    @Test
    @Transactional
    public void getAllSpentsByWalletIsEqualToSomething() throws Exception {
        // Initialize the database
        Wallet wallet = WalletResourceIT.createEntity(em);
        em.persist(wallet);
        em.flush();
        spent.setWallet(wallet);
        spentRepository.saveAndFlush(spent);
        Long walletId = wallet.getId();

        // Get all the spentList where wallet equals to walletId
        defaultSpentShouldBeFound("walletId.equals=" + walletId);

        // Get all the spentList where wallet equals to walletId + 1
        defaultSpentShouldNotBeFound("walletId.equals=" + (walletId + 1));
    }


    @Test
    @Transactional
    public void getAllSpentsByRecurrenceIsEqualToSomething() throws Exception {
        // Initialize the database
        SpentRecurrence recurrence = SpentRecurrenceResourceIT.createEntity(em);
        em.persist(recurrence);
        em.flush();
        spent.setRecurrence(recurrence);
        recurrence.setSpent(spent);
        spentRepository.saveAndFlush(spent);
        Long recurrenceId = recurrence.getId();

        // Get all the spentList where recurrence equals to recurrenceId
        defaultSpentShouldBeFound("recurrenceId.equals=" + recurrenceId);

        // Get all the spentList where recurrence equals to recurrenceId + 1
        defaultSpentShouldNotBeFound("recurrenceId.equals=" + (recurrenceId + 1));
    }


    @Test
    @Transactional
    public void getAllSpentsBySharingIsEqualToSomething() throws Exception {
        // Initialize the database
        SpentSharing sharing = SpentSharingResourceIT.createEntity(em);
        em.persist(sharing);
        em.flush();
        spent.addSharing(sharing);
        spentRepository.saveAndFlush(spent);
        Long sharingId = sharing.getId();

        // Get all the spentList where sharing equals to sharingId
        defaultSpentShouldBeFound("sharingId.equals=" + sharingId);

        // Get all the spentList where sharing equals to sharingId + 1
        defaultSpentShouldNotBeFound("sharingId.equals=" + (sharingId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSpentShouldBeFound(String filter) throws Exception {
        restSpentMockMvc.perform(get("/api/spents?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(spent.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].spentDate").value(hasItem(DEFAULT_SPENT_DATE.toString())))
            .andExpect(jsonPath("$.[*].validated").value(hasItem(DEFAULT_VALIDATED.booleanValue())));

        // Check, that the count call also returns 1
        restSpentMockMvc.perform(get("/api/spents/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSpentShouldNotBeFound(String filter) throws Exception {
        restSpentMockMvc.perform(get("/api/spents?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSpentMockMvc.perform(get("/api/spents/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingSpent() throws Exception {
        // Get the spent
        restSpentMockMvc.perform(get("/api/spents/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSpent() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        int databaseSizeBeforeUpdate = spentRepository.findAll().size();

        // Update the spent
        Spent updatedSpent = spentRepository.findById(spent.getId()).get();
        // Disconnect from session so that the updates on updatedSpent are not directly saved in db
        em.detach(updatedSpent);
        updatedSpent
            .label(UPDATED_LABEL)
            .amount(UPDATED_AMOUNT)
            .spentDate(UPDATED_SPENT_DATE)
            .validated(UPDATED_VALIDATED);
        SpentDTO spentDTO = spentMapper.toDto(updatedSpent);

        restSpentMockMvc.perform(put("/api/spents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentDTO)))
            .andExpect(status().isOk());

        // Validate the Spent in the database
        List<Spent> spentList = spentRepository.findAll();
        assertThat(spentList).hasSize(databaseSizeBeforeUpdate);
        Spent testSpent = spentList.get(spentList.size() - 1);
        assertThat(testSpent.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testSpent.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testSpent.getSpentDate()).isEqualTo(UPDATED_SPENT_DATE);
        assertThat(testSpent.isValidated()).isEqualTo(UPDATED_VALIDATED);
    }

    @Test
    @Transactional
    public void updateNonExistingSpent() throws Exception {
        int databaseSizeBeforeUpdate = spentRepository.findAll().size();

        // Create the Spent
        SpentDTO spentDTO = spentMapper.toDto(spent);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSpentMockMvc.perform(put("/api/spents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(spentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Spent in the database
        List<Spent> spentList = spentRepository.findAll();
        assertThat(spentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSpent() throws Exception {
        // Initialize the database
        spentRepository.saveAndFlush(spent);

        int databaseSizeBeforeDelete = spentRepository.findAll().size();

        // Delete the spent
        restSpentMockMvc.perform(delete("/api/spents/{id}", spent.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Spent> spentList = spentRepository.findAll();
        assertThat(spentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Spent.class);
        Spent spent1 = new Spent();
        spent1.setId(1L);
        Spent spent2 = new Spent();
        spent2.setId(spent1.getId());
        assertThat(spent1).isEqualTo(spent2);
        spent2.setId(2L);
        assertThat(spent1).isNotEqualTo(spent2);
        spent1.setId(null);
        assertThat(spent1).isNotEqualTo(spent2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpentDTO.class);
        SpentDTO spentDTO1 = new SpentDTO();
        spentDTO1.setId(1L);
        SpentDTO spentDTO2 = new SpentDTO();
        assertThat(spentDTO1).isNotEqualTo(spentDTO2);
        spentDTO2.setId(spentDTO1.getId());
        assertThat(spentDTO1).isEqualTo(spentDTO2);
        spentDTO2.setId(2L);
        assertThat(spentDTO1).isNotEqualTo(spentDTO2);
        spentDTO1.setId(null);
        assertThat(spentDTO1).isNotEqualTo(spentDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(spentMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(spentMapper.fromId(null)).isNull();
    }
}
