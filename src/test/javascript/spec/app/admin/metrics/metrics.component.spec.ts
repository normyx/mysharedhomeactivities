import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { of, throwError } from 'rxjs';

import { MySharedHomeActivitiesTestModule } from '../../../test.module';
import { MshaMetricsMonitoringComponent } from 'app/admin/metrics/metrics.component';
import { MshaMetricsService } from 'app/admin/metrics/metrics.service';

describe('Component Tests', () => {
  describe('MshaMetricsMonitoringComponent', () => {
    let comp: MshaMetricsMonitoringComponent;
    let fixture: ComponentFixture<MshaMetricsMonitoringComponent>;
    let service: MshaMetricsService;

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [MySharedHomeActivitiesTestModule],
        declarations: [MshaMetricsMonitoringComponent]
      })
        .overrideTemplate(MshaMetricsMonitoringComponent, '')
        .compileComponents();
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(MshaMetricsMonitoringComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MshaMetricsService);
    });

    describe('refresh', () => {
      it('should call refresh on init', () => {
        // GIVEN
        const response = {
          timers: {
            service: 'test',
            unrelatedKey: 'test'
          },
          gauges: {
            'jcache.statistics': {
              value: 2
            },
            unrelatedKey: 'test'
          }
        };
        spyOn(service, 'getMetrics').and.returnValue(of(response));

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(service.getMetrics).toHaveBeenCalled();
      });
    });
  });
});
