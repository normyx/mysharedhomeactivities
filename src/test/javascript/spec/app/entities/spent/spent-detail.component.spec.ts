/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MySharedHomeActivitiesTestModule } from '../../../test.module';
import { SpentDetailComponent } from 'app/entities/spent/spent-detail.component';
import { Spent } from 'app/shared/model/spent.model';

describe('Component Tests', () => {
  describe('Spent Management Detail Component', () => {
    let comp: SpentDetailComponent;
    let fixture: ComponentFixture<SpentDetailComponent>;
    const route = ({ data: of({ spent: new Spent(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MySharedHomeActivitiesTestModule],
        declarations: [SpentDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(SpentDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SpentDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.spent).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
