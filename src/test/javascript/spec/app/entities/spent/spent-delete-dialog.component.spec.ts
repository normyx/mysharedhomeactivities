/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { MySharedHomeActivitiesTestModule } from '../../../test.module';
import { SpentDeleteDialogComponent } from 'app/entities/spent/spent-delete-dialog.component';
import { SpentService } from 'app/entities/spent/spent.service';

describe('Component Tests', () => {
  describe('Spent Management Delete Component', () => {
    let comp: SpentDeleteDialogComponent;
    let fixture: ComponentFixture<SpentDeleteDialogComponent>;
    let service: SpentService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MySharedHomeActivitiesTestModule],
        declarations: [SpentDeleteDialogComponent]
      })
        .overrideTemplate(SpentDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SpentDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SpentService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
