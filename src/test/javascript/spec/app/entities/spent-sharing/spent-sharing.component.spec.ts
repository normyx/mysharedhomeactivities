/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MySharedHomeActivitiesTestModule } from '../../../test.module';
import { SpentSharingComponent } from 'app/entities/spent-sharing/spent-sharing.component';
import { SpentSharingService } from 'app/entities/spent-sharing/spent-sharing.service';
import { SpentSharing } from 'app/shared/model/spent-sharing.model';

describe('Component Tests', () => {
  describe('SpentSharing Management Component', () => {
    let comp: SpentSharingComponent;
    let fixture: ComponentFixture<SpentSharingComponent>;
    let service: SpentSharingService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MySharedHomeActivitiesTestModule],
        declarations: [SpentSharingComponent],
        providers: []
      })
        .overrideTemplate(SpentSharingComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SpentSharingComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SpentSharingService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new SpentSharing(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.spentSharings[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
