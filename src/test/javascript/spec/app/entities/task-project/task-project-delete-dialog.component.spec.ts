/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { MySharedHomeActivitiesTestModule } from '../../../test.module';
import { TaskProjectDeleteDialogComponent } from 'app/entities/task-project/task-project-delete-dialog.component';
import { TaskProjectService } from 'app/entities/task-project/task-project.service';

describe('Component Tests', () => {
  describe('TaskProject Management Delete Component', () => {
    let comp: TaskProjectDeleteDialogComponent;
    let fixture: ComponentFixture<TaskProjectDeleteDialogComponent>;
    let service: TaskProjectService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MySharedHomeActivitiesTestModule],
        declarations: [TaskProjectDeleteDialogComponent]
      })
        .overrideTemplate(TaskProjectDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TaskProjectDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TaskProjectService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
