/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MySharedHomeActivitiesTestModule } from '../../../test.module';
import { SpentSharingConfigDetailComponent } from 'app/entities/spent-sharing-config/spent-sharing-config-detail.component';
import { SpentSharingConfig } from 'app/shared/model/spent-sharing-config.model';

describe('Component Tests', () => {
  describe('SpentSharingConfig Management Detail Component', () => {
    let comp: SpentSharingConfigDetailComponent;
    let fixture: ComponentFixture<SpentSharingConfigDetailComponent>;
    const route = ({ data: of({ spentSharingConfig: new SpentSharingConfig(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MySharedHomeActivitiesTestModule],
        declarations: [SpentSharingConfigDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(SpentSharingConfigDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SpentSharingConfigDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.spentSharingConfig).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
