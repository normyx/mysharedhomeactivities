/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MySharedHomeActivitiesTestModule } from '../../../test.module';
import { SpentSharingConfigComponent } from 'app/entities/spent-sharing-config/spent-sharing-config.component';
import { SpentSharingConfigService } from 'app/entities/spent-sharing-config/spent-sharing-config.service';
import { SpentSharingConfig } from 'app/shared/model/spent-sharing-config.model';

describe('Component Tests', () => {
  describe('SpentSharingConfig Management Component', () => {
    let comp: SpentSharingConfigComponent;
    let fixture: ComponentFixture<SpentSharingConfigComponent>;
    let service: SpentSharingConfigService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MySharedHomeActivitiesTestModule],
        declarations: [SpentSharingConfigComponent],
        providers: []
      })
        .overrideTemplate(SpentSharingConfigComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SpentSharingConfigComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SpentSharingConfigService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new SpentSharingConfig(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.spentSharingConfigs[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
