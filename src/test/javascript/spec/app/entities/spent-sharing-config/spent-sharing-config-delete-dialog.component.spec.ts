/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { MySharedHomeActivitiesTestModule } from '../../../test.module';
import { SpentSharingConfigDeleteDialogComponent } from 'app/entities/spent-sharing-config/spent-sharing-config-delete-dialog.component';
import { SpentSharingConfigService } from 'app/entities/spent-sharing-config/spent-sharing-config.service';

describe('Component Tests', () => {
  describe('SpentSharingConfig Management Delete Component', () => {
    let comp: SpentSharingConfigDeleteDialogComponent;
    let fixture: ComponentFixture<SpentSharingConfigDeleteDialogComponent>;
    let service: SpentSharingConfigService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MySharedHomeActivitiesTestModule],
        declarations: [SpentSharingConfigDeleteDialogComponent]
      })
        .overrideTemplate(SpentSharingConfigDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SpentSharingConfigDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SpentSharingConfigService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
