/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MySharedHomeActivitiesTestModule } from '../../../test.module';
import { SpentRecurrenceComponent } from 'app/entities/spent-recurrence/spent-recurrence.component';
import { SpentRecurrenceService } from 'app/entities/spent-recurrence/spent-recurrence.service';
import { SpentRecurrence } from 'app/shared/model/spent-recurrence.model';

describe('Component Tests', () => {
  describe('SpentRecurrence Management Component', () => {
    let comp: SpentRecurrenceComponent;
    let fixture: ComponentFixture<SpentRecurrenceComponent>;
    let service: SpentRecurrenceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MySharedHomeActivitiesTestModule],
        declarations: [SpentRecurrenceComponent],
        providers: []
      })
        .overrideTemplate(SpentRecurrenceComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SpentRecurrenceComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SpentRecurrenceService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new SpentRecurrence(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.spentRecurrences[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
