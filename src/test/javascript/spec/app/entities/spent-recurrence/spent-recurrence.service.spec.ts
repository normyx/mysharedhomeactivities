/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SpentRecurrenceService } from 'app/entities/spent-recurrence/spent-recurrence.service';
import { ISpentRecurrence, SpentRecurrence } from 'app/shared/model/spent-recurrence.model';

describe('Service Tests', () => {
  describe('SpentRecurrence Service', () => {
    let injector: TestBed;
    let service: SpentRecurrenceService;
    let httpMock: HttpTestingController;
    let elemDefault: ISpentRecurrence;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(SpentRecurrenceService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new SpentRecurrence(0, 0, currentDate, currentDate, 0);
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign(
          {
            recurrenceFrom: currentDate.format(DATE_FORMAT),
            recurrenceTo: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a SpentRecurrence', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            recurrenceFrom: currentDate.format(DATE_FORMAT),
            recurrenceTo: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            recurrenceFrom: currentDate,
            recurrenceTo: currentDate
          },
          returnedFromService
        );
        service
          .create(new SpentRecurrence(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a SpentRecurrence', async () => {
        const returnedFromService = Object.assign(
          {
            amount: 1,
            recurrenceFrom: currentDate.format(DATE_FORMAT),
            recurrenceTo: currentDate.format(DATE_FORMAT),
            dayInMonth: 1
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            recurrenceFrom: currentDate,
            recurrenceTo: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of SpentRecurrence', async () => {
        const returnedFromService = Object.assign(
          {
            amount: 1,
            recurrenceFrom: currentDate.format(DATE_FORMAT),
            recurrenceTo: currentDate.format(DATE_FORMAT),
            dayInMonth: 1
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            recurrenceFrom: currentDate,
            recurrenceTo: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a SpentRecurrence', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
