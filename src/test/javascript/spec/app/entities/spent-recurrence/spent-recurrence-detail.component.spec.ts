/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MySharedHomeActivitiesTestModule } from '../../../test.module';
import { SpentRecurrenceDetailComponent } from 'app/entities/spent-recurrence/spent-recurrence-detail.component';
import { SpentRecurrence } from 'app/shared/model/spent-recurrence.model';

describe('Component Tests', () => {
  describe('SpentRecurrence Management Detail Component', () => {
    let comp: SpentRecurrenceDetailComponent;
    let fixture: ComponentFixture<SpentRecurrenceDetailComponent>;
    const route = ({ data: of({ spentRecurrence: new SpentRecurrence(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MySharedHomeActivitiesTestModule],
        declarations: [SpentRecurrenceDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(SpentRecurrenceDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SpentRecurrenceDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.spentRecurrence).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
