/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { MySharedHomeActivitiesTestModule } from '../../../test.module';
import { SpentRecurrenceUpdateComponent } from 'app/entities/spent-recurrence/spent-recurrence-update.component';
import { SpentRecurrenceService } from 'app/entities/spent-recurrence/spent-recurrence.service';
import { SpentRecurrence } from 'app/shared/model/spent-recurrence.model';

describe('Component Tests', () => {
  describe('SpentRecurrence Management Update Component', () => {
    let comp: SpentRecurrenceUpdateComponent;
    let fixture: ComponentFixture<SpentRecurrenceUpdateComponent>;
    let service: SpentRecurrenceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MySharedHomeActivitiesTestModule],
        declarations: [SpentRecurrenceUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(SpentRecurrenceUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SpentRecurrenceUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SpentRecurrenceService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new SpentRecurrence(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new SpentRecurrence();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
