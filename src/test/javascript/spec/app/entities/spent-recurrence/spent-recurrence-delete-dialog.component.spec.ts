/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { MySharedHomeActivitiesTestModule } from '../../../test.module';
import { SpentRecurrenceDeleteDialogComponent } from 'app/entities/spent-recurrence/spent-recurrence-delete-dialog.component';
import { SpentRecurrenceService } from 'app/entities/spent-recurrence/spent-recurrence.service';

describe('Component Tests', () => {
  describe('SpentRecurrence Management Delete Component', () => {
    let comp: SpentRecurrenceDeleteDialogComponent;
    let fixture: ComponentFixture<SpentRecurrenceDeleteDialogComponent>;
    let service: SpentRecurrenceService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MySharedHomeActivitiesTestModule],
        declarations: [SpentRecurrenceDeleteDialogComponent]
      })
        .overrideTemplate(SpentRecurrenceDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SpentRecurrenceDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SpentRecurrenceService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
