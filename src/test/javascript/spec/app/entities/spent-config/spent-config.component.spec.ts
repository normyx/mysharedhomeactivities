/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MySharedHomeActivitiesTestModule } from '../../../test.module';
import { SpentConfigComponent } from 'app/entities/spent-config/spent-config.component';
import { SpentConfigService } from 'app/entities/spent-config/spent-config.service';
import { SpentConfig } from 'app/shared/model/spent-config.model';

describe('Component Tests', () => {
  describe('SpentConfig Management Component', () => {
    let comp: SpentConfigComponent;
    let fixture: ComponentFixture<SpentConfigComponent>;
    let service: SpentConfigService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MySharedHomeActivitiesTestModule],
        declarations: [SpentConfigComponent],
        providers: []
      })
        .overrideTemplate(SpentConfigComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SpentConfigComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SpentConfigService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new SpentConfig(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.spentConfigs[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
