/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { MySharedHomeActivitiesTestModule } from '../../../test.module';
import { SpentConfigDeleteDialogComponent } from 'app/entities/spent-config/spent-config-delete-dialog.component';
import { SpentConfigService } from 'app/entities/spent-config/spent-config.service';

describe('Component Tests', () => {
  describe('SpentConfig Management Delete Component', () => {
    let comp: SpentConfigDeleteDialogComponent;
    let fixture: ComponentFixture<SpentConfigDeleteDialogComponent>;
    let service: SpentConfigService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MySharedHomeActivitiesTestModule],
        declarations: [SpentConfigDeleteDialogComponent]
      })
        .overrideTemplate(SpentConfigDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SpentConfigDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SpentConfigService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
