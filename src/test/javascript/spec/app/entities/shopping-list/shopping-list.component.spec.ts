/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MySharedHomeActivitiesTestModule } from '../../../test.module';
import { ShoppingListComponent } from 'app/entities/shopping-list/shopping-list.component';
import { ShoppingListService } from 'app/entities/shopping-list/shopping-list.service';
import { ShoppingList } from 'app/shared/model/shopping-list.model';

describe('Component Tests', () => {
  describe('ShoppingList Management Component', () => {
    let comp: ShoppingListComponent;
    let fixture: ComponentFixture<ShoppingListComponent>;
    let service: ShoppingListService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MySharedHomeActivitiesTestModule],
        declarations: [ShoppingListComponent],
        providers: []
      })
        .overrideTemplate(ShoppingListComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ShoppingListComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ShoppingListService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ShoppingList(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.shoppingLists[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
