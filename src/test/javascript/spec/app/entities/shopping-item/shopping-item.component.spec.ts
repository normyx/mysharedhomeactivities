/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MySharedHomeActivitiesTestModule } from '../../../test.module';
import { ShoppingItemComponent } from 'app/entities/shopping-item/shopping-item.component';
import { ShoppingItemService } from 'app/entities/shopping-item/shopping-item.service';
import { ShoppingItem } from 'app/shared/model/shopping-item.model';

describe('Component Tests', () => {
  describe('ShoppingItem Management Component', () => {
    let comp: ShoppingItemComponent;
    let fixture: ComponentFixture<ShoppingItemComponent>;
    let service: ShoppingItemService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MySharedHomeActivitiesTestModule],
        declarations: [ShoppingItemComponent],
        providers: []
      })
        .overrideTemplate(ShoppingItemComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ShoppingItemComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ShoppingItemService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ShoppingItem(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.shoppingItems[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
