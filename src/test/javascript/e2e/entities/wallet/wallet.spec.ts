/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { WalletComponentsPage, WalletDeleteDialog, WalletUpdatePage } from './wallet.page-object';

const expect = chai.expect;

describe('Wallet e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let walletUpdatePage: WalletUpdatePage;
  let walletComponentsPage: WalletComponentsPage;
  let walletDeleteDialog: WalletDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Wallets', async () => {
    await navBarPage.goToEntity('wallet');
    walletComponentsPage = new WalletComponentsPage();
    await browser.wait(ec.visibilityOf(walletComponentsPage.title), 5000);
    expect(await walletComponentsPage.getTitle()).to.eq('mySharedHomeActivitiesApp.wallet.home.title');
  });

  it('should load create Wallet page', async () => {
    await walletComponentsPage.clickOnCreateButton();
    walletUpdatePage = new WalletUpdatePage();
    expect(await walletUpdatePage.getPageTitle()).to.eq('mySharedHomeActivitiesApp.wallet.home.createOrEditLabel');
    await walletUpdatePage.cancel();
  });

  it('should create and save Wallets', async () => {
    const nbButtonsBeforeCreate = await walletComponentsPage.countDeleteButtons();

    await walletComponentsPage.clickOnCreateButton();
    await promise.all([
      walletUpdatePage.setLabelInput('label'),
      walletUpdatePage.workspaceSelectLastOption()
      // walletUpdatePage.ownerSelectLastOption(),
    ]);
    expect(await walletUpdatePage.getLabelInput()).to.eq('label', 'Expected Label value to be equals to label');
    await walletUpdatePage.save();
    expect(await walletUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await walletComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Wallet', async () => {
    const nbButtonsBeforeDelete = await walletComponentsPage.countDeleteButtons();
    await walletComponentsPage.clickOnLastDeleteButton();

    walletDeleteDialog = new WalletDeleteDialog();
    expect(await walletDeleteDialog.getDialogTitle()).to.eq('mySharedHomeActivitiesApp.wallet.delete.question');
    await walletDeleteDialog.clickOnConfirmButton();

    expect(await walletComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
