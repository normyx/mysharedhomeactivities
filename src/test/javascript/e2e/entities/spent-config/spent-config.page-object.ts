import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class SpentConfigComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('msha-spent-config div table .btn-danger'));
  title = element.all(by.css('msha-spent-config div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class SpentConfigUpdatePage {
  pageTitle = element(by.id('msha-spent-config-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  labelInput = element(by.id('field_label'));
  walletSelect = element(by.id('field_wallet'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setLabelInput(label) {
    await this.labelInput.sendKeys(label);
  }

  async getLabelInput() {
    return await this.labelInput.getAttribute('value');
  }

  async walletSelectLastOption(timeout?: number) {
    await this.walletSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async walletSelectOption(option) {
    await this.walletSelect.sendKeys(option);
  }

  getWalletSelect(): ElementFinder {
    return this.walletSelect;
  }

  async getWalletSelectedOption() {
    return await this.walletSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class SpentConfigDeleteDialog {
  private dialogTitle = element(by.id('msha-delete-spentConfig-heading'));
  private confirmButton = element(by.id('msha-confirm-delete-spentConfig'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
