import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class TaskProjectComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('msha-task-project div table .btn-danger'));
  title = element.all(by.css('msha-task-project div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class TaskProjectUpdatePage {
  pageTitle = element(by.id('msha-task-project-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  labelInput = element(by.id('field_label'));
  workspaceSelect = element(by.id('field_workspace'));
  ownerSelect = element(by.id('field_owner'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setLabelInput(label) {
    await this.labelInput.sendKeys(label);
  }

  async getLabelInput() {
    return await this.labelInput.getAttribute('value');
  }

  async workspaceSelectLastOption(timeout?: number) {
    await this.workspaceSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async workspaceSelectOption(option) {
    await this.workspaceSelect.sendKeys(option);
  }

  getWorkspaceSelect(): ElementFinder {
    return this.workspaceSelect;
  }

  async getWorkspaceSelectedOption() {
    return await this.workspaceSelect.element(by.css('option:checked')).getText();
  }

  async ownerSelectLastOption(timeout?: number) {
    await this.ownerSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async ownerSelectOption(option) {
    await this.ownerSelect.sendKeys(option);
  }

  getOwnerSelect(): ElementFinder {
    return this.ownerSelect;
  }

  async getOwnerSelectedOption() {
    return await this.ownerSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class TaskProjectDeleteDialog {
  private dialogTitle = element(by.id('msha-delete-taskProject-heading'));
  private confirmButton = element(by.id('msha-confirm-delete-taskProject'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
