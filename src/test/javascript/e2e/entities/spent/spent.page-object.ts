import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class SpentComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('msha-spent div table .btn-danger'));
  title = element.all(by.css('msha-spent div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class SpentUpdatePage {
  pageTitle = element(by.id('msha-spent-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  labelInput = element(by.id('field_label'));
  amountInput = element(by.id('field_amount'));
  spentDateInput = element(by.id('field_spentDate'));
  validatedInput = element(by.id('field_validated'));
  spenderSelect = element(by.id('field_spender'));
  walletSelect = element(by.id('field_wallet'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setLabelInput(label) {
    await this.labelInput.sendKeys(label);
  }

  async getLabelInput() {
    return await this.labelInput.getAttribute('value');
  }

  async setAmountInput(amount) {
    await this.amountInput.sendKeys(amount);
  }

  async getAmountInput() {
    return await this.amountInput.getAttribute('value');
  }

  async setSpentDateInput(spentDate) {
    await this.spentDateInput.sendKeys(spentDate);
  }

  async getSpentDateInput() {
    return await this.spentDateInput.getAttribute('value');
  }

  getValidatedInput(timeout?: number) {
    return this.validatedInput;
  }

  async spenderSelectLastOption(timeout?: number) {
    await this.spenderSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async spenderSelectOption(option) {
    await this.spenderSelect.sendKeys(option);
  }

  getSpenderSelect(): ElementFinder {
    return this.spenderSelect;
  }

  async getSpenderSelectedOption() {
    return await this.spenderSelect.element(by.css('option:checked')).getText();
  }

  async walletSelectLastOption(timeout?: number) {
    await this.walletSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async walletSelectOption(option) {
    await this.walletSelect.sendKeys(option);
  }

  getWalletSelect(): ElementFinder {
    return this.walletSelect;
  }

  async getWalletSelectedOption() {
    return await this.walletSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class SpentDeleteDialog {
  private dialogTitle = element(by.id('msha-delete-spent-heading'));
  private confirmButton = element(by.id('msha-confirm-delete-spent'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
