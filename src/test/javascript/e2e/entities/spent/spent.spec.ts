/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { SpentComponentsPage, SpentDeleteDialog, SpentUpdatePage } from './spent.page-object';

const expect = chai.expect;

describe('Spent e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let spentUpdatePage: SpentUpdatePage;
  let spentComponentsPage: SpentComponentsPage;
  let spentDeleteDialog: SpentDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Spents', async () => {
    await navBarPage.goToEntity('spent');
    spentComponentsPage = new SpentComponentsPage();
    await browser.wait(ec.visibilityOf(spentComponentsPage.title), 5000);
    expect(await spentComponentsPage.getTitle()).to.eq('mySharedHomeActivitiesApp.spent.home.title');
  });

  it('should load create Spent page', async () => {
    await spentComponentsPage.clickOnCreateButton();
    spentUpdatePage = new SpentUpdatePage();
    expect(await spentUpdatePage.getPageTitle()).to.eq('mySharedHomeActivitiesApp.spent.home.createOrEditLabel');
    await spentUpdatePage.cancel();
  });

  it('should create and save Spents', async () => {
    const nbButtonsBeforeCreate = await spentComponentsPage.countDeleteButtons();

    await spentComponentsPage.clickOnCreateButton();
    await promise.all([
      spentUpdatePage.setLabelInput('label'),
      spentUpdatePage.setAmountInput('5'),
      spentUpdatePage.setSpentDateInput('2000-12-31'),
      spentUpdatePage.spenderSelectLastOption(),
      spentUpdatePage.walletSelectLastOption()
    ]);
    expect(await spentUpdatePage.getLabelInput()).to.eq('label', 'Expected Label value to be equals to label');
    expect(await spentUpdatePage.getAmountInput()).to.eq('5', 'Expected amount value to be equals to 5');
    expect(await spentUpdatePage.getSpentDateInput()).to.eq('2000-12-31', 'Expected spentDate value to be equals to 2000-12-31');
    const selectedValidated = spentUpdatePage.getValidatedInput();
    if (await selectedValidated.isSelected()) {
      await spentUpdatePage.getValidatedInput().click();
      expect(await spentUpdatePage.getValidatedInput().isSelected(), 'Expected validated not to be selected').to.be.false;
    } else {
      await spentUpdatePage.getValidatedInput().click();
      expect(await spentUpdatePage.getValidatedInput().isSelected(), 'Expected validated to be selected').to.be.true;
    }
    await spentUpdatePage.save();
    expect(await spentUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await spentComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Spent', async () => {
    const nbButtonsBeforeDelete = await spentComponentsPage.countDeleteButtons();
    await spentComponentsPage.clickOnLastDeleteButton();

    spentDeleteDialog = new SpentDeleteDialog();
    expect(await spentDeleteDialog.getDialogTitle()).to.eq('mySharedHomeActivitiesApp.spent.delete.question');
    await spentDeleteDialog.clickOnConfirmButton();

    expect(await spentComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
