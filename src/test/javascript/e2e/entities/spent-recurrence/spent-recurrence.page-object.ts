import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class SpentRecurrenceComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('msha-spent-recurrence div table .btn-danger'));
  title = element.all(by.css('msha-spent-recurrence div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class SpentRecurrenceUpdatePage {
  pageTitle = element(by.id('msha-spent-recurrence-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  amountInput = element(by.id('field_amount'));
  recurrenceFromInput = element(by.id('field_recurrenceFrom'));
  recurrenceToInput = element(by.id('field_recurrenceTo'));
  dayInMonthInput = element(by.id('field_dayInMonth'));
  spentSelect = element(by.id('field_spent'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setAmountInput(amount) {
    await this.amountInput.sendKeys(amount);
  }

  async getAmountInput() {
    return await this.amountInput.getAttribute('value');
  }

  async setRecurrenceFromInput(recurrenceFrom) {
    await this.recurrenceFromInput.sendKeys(recurrenceFrom);
  }

  async getRecurrenceFromInput() {
    return await this.recurrenceFromInput.getAttribute('value');
  }

  async setRecurrenceToInput(recurrenceTo) {
    await this.recurrenceToInput.sendKeys(recurrenceTo);
  }

  async getRecurrenceToInput() {
    return await this.recurrenceToInput.getAttribute('value');
  }

  async setDayInMonthInput(dayInMonth) {
    await this.dayInMonthInput.sendKeys(dayInMonth);
  }

  async getDayInMonthInput() {
    return await this.dayInMonthInput.getAttribute('value');
  }

  async spentSelectLastOption(timeout?: number) {
    await this.spentSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async spentSelectOption(option) {
    await this.spentSelect.sendKeys(option);
  }

  getSpentSelect(): ElementFinder {
    return this.spentSelect;
  }

  async getSpentSelectedOption() {
    return await this.spentSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class SpentRecurrenceDeleteDialog {
  private dialogTitle = element(by.id('msha-delete-spentRecurrence-heading'));
  private confirmButton = element(by.id('msha-confirm-delete-spentRecurrence'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
