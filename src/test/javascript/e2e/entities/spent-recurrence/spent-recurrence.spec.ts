/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { SpentRecurrenceComponentsPage, SpentRecurrenceDeleteDialog, SpentRecurrenceUpdatePage } from './spent-recurrence.page-object';

const expect = chai.expect;

describe('SpentRecurrence e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let spentRecurrenceUpdatePage: SpentRecurrenceUpdatePage;
  let spentRecurrenceComponentsPage: SpentRecurrenceComponentsPage;
  /*let spentRecurrenceDeleteDialog: SpentRecurrenceDeleteDialog;*/

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load SpentRecurrences', async () => {
    await navBarPage.goToEntity('spent-recurrence');
    spentRecurrenceComponentsPage = new SpentRecurrenceComponentsPage();
    await browser.wait(ec.visibilityOf(spentRecurrenceComponentsPage.title), 5000);
    expect(await spentRecurrenceComponentsPage.getTitle()).to.eq('mySharedHomeActivitiesApp.spentRecurrence.home.title');
  });

  it('should load create SpentRecurrence page', async () => {
    await spentRecurrenceComponentsPage.clickOnCreateButton();
    spentRecurrenceUpdatePage = new SpentRecurrenceUpdatePage();
    expect(await spentRecurrenceUpdatePage.getPageTitle()).to.eq('mySharedHomeActivitiesApp.spentRecurrence.home.createOrEditLabel');
    await spentRecurrenceUpdatePage.cancel();
  });

  /* it('should create and save SpentRecurrences', async () => {
        const nbButtonsBeforeCreate = await spentRecurrenceComponentsPage.countDeleteButtons();

        await spentRecurrenceComponentsPage.clickOnCreateButton();
        await promise.all([
            spentRecurrenceUpdatePage.setAmountInput('5'),
            spentRecurrenceUpdatePage.setRecurrenceFromInput('2000-12-31'),
            spentRecurrenceUpdatePage.setRecurrenceToInput('2000-12-31'),
            spentRecurrenceUpdatePage.setDayInMonthInput('5'),
            spentRecurrenceUpdatePage.spentSelectLastOption(),
        ]);
        expect(await spentRecurrenceUpdatePage.getAmountInput()).to.eq('5', 'Expected amount value to be equals to 5');
        expect(await spentRecurrenceUpdatePage.getRecurrenceFromInput()).to.eq('2000-12-31', 'Expected recurrenceFrom value to be equals to 2000-12-31');
        expect(await spentRecurrenceUpdatePage.getRecurrenceToInput()).to.eq('2000-12-31', 'Expected recurrenceTo value to be equals to 2000-12-31');
        expect(await spentRecurrenceUpdatePage.getDayInMonthInput()).to.eq('5', 'Expected dayInMonth value to be equals to 5');
        await spentRecurrenceUpdatePage.save();
        expect(await spentRecurrenceUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await spentRecurrenceComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    });*/

  /* it('should delete last SpentRecurrence', async () => {
        const nbButtonsBeforeDelete = await spentRecurrenceComponentsPage.countDeleteButtons();
        await spentRecurrenceComponentsPage.clickOnLastDeleteButton();

        spentRecurrenceDeleteDialog = new SpentRecurrenceDeleteDialog();
        expect(await spentRecurrenceDeleteDialog.getDialogTitle())
            .to.eq('mySharedHomeActivitiesApp.spentRecurrence.delete.question');
        await spentRecurrenceDeleteDialog.clickOnConfirmButton();

        expect(await spentRecurrenceComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });*/

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
