/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ShoppingListComponentsPage, ShoppingListDeleteDialog, ShoppingListUpdatePage } from './shopping-list.page-object';

const expect = chai.expect;

describe('ShoppingList e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let shoppingListUpdatePage: ShoppingListUpdatePage;
  let shoppingListComponentsPage: ShoppingListComponentsPage;
  let shoppingListDeleteDialog: ShoppingListDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load ShoppingLists', async () => {
    await navBarPage.goToEntity('shopping-list');
    shoppingListComponentsPage = new ShoppingListComponentsPage();
    await browser.wait(ec.visibilityOf(shoppingListComponentsPage.title), 5000);
    expect(await shoppingListComponentsPage.getTitle()).to.eq('mySharedHomeActivitiesApp.shoppingList.home.title');
  });

  it('should load create ShoppingList page', async () => {
    await shoppingListComponentsPage.clickOnCreateButton();
    shoppingListUpdatePage = new ShoppingListUpdatePage();
    expect(await shoppingListUpdatePage.getPageTitle()).to.eq('mySharedHomeActivitiesApp.shoppingList.home.createOrEditLabel');
    await shoppingListUpdatePage.cancel();
  });

  it('should create and save ShoppingLists', async () => {
    const nbButtonsBeforeCreate = await shoppingListComponentsPage.countDeleteButtons();

    await shoppingListComponentsPage.clickOnCreateButton();
    await promise.all([
      shoppingListUpdatePage.setLabelInput('label')
      // shoppingListUpdatePage.ownerSelectLastOption(),
    ]);
    expect(await shoppingListUpdatePage.getLabelInput()).to.eq('label', 'Expected Label value to be equals to label');
    await shoppingListUpdatePage.save();
    expect(await shoppingListUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await shoppingListComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last ShoppingList', async () => {
    const nbButtonsBeforeDelete = await shoppingListComponentsPage.countDeleteButtons();
    await shoppingListComponentsPage.clickOnLastDeleteButton();

    shoppingListDeleteDialog = new ShoppingListDeleteDialog();
    expect(await shoppingListDeleteDialog.getDialogTitle()).to.eq('mySharedHomeActivitiesApp.shoppingList.delete.question');
    await shoppingListDeleteDialog.clickOnConfirmButton();

    expect(await shoppingListComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
