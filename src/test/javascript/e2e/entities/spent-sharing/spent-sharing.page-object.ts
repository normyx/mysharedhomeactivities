import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class SpentSharingComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('msha-spent-sharing div table .btn-danger'));
  title = element.all(by.css('msha-spent-sharing div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class SpentSharingUpdatePage {
  pageTitle = element(by.id('msha-spent-sharing-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  amountShareInput = element(by.id('field_amountShare'));
  shareInput = element(by.id('field_share'));
  sharingUserSelect = element(by.id('field_sharingUser'));
  spentSelect = element(by.id('field_spent'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setAmountShareInput(amountShare) {
    await this.amountShareInput.sendKeys(amountShare);
  }

  async getAmountShareInput() {
    return await this.amountShareInput.getAttribute('value');
  }

  async setShareInput(share) {
    await this.shareInput.sendKeys(share);
  }

  async getShareInput() {
    return await this.shareInput.getAttribute('value');
  }

  async sharingUserSelectLastOption(timeout?: number) {
    await this.sharingUserSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async sharingUserSelectOption(option) {
    await this.sharingUserSelect.sendKeys(option);
  }

  getSharingUserSelect(): ElementFinder {
    return this.sharingUserSelect;
  }

  async getSharingUserSelectedOption() {
    return await this.sharingUserSelect.element(by.css('option:checked')).getText();
  }

  async spentSelectLastOption(timeout?: number) {
    await this.spentSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async spentSelectOption(option) {
    await this.spentSelect.sendKeys(option);
  }

  getSpentSelect(): ElementFinder {
    return this.spentSelect;
  }

  async getSpentSelectedOption() {
    return await this.spentSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class SpentSharingDeleteDialog {
  private dialogTitle = element(by.id('msha-delete-spentSharing-heading'));
  private confirmButton = element(by.id('msha-confirm-delete-spentSharing'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
