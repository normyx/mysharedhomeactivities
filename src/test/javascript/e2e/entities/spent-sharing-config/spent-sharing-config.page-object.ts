import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class SpentSharingConfigComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('msha-spent-sharing-config div table .btn-danger'));
  title = element.all(by.css('msha-spent-sharing-config div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class SpentSharingConfigUpdatePage {
  pageTitle = element(by.id('msha-spent-sharing-config-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  shareInput = element(by.id('field_share'));
  sharingUserSelect = element(by.id('field_sharingUser'));
  defaultSpentConfigSelect = element(by.id('field_defaultSpentConfig'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setShareInput(share) {
    await this.shareInput.sendKeys(share);
  }

  async getShareInput() {
    return await this.shareInput.getAttribute('value');
  }

  async sharingUserSelectLastOption(timeout?: number) {
    await this.sharingUserSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async sharingUserSelectOption(option) {
    await this.sharingUserSelect.sendKeys(option);
  }

  getSharingUserSelect(): ElementFinder {
    return this.sharingUserSelect;
  }

  async getSharingUserSelectedOption() {
    return await this.sharingUserSelect.element(by.css('option:checked')).getText();
  }

  async defaultSpentConfigSelectLastOption(timeout?: number) {
    await this.defaultSpentConfigSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async defaultSpentConfigSelectOption(option) {
    await this.defaultSpentConfigSelect.sendKeys(option);
  }

  getDefaultSpentConfigSelect(): ElementFinder {
    return this.defaultSpentConfigSelect;
  }

  async getDefaultSpentConfigSelectedOption() {
    return await this.defaultSpentConfigSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class SpentSharingConfigDeleteDialog {
  private dialogTitle = element(by.id('msha-delete-spentSharingConfig-heading'));
  private confirmButton = element(by.id('msha-confirm-delete-spentSharingConfig'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
