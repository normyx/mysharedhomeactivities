/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ProfilComponentsPage, ProfilDeleteDialog, ProfilUpdatePage } from './profil.page-object';
import * as path from 'path';

const expect = chai.expect;

describe('Profil e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let profilUpdatePage: ProfilUpdatePage;
  let profilComponentsPage: ProfilComponentsPage;
  let profilDeleteDialog: ProfilDeleteDialog;
  const fileNameToUpload = 'logo-jhipster.png';
  const fileToUpload = '../../../../../../src/main/webapp/content/images/' + fileNameToUpload;
  const absolutePath = path.resolve(__dirname, fileToUpload);

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Profils', async () => {
    await navBarPage.goToEntity('profil');
    profilComponentsPage = new ProfilComponentsPage();
    await browser.wait(ec.visibilityOf(profilComponentsPage.title), 5000);
    expect(await profilComponentsPage.getTitle()).to.eq('mySharedHomeActivitiesApp.profil.home.title');
  });

  it('should load create Profil page', async () => {
    await profilComponentsPage.clickOnCreateButton();
    profilUpdatePage = new ProfilUpdatePage();
    expect(await profilUpdatePage.getPageTitle()).to.eq('mySharedHomeActivitiesApp.profil.home.createOrEditLabel');
    await profilUpdatePage.cancel();
  });

  it('should create and save Profils', async () => {
    const nbButtonsBeforeCreate = await profilComponentsPage.countDeleteButtons();

    await profilComponentsPage.clickOnCreateButton();
    await promise.all([
      profilUpdatePage.setDisplayNameInput('displayName'),
      profilUpdatePage.setPhotoInput(absolutePath),
      profilUpdatePage.userSelectLastOption()
    ]);
    expect(await profilUpdatePage.getDisplayNameInput()).to.eq('displayName', 'Expected DisplayName value to be equals to displayName');
    expect(await profilUpdatePage.getPhotoInput()).to.endsWith(fileNameToUpload, 'Expected Photo value to be end with ' + fileNameToUpload);
    await profilUpdatePage.save();
    expect(await profilUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await profilComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Profil', async () => {
    const nbButtonsBeforeDelete = await profilComponentsPage.countDeleteButtons();
    await profilComponentsPage.clickOnLastDeleteButton();

    profilDeleteDialog = new ProfilDeleteDialog();
    expect(await profilDeleteDialog.getDialogTitle()).to.eq('mySharedHomeActivitiesApp.profil.delete.question');
    await profilDeleteDialog.clickOnConfirmButton();

    expect(await profilComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
