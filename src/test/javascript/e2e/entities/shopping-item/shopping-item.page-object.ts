import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class ShoppingItemComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('msha-shopping-item div table .btn-danger'));
  title = element.all(by.css('msha-shopping-item div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class ShoppingItemUpdatePage {
  pageTitle = element(by.id('msha-shopping-item-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  labelInput = element(by.id('field_label'));
  doneInput = element(by.id('field_done'));
  listSelect = element(by.id('field_list'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setLabelInput(label) {
    await this.labelInput.sendKeys(label);
  }

  async getLabelInput() {
    return await this.labelInput.getAttribute('value');
  }

  getDoneInput(timeout?: number) {
    return this.doneInput;
  }

  async listSelectLastOption(timeout?: number) {
    await this.listSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async listSelectOption(option) {
    await this.listSelect.sendKeys(option);
  }

  getListSelect(): ElementFinder {
    return this.listSelect;
  }

  async getListSelectedOption() {
    return await this.listSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ShoppingItemDeleteDialog {
  private dialogTitle = element(by.id('msha-delete-shoppingItem-heading'));
  private confirmButton = element(by.id('msha-confirm-delete-shoppingItem'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
