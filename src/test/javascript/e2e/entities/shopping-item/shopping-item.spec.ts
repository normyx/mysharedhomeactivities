/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ShoppingItemComponentsPage, ShoppingItemDeleteDialog, ShoppingItemUpdatePage } from './shopping-item.page-object';

const expect = chai.expect;

describe('ShoppingItem e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let shoppingItemUpdatePage: ShoppingItemUpdatePage;
  let shoppingItemComponentsPage: ShoppingItemComponentsPage;
  let shoppingItemDeleteDialog: ShoppingItemDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load ShoppingItems', async () => {
    await navBarPage.goToEntity('shopping-item');
    shoppingItemComponentsPage = new ShoppingItemComponentsPage();
    await browser.wait(ec.visibilityOf(shoppingItemComponentsPage.title), 5000);
    expect(await shoppingItemComponentsPage.getTitle()).to.eq('mySharedHomeActivitiesApp.shoppingItem.home.title');
  });

  it('should load create ShoppingItem page', async () => {
    await shoppingItemComponentsPage.clickOnCreateButton();
    shoppingItemUpdatePage = new ShoppingItemUpdatePage();
    expect(await shoppingItemUpdatePage.getPageTitle()).to.eq('mySharedHomeActivitiesApp.shoppingItem.home.createOrEditLabel');
    await shoppingItemUpdatePage.cancel();
  });

  it('should create and save ShoppingItems', async () => {
    const nbButtonsBeforeCreate = await shoppingItemComponentsPage.countDeleteButtons();

    await shoppingItemComponentsPage.clickOnCreateButton();
    await promise.all([shoppingItemUpdatePage.setLabelInput('label'), shoppingItemUpdatePage.listSelectLastOption()]);
    expect(await shoppingItemUpdatePage.getLabelInput()).to.eq('label', 'Expected Label value to be equals to label');
    const selectedDone = shoppingItemUpdatePage.getDoneInput();
    if (await selectedDone.isSelected()) {
      await shoppingItemUpdatePage.getDoneInput().click();
      expect(await shoppingItemUpdatePage.getDoneInput().isSelected(), 'Expected done not to be selected').to.be.false;
    } else {
      await shoppingItemUpdatePage.getDoneInput().click();
      expect(await shoppingItemUpdatePage.getDoneInput().isSelected(), 'Expected done to be selected').to.be.true;
    }
    await shoppingItemUpdatePage.save();
    expect(await shoppingItemUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await shoppingItemComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last ShoppingItem', async () => {
    const nbButtonsBeforeDelete = await shoppingItemComponentsPage.countDeleteButtons();
    await shoppingItemComponentsPage.clickOnLastDeleteButton();

    shoppingItemDeleteDialog = new ShoppingItemDeleteDialog();
    expect(await shoppingItemDeleteDialog.getDialogTitle()).to.eq('mySharedHomeActivitiesApp.shoppingItem.delete.question');
    await shoppingItemDeleteDialog.clickOnConfirmButton();

    expect(await shoppingItemComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
